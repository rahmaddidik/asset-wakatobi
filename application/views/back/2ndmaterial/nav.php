    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                        <div class="uk-dropdown uk-dropdown-width-3">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-2-3">
                                    <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                        <?php $posts = $this->base_config->groups_access_sigle('menu','posts'); if($posts == false) { ?>
                                        <a href="cms/posts/index/add">
                                            <i class="material-icons md-36">&#xE02F;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_new}</span>
                                        </a>
                                        <?php } ?>
                                        <?php $pages = $this->base_config->groups_access_sigle('menu','pages'); if($pages == false) { ?>
                                        <a href="cms/pages/index/add">
                                            <i class="material-icons md-36">&#xE051;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_pages}</span>
                                        </a>
                                        <?php } ?>
                                        <?php $media = $this->base_config->groups_access_sigle('menu','media'); if($media == false) { ?>
                                        <a href="cms/media">
                                            <i class="material-icons md-36 ">&#xE3B6;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_media}</span>
                                        </a>
                                        <?php } ?>
                                        <?php $comments = $this->base_config->groups_access_sigle('menu','comments'); if($comments == false) { ?>
                                        <a href="cms/comments">
                                            <i class="material-icons md-36">&#xE0B9;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_comment}</span>
                                        </a>
                                        <?php } ?>
                                        <?php if($this->ion_auth->is_admin()){ ?>
                                        <a href="cms/customize">
                                            <i class="material-icons md-36">&#xE3B7;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_appear}</span>
                                        </a>
                                        <a href="cms/users">
                                            <i class="material-icons md-36">&#xE7EF;</i>
                                            <span class="uk-text-muted uk-display-block">{_nav_menu_users}</span>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <ul class="uk-nav uk-nav-dropdown uk-panel">
                                        <li class="uk-nav-header">{_shortcut_link}</li>
                                        <li><a target="_blank" href="<?php echo base_url() ;?>">{_shortcut_visit}</a></li>
                                        <li><a target="_blank" href="https://www.google.com/?q=<?php echo base_url() ;?>">{_shortcut_index}</a></li>
                                        <li><a target="_blank" href="javascript:void(0)">{_shortcut_doc}</a></li>
                                        <li><a target="_blank" href="https://secondvisioncorp.com">{_shortcut_umbrella}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><?php $allnotif = $this->M_base_config->countDatamultiple(array('table'=>'tb_notification','where'=>array(array('wherefield'=>'notification_status','where_value'=>'active'),array('wherefield'=>'notification_user','where_value'=>$this->ion_auth->user()->row()->id))));
                                if(!empty($allnotif)){ echo '<span class="uk-badge">'.$allnotif.'</span>'; } ?>
                            </a>
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-2 uk-active notifupdate"><a href="#" class="js-uk-prevent uk-text-small">{_contact_notif} (<span id="timeline"><?php $contactnotif=array('table'=>'tb_notification','where'=>array(array('wherefield'=>'notification_status','where_value'=>'active'),array('wherefield'=>'notification_type','where_value'=>'contact'),array('wherefield'=>'notification_user','where_value'=>$this->ion_auth->user()->row()->id))); echo $this->M_base_config->countDatamultiple($contactnotif); ?></span>)</a></li>
                                        <li class="uk-width-1-2 notifupdate"><a href="#" class="js-uk-prevent uk-text-small">{_timeline_notif} (<span id="timeline"><?php $timeline=array('table'=>'tb_notification','where'=>array(array('wherefield'=>'notification_status','where_value'=>'active'),array('wherefield'=>'notification_type','where_value'=>'timeline'),array('wherefield'=>'notification_user','where_value'=>$this->ion_auth->user()->row()->id))); echo $this->M_base_config->countDatamultiple($timeline); ?></span>)</a></li>
                                    </ul>
                                    <?php $notif=$this->base_config->notificationlist(); ?>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <?php 
                                                 $nonotif =false;
                                                foreach($notif as $notifval) {
                                                        if($notifval->notification_type  == 'comment' && $notifval->notification_type == 'contact') {
                                                             $nonotif =true;
                                                ?>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green"><i class="material-icons"><?php if($notifval->notification_icon == 'update'){ echo "&#xE150;"; }else if($notifval->notification_icon == 'delete') {echo '&#xE15C;'; }else{ echo '&#xE147;'; } ?></i></span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $notifval->notification_desc; ?></span>
                                                        <span class="uk-text-small uk-text-muted"><?php echo lang('in_notif')." : ".$this->base_config->timeAgo($notifval2->notification_date);?>.</span>
                                                    </div>
                                                </li>
                                                <?php 
                                                        }
                                                    }
                                                    if($nonotif == false){
                                                        echo '<li>'.lang('no_notif').'</li>';
                                                    }   
                                                ?>
                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="cms/comments" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">{_all_notif}</a>
                                            </div>
                                        </li>
                                        <li>
                                            <ul class="md-list md-list-addon">
                                                <?php 
                                                $nonotif2 =false;
                                                foreach($notif as $notifval2) {
                                                        if($notifval2->notification_type  == 'timeline') {
                                                            $nonotif2 =true;
                                                ?>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-light-green"><i class="material-icons"><?php if($notifval2->notification_icon == 'update'){ echo "&#xE150;"; }else if($notifval2->notification_icon == 'delete') {echo '&#xE15C;'; }else{ echo '&#xE147;'; } ?></i></span>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php echo $notifval2->notification_desc; ?></span>
                                                        <span class="uk-text-small uk-text-muted"><?php echo lang('in_notif')." : ".$this->base_config->timeAgo($notifval2->notification_date); ?>.</span>
                                                    </div>
                                                </li>
                                                <?php 
                                                        }
                                                    }
                                                    if($nonotif2 == false){
                                                        echo '<li>'.lang('no_notif').'</li>';
                                                    }  
                                                ?>

                                            </ul>
                                            <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="cms/notification" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">{_all_notif}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="img/load/100/100/crop/<?php echo  $this->ion_auth->user()->row()->user_avatar ;?>" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="cms/profile">{_nav_menu_users_profile}</a></li>
                                    <li><a href="cms/auth/logout">{_logout}</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form action="cms/search" method="get" class="uk-form">
                <input type="text" name="q" class="header_main_search_input" />
                <button type="submit" class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            </form>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="cms" class="sSidebar_hide"><img width="190" height="80" src="img/load/190/80/png2/{logo}" alt="" /></a>
            </div>
        </div>
        
        <div class="menu_section">
            <ul>
                <li class="<?php echo $this->base_config->activelinknav(''); ?>" title="<?php echo lang('nav_menu_dashboard'); ?>">
                    <a href="cms">
                        <span class="menu_icon"><i class="material-icons">&#xE88A;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_dashboard'); ?></span>
                    </a>
                </li>
                <?php $jadwal_kegiatan = $this->base_config->groups_access_sigle('menu','jadwal_kegiatan'); if($jadwal_kegiatan == false) { ?>
                <li class="<?php echo $this->base_config->activelinknav('jadwal_kegiatan'); ?>" title="<?php echo 'jadwal kegiatan'; ?>">
                    <a href="cms/jadwal_kegiatan">
                        <span class="menu_icon"><i class="material-icons">&#xE8A3;</i></span>
                        <span class="menu_title"><?php echo 'jadwal kegiatan'; ?></span>
                    </a>
                </li>
                <?php } ?>
                <?php $saran_kritik = $this->base_config->groups_access_sigle('menu','kritik_saran'); if($saran_kritik == false) { ?>
                    <li class="<?php echo $this->base_config->activelinknav('kritik_saran'); ?>" title="<?php echo 'kritik dan saran'; ?>">
                        <a href="cms/kritik_saran">
                            <span class="menu_icon"><i class="material-icons">&#xE8D4;</i></span>
                            <span class="menu_title"><?php echo 'Kritik dan Saran'; ?></span>
                        </a>
                    </li>
                <?php } ?>
                <?php $posts = $this->base_config->groups_access_sigle('menu','posts'); if($posts == false) { ?>
                <li class="<?php echo $this->base_config->activelinknav('posts'); ?>" title="<?php echo lang('nav_menu_posts'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE02F;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_posts'); ?></span>
                    </a>
                    <ul>
                        <?php $posts_add = $this->base_config->groups_access_sigle('menu','posts_add'); if($posts_add == false) { ?>
                        <li class="<?php echo $this->base_config->activelinknav('posts',true); ?>"><a href="cms/posts"><?php echo lang('nav_menu_allposts'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('add','posts'); ?>"><a  href="cms/posts/index/add"><?php echo lang('nav_menu_new'); ?></a></li>
                        <?php } ?>
                        <?php $posts_categories = $this->base_config->groups_access_sigle('menu','posts_categories'); if($posts_categories == false) { ?>
                        <li class="<?php echo $this->base_config->activelinknav('category',true); ?>"><a href="cms/category/"><?php echo lang('nav_menu_cat'); ?></a></li>
                         <?php } ?>
                         <?php $posts_tags = $this->base_config->groups_access_sigle('menu','posts_tags'); if($posts_tags == false) { ?>
                        <li class="<?php echo $this->base_config->activelinknav('tags',true); ?>"><a href="cms/tags/"><?php echo lang('nav_menu_tags'); ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <?php $media = $this->base_config->groups_access_sigle('menu','media'); if($media == false) { ?> 
                <li class="<?php echo $this->base_config->activelinknav('media'); ?>" title="<?php echo lang('nav_menu_media'); ?>">
                    <a href="cms/media">
                        <span class="menu_icon"><i class="material-icons">&#xE3B6;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_media'); ?></span>
                    </a>
                </li>
                <?php } ?>
                
                <?php $pages = $this->base_config->groups_access_sigle('menu','pages'); if($pages == false) { ?> 
                <li class="<?php echo $this->base_config->activelinknav('pages'); ?>" title="<?php echo lang('nav_menu_pages'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE051;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_pages'); ?></span>
                    </a>
                    <ul>
                        <li class="<?php echo $this->base_config->activelinknav('pages',true); ?>"><a href="cms/pages"><?php echo lang('nav_menu_allpages'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('add','pages'); ?>"><a  href="cms/pages/index/add"><?php echo lang('nav_menu_new'); ?></a></li>
                    </ul>
                </li>
                <?php } ?>
                
                <?php $pages = $this->base_config->groups_access_sigle('menu','comments'); if($pages == false) { ?>  
                <li class="<?php echo $this->base_config->activelinknav('comments'); ?>" title="<?php echo lang('nav_menu_comment'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_comment'); ?></span>
                    </a>
                    <ul>
                        <li class="<?php echo $this->base_config->activelinknav('comments',true); ?>"><a href="cms/comments"><?php echo lang('nav_menu_allcomment'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('contact',true); ?>"><a  href="cms/contact"><?php echo lang('nav_menu_contact'); ?></a></li>
                    </ul>
                </li>
                <?php } ?>
                <?php if($this->ion_auth->is_admin()){ ?>
                <li class="<?php echo $this->base_config->activelinknav('themes'); ?>" title="<?php echo lang('nav_menu_appear'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE3B7;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_appear'); ?></span>
                    </a>
                    <ul>
                        <li class="<?php echo $this->base_config->activelinknav('themes',true); ?>"><a href="cms/themes"><?php echo lang('nav_menu_appear_theme'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('customize',true); ?>"><a  href="cms/customize"><?php echo lang('nav_menu_appear_custom'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('widgets',true); ?>"><a  href="cms/widgets"><?php echo lang('nav_menu_appear_widget'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('menus',true); ?>"><a  href="cms/menus"><?php echo lang('nav_menu_appear_menu'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('cpanel',true); ?>"><a  href="cms/cpanel"><?php echo lang('nav_menu_appear_cpanel'); ?></a></li>
                    </ul>
                </li>
                <li class="" title="<?php echo lang('nav_menu_plugins'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE627;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_plugins'); ?></span>
                    </a>
                    <ul>
                        <li class=""><a href="javascript:void(0)"><?php echo lang('nav_menu_plugins_in'); ?></a></li>
                        <li class=""><a  href="javascript:void(0)"><?php echo lang('nav_menu_plugins_new'); ?></a></li>
                    </ul>
                </li>
                <?php } ?>
                <li class="<?php echo $this->base_config->activelinknav('users'); ?>" title="<?php echo lang('nav_menu_users'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE7EF;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_users'); ?></span>
                    </a> 
                    <ul>
                        <?php if($this->ion_auth->is_admin()){ ?>
                        <li class="<?php echo $this->base_config->activelinknav('users',true); ?>"><a href="cms/users"><?php echo lang('nav_menu_users_all'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('add','users'); ?>"><a  href="cms/users/index/add"><?php echo lang('nav_menu_users_new'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('groups',true); ?>"><a  href="cms/groups"><?php echo lang('nav_menu_users_group'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('export_database',true); ?>"><a  href="cms/export_database">Export Database</a></li>
                        <?php } ?>
                        <li class="<?php echo $this->base_config->activelinknav('profile',true); ?>"><a  href="cms/profile"><?php echo lang('nav_menu_users_profile'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('notification',true); ?>"><a  href="cms/notification"><?php echo lang('nav_menu_users_notif'); ?></a></li>
                    </ul>
                </li>
                <?php if($this->ion_auth->is_admin()){ ?>
                    <li class="<?php echo $this->base_config->activelinknav('export_database'); ?>" title="export_database">
                        <a href="cms/export_database">
                            <span class="menu_icon"><i class="material-icons">&#xE255;</i></span>
                            <span class="menu_title">Export Database</span>
                        </a>
                    </li>
                <li class="<?php echo $this->base_config->activelinknav('setting'); ?>" title="<?php echo lang('nav_menu_setting'); ?>">
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE8B8;</i></span>
                        <span class="menu_title"><?php echo lang('nav_menu_setting'); ?></span>
                    </a> 
                    <ul>
                        <li class="<?php echo $this->base_config->activelinknav('setting',true); ?>"><a href="cms/setting">Setting</a></li>
                        <li class="<?php echo $this->base_config->activelinknav('general',true); ?>"><a href="cms/general"><?php echo lang('nav_menu_setting_general'); ?></a></li>
                       <!--  <li class="<?php echo $this->base_config->activelinknav('add','users'); ?>"><a  href="cms/users/index/add"><?php echo lang('nav_menu_setting_writing'); ?></a></li> -->
                        <li class="<?php echo $this->base_config->activelinknav('company',true); ?>"><a  href="cms/company"><?php echo lang('nav_menu_setting_company'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('seo',true); ?>"><a  href="cms/seo"><?php echo lang('nav_menu_setting_seo'); ?></a></li>
                        <li class="<?php echo $this->base_config->activelinknav('mitra',true); ?>"><a  href="cms/mitra"><?php echo lang('nav_menu_setting_mitra'); ?></a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
    </aside><!-- main sidebar end -->