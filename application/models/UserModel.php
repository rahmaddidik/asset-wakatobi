<?php
/**
 * @property  CI_Lang $lang
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 */
class UserModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_group_excerp_members()
    {
        $tmp = [];
        $this->db->select('tb_user.id');
        $this->db->join('tb_users_groups','tb_users_groups.user_id=tb_user.id');
        $this->db->join('tb_groups','tb_groups.id=tb_users_groups.group_id');
        $this->db->where_not_in('tb_groups.name', 'members' );
        $this->db->group_by('tb_user.id');
        $result = $this->db->get('tb_user')->result_array();
        foreach ( $result as $item) {
            $tmp[] = $item['id'];
        }
        return $tmp;
    }

    public function getByGroup($group_name='members')
    {
        $tmp = [];
        $this->db->select('tb_user.id');
        $this->db->join('tb_users_groups','tb_users_groups.user_id=tb_user.id');
        $this->db->join('tb_groups','tb_groups.id=tb_users_groups.group_id');
        $this->db->where('tb_groups.name',$group_name );
        $this->db->group_by('tb_user.id');
        $result = $this->db->get('tb_user')->result_array();
        foreach ( $result as $item) {
            $tmp[] = $item['id'];
        }
        return $tmp;
    }

    public function getSingleGroupIDbyName($group_name='members')
    {
        $this->db->where('name', $group_name);
        return $this->db->get('tb_groups',1)->row('id');
    }

    public function getByEmail($email)
    {
        $this->db->where('email', $email);
        $userDB = $this->db->get('tb_user', 1)->row();
        return $userDB;
    }

    public function getByID($id)
    {
        $this->db->where('id', $id);
        $userDB = $this->db->get('tb_user', 1)->row();
        return $userDB;
    }

    public function getLocation($ipAddress)
    {
        $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ipAddress"));
        //$country = $geo["geoplugin_countryName"];
        $city = @$geo["geoplugin_city"];
        return $city;
    }
}