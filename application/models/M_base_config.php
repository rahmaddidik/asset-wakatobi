<?php
/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  CI_Email email
 */
class M_base_config  extends CI_Model  {
	
	public function __construct()
    {
    	parent::__construct();
    	date_default_timezone_set('Asia/Jakarta');
    }
    public function getData($param){
		$data='';
		if(array_key_exists('where', $param)){
			for($i=0;$i<count($param['where']);$i++){
				$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
			}
		}
    	$query = $this->db->order_by($param['nm_sort'],$param['sort'])
    			 		  ->limit($param['limit'],$param['offset'])
    			 		  ->get($param['table']);
		if($query){
			$data=$query->result();
		}else{
			$data='';
		}
		return $data;
	}
	public function getSimpleData($param){
		$tmp='';
        $return = $param['return'];
		if(array_key_exists('where', $param)){
			for($i=0;$i<count($param['where']);$i++){
				$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
			}
		}
		$query=$this->db->get($param['table']);
		if ($query->num_rows() > 0)
		{
			foreach($query->result() as $val){
					$tmp=$val->$return;
			}
		}
        return $tmp;
	}
	public function countDatamultiple($param){
		if(array_key_exists('where', $param)){
			for($i=0;$i<count($param['where']);$i++){
				$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
			}
		}
    	return $this->db->count_all_results($param['table']);
	}
    public function getSingleSetting($jenis,$nama){
		$this->db->where('setting_type',$jenis);
		$this->db->where('setting_name',$nama);
		$query=$this->db->get('tb_setting');
		if($query)
		$row = $query->row();
		if($row){
			$data=$row->setting_value;
		}else{
			$data='';
		}
		return $data;
	}
	public function getMultiSetting($jenis,$nama){
		if(!empty($nama))
		$this->db->where_in('setting_name',$nama);
		
		$this->db->where('setting_typ',$jenis);
		$query=$this->db->get('tb_setting');
		if($query){
			$data=$query->result();
		}else{
			$data='';
		}
		return $data;
	}
	public function cekaAuth(){
		if (!$this->ion_auth->logged_in()){
			redirect('cms/auth', 'refresh');
		}
	}
	public function ifLogin(){ 
		if ($this->ion_auth->logged_in()){
			redirect('cms', 'refresh');
		}
	}
	public function countData($table,$where,$where_value) {
		return $this->db
					->where($where,$where_value)
			 		->get($table)
			 		->num_rows();
	}
	public function search($param){
		$data='';
		if(array_key_exists('where', $param)){
			for($i=0;$i<count($param['where']);$i++){
				$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
			}
		}
		if(array_key_exists('match', $param)){
			for($i=0;$i<count($param['match']);$i++){
				$this->db->where('MATCH ('.$param['match'][$i]['matchfield'].') AGAINST ("'.$param['match'][$i]['match_value'].'")', NULL, FALSE);
				
			}
		}
    	$query = $this->db->order_by($param['nm_sort'],$param['sort'])
    			 		  ->limit($param['limit'],$param['offset'])
    			 		  ->get($param['table']);
		if($query->num_rows() > 0){
			$data=$query->result();
		}else{
			if(array_key_exists('where', $param)){
				for($i=0;$i<count($param['where']);$i++){
					$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
				}
			}
			if(array_key_exists('match', $param)){
				for($i=0;$i<count($param['match']);$i++){
					$this->db->like($param['match'][$i]['matchfield'],$param['match'][$i]['match_value']);
				}
			}
	    	$query = $this->db->order_by($param['nm_sort'],$param['sort'])
	    			 		  ->limit($param['limit'],$param['offset'])
	    			 		  ->get($param['table']);
	        $data=$query->result();
		}
		return $data;
	}
	public function countSearch($param){
		$data='';
		if(array_key_exists('where', $param)){
			for($i=0;$i<count($param['where']);$i++){
				$this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
			}
		}
		if(array_key_exists('match', $param)){
			for($i=0;$i<count($param['match']);$i++){
					$this->db->where('MATCH ('.$param['match'][$i]['matchfield'].') AGAINST ("'.$param['match'][$i]['match_value'].'")', NULL, FALSE);
			}
		}
    	$query = $this->db->get($param['table']);
		if($query->num_rows() > 0){
			$data=$query->num_rows();
		}else{
			if(array_key_exists('match', $param)){
				for($i=0;$i<count($param['match']);$i++){
					$this->db->like($param['match'][$i]['matchfield'],$param['match'][$i]['match_value']);
				}
			}
	    	$query = $this->db->get($param['table']);
	        $data=$query->num_rows();
		}
		return $data;
	}
	//Record Notification
	public function insertnotif($data=array()){
		$user_notification = array("notification_type" => $data['type'],"notification_user" =>$data['user'],"notification_parent" => $data['parent'],"notification_desc" => $data['desc'].' <a target="_blank" href="'.$data['link'].'">'.$data['title'].'</a>',"notification_status" => 'active',"notification_icon" => $data['icon'],"notification_date" => date('Y-m-d H:i:s'));
        $this->db->insert('tb_notification',$user_notification);
	}
	
}
