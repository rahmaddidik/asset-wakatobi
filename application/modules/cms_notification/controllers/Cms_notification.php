<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_notification extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $user = $this->ion_auth->user()->row();
            $umbrella = new grocery_CRUD();
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_notification');
            $umbrella->set_subject('Notifications');
            $umbrella->columns('notification_type', 'notification_desc', 'notification_date', 'notification_status');
            $umbrella->where('notification_user', $user->id);
            $umbrella->order_by('notification_date', 'desc');
            $umbrella->unset_add();
            $umbrella->unset_edit();
            $umbrella->display_as('notification_status', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->callback_column('notification_date', array($this, '_notif_date'));
            $umbrella->callback_column('notification_status', array($this, '_callback_notification_type'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->set_composer(false);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _notif_date($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->notification_status == 'active') {
            $status = '<span class="uk-badge uk-badge-success">' . $row->notification_status . '</span>';
        } else {
            $status = '<span class="uk-badge uk-badge-warning">' . $row->notification_status . '</span>';
        }
        return lang('in_notif') . " : " . $this->base_config->timeAgo($value) . "<br>" . $status;
    }

    public function _callback_notification_type($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->notification_id . '" name="' . $row->notification_id . '" id="' . $row->notification_id . '" data-md-icheck />';
    }


}