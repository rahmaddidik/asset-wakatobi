<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_pages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            //Get Panel setting
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('page');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_post');
            $umbrella->set_subject('Page');
            $umbrella->columns('post_title', 'post_author', 'post_date', 'post_name');
            $umbrella->where('tb_post.post_type', 'pages');
            $umbrella->order_by('post_id', 'desc');
            $umbrella->unset_texteditor('post_title', 'full_text');
            $umbrella->unset_texteditor('post_meta_desc', 'full_text');
            $umbrella->unset_texteditor('post_meta_keyword', 'full_text');
            $umbrella->field_type('post_title', 'string');
            $umbrella->field_type('post_meta_keyword', 'string');
            $umbrella->field_type('post_name', 'hidden');
            $umbrella->field_type('post_type', 'hidden');
            $umbrella->field_type('post_mime_type', 'hidden');
            $umbrella->field_type('post_author', 'hidden');
            $umbrella->field_type('post_modified', 'hidden');
            $umbrella->add_fields('post_title', 'post_content', 'post_seo_title', 'post_meta_desc', 'post_meta_keyword', 'post_status', 'post_priority', 'post_date', 'post_comment', 'media_gallery', 'post_type', 'post_author', 'post_mime_type', 'post_name', 'post_parent', 'post_modified');
            $umbrella->edit_fields('post_title', 'post_content', 'post_seo_title', 'post_meta_desc', 'post_meta_keyword', 'post_status', 'post_priority', 'post_date', 'post_comment', 'media_gallery', 'post_type', 'post_author', 'post_mime_type', 'post_parent', 'post_modified');
            $umbrella->set_relation_n_n('media_gallery', 'tb_terms', 'tb_post', 'tb_terms.post_id', 'category_id', 'post_name', null, array('post_type' => 'media'));
            $umbrella->display_as('media_gallery', '');
            $umbrella->display_as('post_parent', '');
            $umbrella->display_as('post_comment_count', 'Comment');
            $umbrella->display_as('post_name', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('post_title', array($this, '_callback_title'));
            $umbrella->callback_column('post_date', array($this, '_post_date'));
            $umbrella->callback_column('post_name', array($this, '_callback_post_id'));
            $umbrella->callback_edit_field('post_parent', array($this, '_edit_callback_post_parent'));
            $umbrella->callback_edit_field('media_gallery', array($this, '_edit_callback_media_gallery'));
            $umbrella->callback_after_insert(array($this, 'notification_user_after_insert'));
            $umbrella->callback_after_update(array($this, 'notification_user_after_update'));
            $umbrella->callback_after_delete(array($this, 'notification_user_after_delete'));
            $umbrella->set_listfilter(true);
            $umbrella->callback_add_field('media_gallery', function () {
                return '';
            });
            $typefilter = $this->uri->segment(4);
            if ($typefilter == "publish" || $typefilter == "draf" || $typefilter == "trash") {
                $umbrella->where('tb_post.post_status', $typefilter);
            }
            $umbrella->callback_add_field('post_parent', function () {
                return '';
            });
            //Set Column Abigous
            $state = $umbrella->getState();
            if ($state != 'ajax_list' && $state != 'ajax_list_info') {
                $umbrella->set_relation('post_parent', 'tb_post', 'post_name');
            }
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */

    public function _set_callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        $post_array['post_modified'] = date("Y-m-d H:i:s");
        if (empty($post_array['post_status']))
            $post_array['post_priority'] = "Draft";
        if (empty($post_array['post_priority']))
            $post_array['post_priority'] = "Normal";
        if (empty($post_array['post_comment']))
            $post_array['post_comment'] = "Enable";
        if (empty($post_array['post_seo_title']))
            $post_array['post_seo_title'] = strip_tags(substr($post_array['post_title'], 0, 70));
        if (empty($post_array['post_meta_desc']))
            $post_array['post_meta_desc'] = strip_tags(substr($post_array['post_content'], 0, 156));
        if (empty($post_array['post_meta_keyword']))
            $post_array['post_meta_keyword'] = $post_array['post_seo_title'];
        if (empty($post_array['post_date']))
            $post_array['post_date'] = date("Y-m-d H:i:s");

        $config = array(
            'field' => 'post_name',
            'title' => 'post_title',
            'table' => 'tb_post',
            'id' => 'post_id',
        );
        $post_array['post_mime_type'] = 'text';
        $post_array['post_type'] = 'pages';
        $post_array['post_author'] = $this->ion_auth->user()->row()->username;
        $this->load->library('slug', $config);
        $post_array['post_name'] = $this->slug->create_uri($post_array['post_title']);

        return $post_array;
    }

    public function _set_callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        $post_array['post_modified'] = date("Y-m-d H:i:s");
        if (empty($post_array['post_status']))
            $post_array['post_priority'] = "Draft";
        if (empty($post_array['post_priority']))
            $post_array['post_priority'] = "Normal";
        if (empty($post_array['post_comment']))
            $post_array['post_comment'] = "Enable";
        if (empty($post_array['post_seo_title']))
            $post_array['post_seo_title'] = strip_tags(substr($post_array['post_title'], 0, 70));
        if (empty($post_array['post_meta_desc']))
            $post_array['post_meta_desc'] = strip_tags(substr($post_array['post_content'], 0, 156));
        if (empty($post_array['post_meta_keyword']))
            $post_array['post_meta_keyword'] = $post_array['post_seo_title'];
        if (empty($post_array['post_date'])) {
            $post_array['post_date'] = date("Y-m-d H:i:s");
        } else {
            $post_array['post_date'] = date("Y-m-d H:i:s", strtotime($post_array['post_date']));
        }

        return $post_array;
    }

    public function _post_date($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->post_status == 'Publish') {
            $status = '<span class="uk-badge uk-badge-success">' . lang('colum_publish') . '</span>';
        } else if ($row->post_status == 'Trash') {
            $status = '<span class="uk-badge uk-badge-danger">' . lang('colum_trash') . '</span>';
        } else {
            $status = '<span class="uk-badge uk-badge-warning">' . lang('colum_draf') . '</span>';
        }
        return $value . "<br><small>" . lang('updatein') . " : " . $this->base_config->timeAgo($row->post_modified) . "</small><br>" . $status;
    }

    public function _callback_title($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $type_con = $this->uri->segment(2);
        return "<a title='$value' href='" . site_url('cms/' . $type_con . '/index/edit/' . $row->post_id) . "'><b>$value</b></a>";
    }

    public function _callback_post_id($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->post_id . '" name="' . $row->post_id . '" id="' . $row->post_id . '" data-md-icheck />';
    }

    public function _edit_callback_post_parent($value, $primary_key, $row, $values)
    {
        $this->M_base_config->cekaAuth();
        $data = "";
        if (!empty($value)) {
            $url = $this->M_base_config->getSimpleData(array('table' => 'tb_post', 'return' => 'post_name', 'where' => array(array('wherefield' => 'post_id', 'where_value' => $value))));
            $data .= '<li class="uk-position-relative"><button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute mediaempety"></button><img src="img/load/200/200/png/' . $url . '" alt="" class="img_medium"/><input type="hidden" value="' . $value . '" id="field-post_parent" name="post_parent" /></li>';
        }
        return $data;
    }

    public function _edit_callback_media_gallery($value, $primary_key, $row, $values)
    {
        $this->M_base_config->cekaAuth();
        $data = "";
        if (!empty($value)) {
            foreach ($value as $key => $url) {
                $data .= '<li class="uk-position-relative"><button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute mediaempety"></button><img src="img/load/200/200/png/' . $url . '" alt="" class="img_small"/><input type="hidden" value="' . $key . '" id="field-media_gallery" name="media_gallery[]" /></li>';
            }
        }
        return $data;
    }

    public function set_post_status()
    {
        $this->M_base_config->cekaAuth();
        if ($_POST) {
            $post_id = $this->input->post('id');
            $post_status = $this->input->post('status');
            $data = array('post_status' => $post_status);
            $this->db->where('post_id', $post_id);
            $this->db->update('tb_post', $data);
        }
    }

    /*
   ============================================
   Record User Notification callback
   ============================================
   */
    public function notification_user_after_insert($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_update($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i>  ' . lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_delete($primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

}