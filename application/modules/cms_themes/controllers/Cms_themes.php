<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_themes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('theme_front');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('Themes');
            $umbrella->columns('setting_desc', 'setting_name', 'setting_value');
            $umbrella->where('setting_type', 'front-theme');
            $umbrella->order_by('setting_id', 'desc');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->unset_delete();
            $umbrella->unset_edit_fields('setting_name');
            $umbrella->unset_edit_fields('setting_type');
            $umbrella->field_type('setting_value', 'enum', array('active', 'deactive'));
            $umbrella->field_type('setting_type', 'hidden', 'front-theme');
            $umbrella->set_field_upload('setting_desc', 'assets/uploads/');
            $umbrella->unique_fields('setting_name');
            $umbrella->display_as('setting_name', lang('theme_name'));
            $umbrella->display_as('setting_value', lang('theme_status'));
            $umbrella->display_as('setting_desc', lang('theme_pic'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_after_insert(array($this, '_set_callback_after_insert'));
            $umbrella->callback_after_update(array($this, '_set_callback_after_update'));
            $umbrella->callback_column('setting_desc', array($this, '_setting_desc'));
            $umbrella->callback_column('setting_value', array($this, '_theme_status'));
            $umbrella->set_bulkactionfalse(true);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _setting_desc($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<div class="text-left"><a href="' . base_url() . 'img/load/100/100/full/' . $value . '" class="image-thumbnail"><img src="' . base_url() . 'img/load/80/80/png/' . $value . '" class="img_small" alt=""></a></div>';
    }

    public function _set_callback_before_insert($post_array, $primary_key)
    {
        $post_array['setting_name'] = preg_replace('/[^a-zA-Z0-9]/', '-', $post_array['setting_name']);
        return $post_array;
    }

    public function _set_callback_before_update($post_array, $primary_key)
    {
        $post_array['setting_name'] = preg_replace('/[^a-zA-Z0-9]/', '-', $post_array['setting_name']);
        return $post_array;

    }

    public function _set_callback_after_insert($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $this->db->where('setting_type', 'front-theme');
        $query = $this->db->get('tb_setting')->result();
        if ($post_array['setting_value'] == 'active') {
            foreach ($query as $val) {
                if ($val->setting_id != $primary_key) {
                    $this->db->set('setting_value', 'deactive');
                    $this->db->where('setting_id', $val->setting_id);
                    $this->db->update('tb_setting');
                }
            }
        }
        return $post_array;
    }

    public function _set_callback_after_update($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $this->db->where('setting_type', 'front-theme');
        $query = $this->db->get('tb_setting')->result();
        if ($post_array['setting_value'] == 'active') {
            foreach ($query as $val) {
                if ($val->setting_id != $primary_key) {
                    $this->db->set('setting_value', 'deactive');
                    $this->db->where('setting_id', $val->setting_id);
                    $this->db->update('tb_setting');
                }
            }
        }
        return $post_array;
    }

    public function _theme_status($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->setting_value == 'active') {
            $status = '<span class="uk-badge uk-badge-success">' . $row->setting_value . '</span>';
        } else {
            $status = '<span class="uk-badge ">' . $row->setting_value . '</span>';
        }
        return $status;
    }
}