<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_Input $input
 * @property CI_User_agent $agent
 * @property CI_Email $email
 * @property Mahana_hierarchy $mahana_hierarchy
 * @property CI_Form_validation $form_validation
 * @property CI_Session session
 * @property CI_Parser parser
 * @property Front front
 * @property CI_Upload upload
 */
use Lullabot\AMP\AMP;
use Lullabot\AMP\Validate\Scope;

class Home extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
        $this->load->model('front');
        //redirect('/cms');
        //wp_function();
    }

	public function _remap()
	{
		$segment_1 = $this->uri->segment(1);
		$segment_2 = $this->uri->segment(2);
        switch($segment_1){
        	case null:
			case false:
            case '':
                $this->index();
                break;
            case 'amp':
                $this->amp();
                break;
            case 'category':
                $this->category();
                break;
            case 'send-comment':
                $this->send_comment();
                break;
            case 'kirim-email':
                $this->kirim_email();
                break;
		    case ($segment_1 != 'category'):
		    default:
		    	$this->single($segment_1);
		        break;
		}
	}

	protected function amp()
    {
        $amp = new AMP();

        $html =
            '<p><a href="javascript:run();">Run</a></p>';

// Load up the HTML into the AMP object
// Note that we only support UTF-8 or ASCII string input and output. (UTF-8 is a superset of ASCII)
        $amp->loadHtml($html);

// If you're feeding it a complete document use the following line instead
// $amp->loadHtml($html, ['scope' => Scope::HTML_SCOPE]);

// If you want some performance statistics (see https://github.com/Lullabot/amp-library/issues/24)
// $amp->loadHtml($html, ['add_stats_html_comment' => true]);

// Convert to AMP HTML and store output in a variable
        $amp_html = $amp->convertToAmpHtml();

// Print AMP HTML
        print($amp_html);

// Print validation issues and fixes made to HTML provided in the $html string
        //print($amp->warningsHumanText());
        exit();
    }

    function send_comment(){
        $form = $this->input->post('form');
        parse_str($form, $data);
        $post_id = $data['post_id'];
        $feed_author = $data['feed_author'];
        $feed_author_email = $data['feed_author_email'];
        $feed_author_url = $data['feed_author_url'];
        $feed_content = $data['feed_content'];

        $insert_user_feeds = array(
            'feed_parent'       => $post_id,
            'feed_author'       => $feed_author,
            'feed_author_email' => $feed_author_email,
            'feed_author_url'   => $feed_author_url,
            'feed_content'      => $feed_content,
            'feed_status'       => 'pending',
            'feed_type'         => 'comments',
            'feed_date'         => date('Y-m-d h:i:s'),
            'feed_ip'           => $this->input->ip_address(),
            'feed_agent'        => $this->agent->browser(),
            'feed_user_id'      => 2,
        );
        $return = $this->db->insert('tb_users_feeds', $insert_user_feeds);
        if($return){
            echo 'Komentar anda menunggu proses validasi oleh Admin';
        }else{
            echo 'Komentar anda gagal dikirim.';
        }

        exit();
    }

    public function single($post_name=null){
        $data['company']            = $this->get_company();
        $data['posts'] 				= $this->front->get_post_sigle('posts',$post_name);
        $data['company']            = $this->get_company();
        $data['commentall']         = $this->get_comment_all();
        $data['populer_posts'] 		= $this->front->get_post_parse(2,0,'posts','post_view','desc');
        $data['primary_menu']     = $this->front->get_navmenu();
        $data['footer_menu']      = $this->front->get_navmenu('Footer Menu');
        if(empty($data['posts'])){
            show_404();
        }
        $data_front_setting	        = $this->base_config->front_setting();
        $data_seo_new 				= $this->front->get_post_seo('posts',$post_name);
        $sidebar                    = $this->sidebar();
        $path_asset                 = $this->path_asset();
        $data['widget']             = array();
        $data                       = array_merge($data, $data_front_setting, $data_seo_new, $sidebar, $path_asset);
        //Render Output
        view_front('pages.single', $data);
    }

    protected function get_category_list($limit = null, $tipe = 'category'){
        return $this->front->get_category_list($limit, $tipe);
    }

    protected function sidebar(){
        //SIDEBAR PARSE
        $data['category_list']  = $this->get_category_list();
        $data['archive_list']   = $this->get_archive_list();
        $data['tag_list']       = $this->get_category_list('10', 'tags');
        $data['our_gallery']    = $this->get_where_category('gallery', 6);
        return $data;
    }

    protected function get_archive_list($limit = null){
        return $this->front->get_archive_list($limit);
    }

    protected function get_post_list($get_category, $per_page, $hal){
        $post_list               = array(
            'category_slug' => $get_category,
            'order_by'      => 'tb_post.post_view',
            'order_by_value'=> 'desc',
            'limit'         => $per_page,
            'offset'        => $hal,
        );
        $return       = $this->front->get_post_wherecategory($post_list);
        return $return;
    }

    protected function get_comment_all(){
        $commentdata                = array(
            'where'         => array(array('wherefield'=>'feed_status','where_value'=>'approved'),array('wherefield'=>'feed_type','where_value'=>'comments')),
            'order_by'      => 'feed_date',
            'order_by_value'=> 'desc',
            'limit'         => 3
        );
        return $this->front->get_comment_all($commentdata);
    }

    protected function get_pagination($get_category, $per_page, $total_posts){
        $config['base_url']         = base_url().'category/'.$get_category;
        $config['per_page']         = $per_page;
        $config['total_rows']       = $total_posts;
        $config['full_tag_open']    = '<ul class="pagination pagination-lg">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = '';
        $config['last_link']        = '';
        $config['first_tag_open']   = "<ul class=\"pagination pagination-lg\">";
        $config['first_tag_close']  = '</ul>';
        $config['prev_link']        = '<i class="fa fa-long-arrow-left"></i>Prev';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = 'Next<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    protected function get_total_post($get_category){
        $total_garage_posts        = array(
            'category_slug' => $get_category,
            'order_by'      => 'tb_post.post_view',
            'order_by_value'=> 'desc',
            'get_total'     => true,
        );
        return $this->front->get_post_wherecategory($total_garage_posts);
    }

    public function category(){
        $data_front_setting    = $this->base_config->front_setting();
        $get_category          = $this->uri->segment(2);
        $hal                   = $this->uri->segment(3);
        $per_page              = 5;
        $sidebar               = $this->sidebar();
        $data_seo_new          = $this->front->get_category_seo($get_category);
        $data['post_list']     = $this->get_post_list($get_category, $per_page, $hal);
        $data['total_posts']   = $this->get_total_post($get_category);
        $data['pagging']       = $this->get_pagination($get_category, $per_page, $data['total_posts']);
        $data['company']       = $this->get_company();
        $data['commentall']    = $this->get_comment_all();
        $data['populer_posts'] = $this->front->get_post_parse(2,0,'posts','post_view','desc');
        $data['post_name']     = 'category/'.$get_category;
        $data['primary_menu']  = $this->front->get_navmenu();
        $data['footer_menu']   = $this->front->get_navmenu('Footer Menu');
        $path_asset            = $this->path_asset();
        $data                  = array_merge($data, $data_front_setting, $data_seo_new, $sidebar, $path_asset);

        view_front('pages.category', $data);
    }

    public function kirim_email()
    {
        $first_name = $this->input->post('first_name', TRUE);
        $last_name  = $this->input->post('last_name', TRUE);
        $email      = $this->input->post('email', TRUE);
        $message    = $this->input->post('message', TRUE);

        $tb_users_feeds = array(
            'feed_author'       => $first_name.' '.$last_name,
            'feed_author_email' => $email,
            'feed_content'      => $message,
            'feed_status'       => 'pending',
            'feed_type'         => 'contact',
            'feed_date'         => date('Y-m-d h:m:s'),
            'feed_ip'           => $this->input->ip_address(),
            'feed_agent'        => $this->agent->browser().' '.$this->agent->version(),

        );
        $this->db->insert('tb_users_feeds', $tb_users_feeds);

        // KIRIM EMAIL
/*        $this->load->library('email');
        $this->email->from($email, $first_name.' '.$last_name);
        $this->email->to('gitsolution.pt@gmail.com');
        $this->email->subject('Kontak Picksite');
        $this->email->message($message);
        $this->email->send();*/

        redirect('','refresh');
    }

    protected function get_where_category($category_slug, $limit = null, $offset = 0){
        $array                     = array(
            'category_slug' => $category_slug,
            'order_by'      => 'tb_post.post_date',
            'order_by_value'=> 'desc',
            'limit'         => $limit,
            'offset'        => $offset,
        );
        return $this->front->get_post_wherecategory($array);
    }

    protected function get_company(){
        $company                  = array(
            'where' => array(array('wherefield'=>'setting_type','where_value'=>'setting_company'))
        );
        return $this->front->get_multi_setting_sigle($company);
    }

    protected function path_asset(){
        $data['base_url']           = base_url();
        $data['asset']              = $this->base_config->asset_front();
        return $data;
    }

	public function index()
	{
	    $data = array();
        $data_front_setting       = $this->base_config->front_setting();
        $data['slider']           = $this->get_where_category('slider', 10);
        $data['template']         = $this->get_where_category('template', 8);
        $data['features']         = $this->get_where_category('features', 4);
        $data['testimonial']      = $this->get_where_category('testimonial', 2);
        $data['pricing']          = $this->get_where_category('pricing', 10);
        $data['partner']          = $this->get_where_category('partner', 5);
        $data['client']           = $this->get_where_category('client', 6);

        $data['desc_feature']     = $data_front_setting['site_feature'];
        $data['desc_template']    = $data_front_setting['site_template'];
        $data['desc_pricing']     = $data_front_setting['site_pricing'];
        $data['desc_testimonial'] = $data_front_setting['site_testimonial'];
        $data['desc_client']      = $data_front_setting['site_client'];
        $data['desc_partner']     = $data_front_setting['site_partner'];
        $data['company']          = $this->get_company();
        $data['primary_menu']     = $this->front->get_navmenu();
        $data['footer_menu']      = $this->front->get_navmenu('Footer Menu');
        $data['kontak_kami'] = $this->front->get_kontak_kami();
        $data['menu_active'] = 'Beranda';
        $path_asset               = $this->path_asset();
        $data                     = array_merge($data, $data_front_setting, $path_asset);
        view_front('pages.beranda', $data);
	}

	public function sigle($post_name=null)
	{
	  	
        $data_seo					=$this->base_config->front_setting();
        #=================================
        # Get Post Data sigle
        #=================================
        $this->load->model('front');
        $data_seo_new 				= $this->front->get_post_seo('posts',$post_name);
        $data = array_merge($data_seo,$data_seo_new);
        $data['posts'] 				= $this->front->get_post_sigle('posts',$post_name);
        if(empty($data['posts'])){
        	show_404();
        }
        #=================================
        # Get Post Data by views
        #=================================
        $data['populer_posts'] 		= $this->front->get_post_parse(2,0,'posts','post_view','desc');
        $data['asset']              = $this->base_config->asset_front();
     	$data['base_url']           = base_url();
     	//$this->parser->parse('_v_head',$data);
        //$this->parser->parse('_v_sigle',$data);
        //$this->parser->parse('_v_footer',$data);
       //$this->output->enable_profiler(TRUE);
	}
}
  