<?php 
class Front extends CI_Model  {
	
	public function __construct()
    {
    	parent::__construct();
    	date_default_timezone_set('Asia/Jakarta');
        $this->load->library('mahana_hierarchy');

    }
    public function get_post_parse($limit=null,$offset=null,$type=null,$orderby=null,$orderbyvalue=null, $category=null){
    	$return = array();
    	$tmp 	= array();

    	if($limit != null && $offset !=null){
    		$limit			= $limit;
    		$offset			=$offset;
    	}else {
    		$limit			= 0;
    		$offset			= 10;
    	}
    	if($orderby != null){
    		$orderby 		= $orderby;
    		$orderbyvalue   = $orderbyvalue;
    	}else{
    		$orderby 		= 'post_date';
    		$orderbyvalue	= 'desc' ;
    	}	

        if($category){
            $this->db->join('tb_terms', 'tb_terms.post_id = tb_post.post_id AND terms_type = "category"');
            $this->db->join('tb_category', 'tb_category.category_id = tb_terms.category_id AND tb_category.category_name = "Slider"');
        }

    	$this->db->where('post_status', 'Publish');
        $this->db->where('post_type', $type);
        $this->db->order_by($orderby,$orderbyvalue);
        $dataonlypost    	= $this->db->get('tb_post',$offset,$limit)->result();
        foreach($dataonlypost as $name){
        	$tmp['post_id'] 					= $name->post_id;
        	$tmp['post_name'] 					= $name->post_name;
        	$tmp['post_title'] 					= $name->post_title;
        	$tmp['post_content'] 				= $name->post_content;
        	$tmp['post_priority'] 				= $name->post_priority;
        	$tmp['post_date'] 					= $name->post_date;
        	$tmp['post_modified'] 				= $name->post_modified;
        	$tmp['post_seo_title'] 				= $name->post_seo_title;
        	$tmp['post_meta_desc'] 				= $name->post_meta_desc;
        	$tmp['post_meta_keyword'] 			= $name->post_meta_keyword;
        	$tmp['post_comment'] 				= $name->post_comment;
        	$tmp['post_type'] 					= $name->post_type;
        	$tmp['post_author'] 				= $name->post_author;
        	$tmp['post_view'] 					= $name->post_view;
        	$tmp['post_comment_count'] 			= $name->post_comment_count;
        	$image = $this->db->where('post_id',$name->post_parent)->get("tb_post",1)->result();
        	foreach($image as $img) {
        		$tmp['post_img'] 				= $img->post_name;
        	}
        	$user = $this->db->where('username',$name->post_author)->get("tb_user",1)->result();
        	foreach($user as $user) {
        		$tmp['post_user_display_name'] 	= $user->user_display_name;
        		$tmp['post_user_avatar'] 		= $user->user_avatar;
        	}
        	$return[] = $tmp;
        	unset($tmp);
        }
        
        return $return;
    }
    public function get_post_sigle($type=null,$slug=null){
    	$return = array();
    	$tmp 	= array();
    	$seo 	= $data=$this->base_config->front_setting();
    	$this->db->where('post_status', 'Publish');
    	$this->db->where('post_type',$type);
        $this->db->where('post_name',$slug);
        $dataonlypost    	= $this->db->get('tb_post')->result();
        foreach($dataonlypost as $name){
        	$tmp['post_id'] 					= $name->post_id;
        	$tmp['post_title'] 					= $name->post_title;
        	$tmp['post_name'] 					= $name->post_name;
        	$tmp['post_content'] 				= $name->post_content;
        	$tmp['post_priority'] 				= $name->post_priority;
        	$tmp['post_date'] 					= $name->post_date;
        	$tmp['post_modified'] 				= $name->post_modified;
        	$tmp['post_seo_title'] 				= $name->post_seo_title;
        	$tmp['post_meta_desc'] 				= $name->post_meta_desc;
        	$tmp['post_meta_keyword'] 			= $name->post_meta_keyword;
        	$tmp['post_comment'] 				= $name->post_comment;
        	$tmp['post_type'] 					= $name->post_type;
        	$tmp['post_author'] 				= $name->post_author;
        	$tmp['post_view'] 					= $name->post_view;
        	$tmp['post_comment_count'] 			= $name->post_comment_count;
        	$image = $this->db->where('post_id',$name->post_parent)->get("tb_post",1)->result();
        	foreach($image as $img) {
        		$tmp['post_img'] 				= $img->post_name;
        	}
        	$user = $this->db->where('username',$name->post_author)->get("tb_user",1)->result();
        	foreach($user as $user) {
                $tmp['post_user_display_name'] 	= $user->user_display_name;
                $tmp['post_user_avatar'] 		= $user->user_avatar;
                $tmp['user_bio']                = $user->user_bio;
                $tmp['user_facebook']           = $user->user_facebook;
                $tmp['user_twitter']            = $user->user_twitter;
                $tmp['user_google_plus']        = $user->user_google_plus;
        	}

            $this->db->select('category_name');
            $this->db->where('terms_type', 'tags');
            $this->db->where('post_id', $name->post_id);
            $this->db->join('tb_category', 'tb_category.category_id = tb_terms.category_id AND category_type = "tags"');
            $tmp['tags'] = $this->db->get('tb_terms')->result();
            $tmp['comments'] = $this->get_comment_where_post($tmp['post_id']);

        	$return[] = $tmp;
        	unset($tmp);
        }
        
        return $return;
    }

    function get_comment_where_post($post_id){
        $return = $this->db
            ->join('tb_user', 'tb_user.id = tb_users_feeds.feed_user_id', 'LEFT')
            ->where('feed_type', 'comments')
            ->where('feed_status', 'approved')
            ->where('feed_parent', $post_id)
            ->get('tb_users_feeds');
        return $return->result();
    }

    public function get_post_seo($type=null,$slug=null){
    	$return = array();
    	$tmp 	= array();
    	$seo 	= $data=$this->base_config->front_setting();
    	$this->db->where('post_status', 'Publish');
    	$this->db->where('post_type',$type);
        $this->db->where('post_name',$slug);
        $dataonlypost    	= $this->db->get('tb_post')->result();
        foreach($dataonlypost as $name){
        	if(!empty($name->post_seo_title))
        		$tmp['site_title'] 				= $name->post_seo_title;
        	if(!empty($name->post_meta_desc))
        		$tmp['site_desc'] 				= $name->post_meta_desc;
        	if(!empty($name->post_meta_keyword))
        		$tmp['site_keyword'] 			= $name->post_meta_keyword;
        	$return = $tmp;
        	unset($tmp);
        }
        return $return;
    }
    public function get_post_total($table=null,$type=null){
    	$this->db->where('post_status', 'Publish');
    	if($type != null)
        $this->db->where('post_type', $type);
        return $this->db->count_all_results($table);
    }

    public function get_navmenu($setting_name = 'Primary Menu'){
        $this->db->where('setting_type','front-customize');
        $this->db->where('setting_name', $setting_name);
        $idmenu = $this->db->get('tb_setting',1)->row('setting_value');
        return $this->getmenulist($idmenu);
    }

    function get_kontak_kami()
    {
        $post = $this->db->where('post_id', 323)->get('tb_post')->result();

        return $this->get_full_post_image($post);
    }

    public function get_post_wherecategory($param){
        if(isset($param['get_total'])){
            $category_id = $this->db->where('category_slug',$param['category_slug'])->get('tb_category',1)->row('category_id');
            if(array_key_exists('where', $param)){
                for($i=0;$i<count($param['where']);$i++){
                    $this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
                }
            }
            $post    = $this->db->where('category_id',$category_id)
                ->join('tb_post','tb_post.post_id = tb_terms.post_id')
                ->where('tb_post.post_status','Publish')
                ->order_by($param['order_by'].' '.$param['order_by_value'])
                ->count_all_results('tb_terms');
            return $post;
        }else {
            $category_id = $this->db->where('category_slug',$param['category_slug'])->get('tb_category',1)->row('category_id');
            if(array_key_exists('where', $param)){
                for($i=0;$i<count($param['where']);$i++){
                    $this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
                }
            }
            $post    = $this->db->where('category_id',$category_id)
                ->join('tb_post','tb_post.post_id = tb_terms.post_id')
                ->where('tb_post.post_status','Publish')
                ->order_by($param['order_by'].' '.$param['order_by_value'])
                ->limit($param['limit'],$param['offset'])
                ->get('tb_terms')->result();
            return $this->get_full_post_image($post);
            }
    }

    public function get_full_post_image($dataonlypost){
        $return = array();
        $tmp    = array();
        foreach($dataonlypost as $name){
            $tmp['post_id']                     = $name->post_id;
            $tmp['post_name']                   = $name->post_name;
            $tmp['post_title']                  = $name->post_title;
            $tmp['post_content']                = $name->post_content;
            $tmp['post_priority']               = $name->post_priority;
            $tmp['post_date_day']               = date('d', strtotime($name->post_date));
            $tmp['post_date_month']             = date('M', strtotime($name->post_date));
            $tmp['post_date_years']             = date('yyyy', strtotime($name->post_date));
            $tmp['post_date']                   = $name->post_date;
            $tmp['post_modified']               = $name->post_modified;
            $tmp['post_seo_title']              = $name->post_seo_title;
            $tmp['post_meta_desc']              = $name->post_meta_desc;
            $tmp['post_meta_keyword']           = $name->post_meta_keyword;
            $tmp['post_comment']                = $name->post_comment;
            $tmp['post_type']                   = $name->post_type;
            $tmp['post_author']                 = $name->post_author;
            $tmp['post_view']                   = $name->post_view;
            $tmp['post_comment_count']          = $name->post_comment_count;
            $image = $this->db->where('post_id',$name->post_parent)->get("tb_post",1)->result();
            if($image){
                foreach($image as $img) {
                    $tmp['post_img']                = $img->post_name;
                }
            }else{
                $tmp['post_img'] = $name->post_parent;
            }

            $user = $this->db->where('username',$name->post_author)->get("tb_user",1)->result();
            foreach($user as $user) {
                $tmp['user_display_name']  = $user->user_display_name;
                $tmp['user_avatar']        = $user->user_avatar;
                $tmp['username']        = $user->username;
                $tmp['user_company']        = $user->user_company;
                $tmp['user_bio']        = $user->user_bio;
                $tmp['user_facebook']        = $user->user_facebook;
                $tmp['user_twitter']        = $user->user_twitter;
                $tmp['user_google_plus']        = $user->user_google_plus;
            }
            $return[] = $tmp;
            unset($tmp);
        }
        return $return;
    }

    public function get_comment_all($param){
        $return = array();
        $tmp    = array();
        if(array_key_exists('where', $param)){
            for($i=0;$i<count($param['where']);$i++){
                $this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
            }
        }
        if(array_key_exists('order_by', $param)){
            $this->db->order_by($param['order_by'].' '.$param['order_by_value']);
        }
        $feedonly = $this->db->get('tb_users_feeds',$param['limit'])->result();
        foreach($feedonly as $feed){
            $tmp['feed_id']                     = $feed->feed_id;
            $tmp['feed_author']                 = $feed->feed_author;
            $tmp['feed_author_email']           = $feed->feed_author_email;
            $tmp['feed_author_url']             = $feed->feed_author_url;
            $tmp['feed_content']                = $feed->feed_content;
            $tmp['feed_status']                 = $feed->feed_status;
            $tmp['feed_date_show']              = $this->base_config->timeAgo($feed->feed_date);
            $tmp['feed_date']                   = $feed->feed_date;
            $tmp['feed_ip']                     = $feed->feed_ip;
            $tmp['feed_user_id']                = $feed->feed_user_id;
            $this->db->where('id',$feed->feed_user_id);
            $user = $this->db->get('tb_user',1)->result();
            foreach($user as $user) {
                $tmp['username']                =  $user->username;
                $tmp['user_display_name']       =  $user->user_display_name;
                $tmp['user_avatar']             =  $user->user_avatar;
                $tmp['user_company']            =  $user->user_company;
            }
            $return[] = $tmp;
            unset($tmp);
        }
        return $return;
    }

    public function get_category_seo($category_slug=null){
        $return = array();
        $tmp    = array();
        $seo    = $data=$this->base_config->front_setting();
        $this->db->where('category_slug',$category_slug);
        $dataonlypost       = $this->db->get('tb_category')->result();

        foreach($dataonlypost as $name){
            if(!empty($name->category_name))
                $tmp['site_title']              = $name->category_name.' - '.$seo['site_title'];
            if(!empty($name->category_name))
                $tmp['site_desc']               = $name->category_name.' - '.$seo['site_desc'];
            $return = $tmp;
            unset($tmp);
        }
        return $return;
    }

    function get_archive_list($limit = null){
        if($limit){
            $this->db->limit($limit);
        }
        $return = $this->db
            ->select('DATE_FORMAT(post_date, "%M %Y") as bulan, COUNT(post_date) as jumlah')
            ->where('post_type', 'posts')
            ->group_by('YEAR(post_date)')
            ->group_by('MONTH(post_date)')
            ->get('tb_post');
        return $return->result();
    }

    function get_category_list($limit = null, $tipe = 'category'){
        if($limit){
            $this->db->limit($limit);
        }
        $return = $this->db
            ->select('tb_category.*')
            ->select('COUNT(terms_id) AS jumlah')
            ->join('tb_terms', "tb_terms.category_id = tb_category.category_id AND terms_type = '".$tipe."'")
            ->where('category_type', $tipe)
            ->group_by('tb_category.category_id')
            ->get('tb_category');
        return $return->result();
    }

    public function getmenulist($menus_type =null){
        $data = $this->mahana_hierarchy->where(array('category_slug'=>$menus_type,'category_deep'=>0))->get();  
        $listmenu = "";
        $getchild = "";
        for($x=0; $x<count($data); $x++) {
            if($data[$x]['category_type'] == 1) {
                $this->db->where('category_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus ='category_id';
                $name_slug ='category_slug';
                $name_plus ='category_name';
                $parenttype = 1;
            }else if($data[$x]['category_type'] == 2) {
                $this->db->where('category_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus ='category_id';
                $name_slug ='category_slug';
                $name_plus ='category_name';
                $parenttype = 2;
            }else {
                $this->db->where('post_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_post')->result();
                $idplus ='post_id';
                $name_slug ='post_name';
                $name_plus ='post_title';
                $parenttype = 3;
            }

            foreach($getcat as $cat) {
                $children = $this->gethierarchychild($data[$x]['category_id']);
                if(!empty($children)){
                    $classparent = "uk-parent";
                    $getchild = $children;
                }else {
                    $classparent = "";
                    $getchild = "";
                }
                if( $parenttype == 3 ){
                     $listmenu.='<li><a href="'.$cat->$name_slug.'"><span>'.$cat->$name_plus.'</span></a>';
                }elseif( $parenttype == 2 )
                {
                    $listmenu.='<li><a href="'.$cat->category_desc.'"><span>'.$cat->$name_plus.'</span></a>';
                }else{
                    $listmenu.='<li><a href="category/'.$cat->$name_slug.'"><span>'.$cat->$name_plus.'</span></a>';
                }
              
            }
            $listmenu.=$getchild;
            $listmenu.='</li>';
           
        }
       return $listmenu;
    }

    public function gethierarchychild($parent_id){
       $data= $this->mahana_hierarchy->get_children($parent_id);
       $getchild ="";
       if(!empty($data)) {
       $listmenu = '<div class="sub-main-menu"><ul class="sub-menu nav nav-tabs">';
       for($x=0; $x<count($data); $x++) {
            if($data[$x]['category_type'] == 1) {
                $this->db->where('category_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus ='category_id';
                $name_slug ='category_slug';
                $name_plus ='category_name';
                $parenttype = 1;
            }else if($data[$x]['category_type'] == 2) {
                $this->db->where('category_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus ='category_id';
                $name_slug ='category_slug';
                $name_plus ='category_name';
                $parenttype = 2;
            }else {
                $this->db->where('post_id',$data[$x]['category_name']);
                $getcat = $this->db->get('tb_post')->result();
                $idplus ='post_id';
                $name_slug ='post_name';
                $name_plus ='post_title';
                $parenttype = 3;
            }
            foreach($getcat as $cat) {
                $children = $this->gethierarchychild($data[$x]['category_id']);
                if(!empty($children)){
                    $classparent = "uk-parent";
                    $getchild = $children;
                }else {
                    $classparent = "";
                    $getchild = "";
                }
               if($parenttype == 3){
                     $listmenu.='<li><a href="'.$cat->$name_slug.'"><span>'.$cat->$name_plus.'</span></a>';
                }else {
                    $listmenu.='<li><a href="category/'.$cat->$name_slug.'"><span>'.$cat->$name_plus.'</span></a>';
                }
            }
            $listmenu.=$getchild;
            $listmenu.='</li>';
        }
       $listmenu.=' </ul></div>';
        return $listmenu;
       }
    }

    public function get_client()
    {   
        return $this->db
        ->where('setting_type', 'setting_mitra')
        ->get('tb_setting')->result();
    }

    public function get_template()
    {   
        return $this->db
        ->where('setting_type', 'setting_template')
        ->get('tb_setting')->result();
    }

    public function get_multi_setting_sigle($param){
        $return = array();
        $tmp    = array();
        if(array_key_exists('where', $param)){
            for($i=0;$i<count($param['where']);$i++){
                $this->db->where($param['where'][$i]['wherefield'],$param['where'][$i]['where_value']);
            }
        }
        $data_setting = $this->db->get('tb_setting')->result();
        foreach ($data_setting as $setting) {
            $tmp[$setting->setting_desc]   = $setting->setting_value;
        }
        return $tmp;
    }

    public function get_commpany_setting($value){
	    $this->db->where('setting_type','setting_company');
	    $this->db->where('setting_desc',$value);
	    return $this->db->get('tb_setting')->row('setting_value') ;
    }
}
