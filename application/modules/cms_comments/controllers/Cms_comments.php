<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_comments extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('comment');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_users_feeds');
            $umbrella->set_subject('Comments');
            $umbrella->columns('feed_author', 'feed_content', 'feed_parent', 'feed_date', 'feed_type');
            $umbrella->where('feed_type', 'comments');
            $umbrella->order_by('feed_id', 'desc');
            $umbrella->unset_texteditor('post_title', 'feed_content');
            $umbrella->field_type('feed_type', 'hidden');
            $umbrella->field_type('feed_ip', 'hidden');
            $umbrella->field_type('feed_agent', 'hidden');
            $umbrella->display_as('feed_parent', lang('com_parent'));
            $umbrella->display_as('feed_author', lang('com_author'));
            $umbrella->display_as('feed_author_email', lang('com_email'));
            $umbrella->display_as('feed_type', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->display_as('feed_author_url', lang('com_url'));
            $umbrella->display_as('feed_content', lang('com_content'));
            $umbrella->display_as('feed_status', lang('com_status'));
            $umbrella->display_as('feed_date', lang('com_date'));
            $umbrella->display_as('feed_user_id', lang('com_user'));
            $umbrella->set_relation('feed_parent', 'tb_post', 'post_title');
            $umbrella->set_relation('feed_user_id', 'tb_user', 'id');
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('feed_date', array($this, '_feed_date'));
            $umbrella->callback_column('feed_type', array($this, '_callback_feed_id'));
            $umbrella->callback_column('feed_author', array($this, '_callback_feed_author'));
            $umbrella->callback_after_insert(array($this, 'notification_user_after_insert'));
            $umbrella->callback_after_update(array($this, 'notification_user_after_update'));
            $umbrella->callback_after_delete(array($this, 'notification_user_after_delete'));
            $umbrella->set_composer(false);
            $umbrella->set_listfilterfeed(true);
            $typefilter = $this->uri->segment(4);
            if ($typefilter == "approved" || $typefilter == "pending" || $typefilter == "spam" || $typefilter == "trash") {
                $umbrella->where('tb_users_feeds.feed_status', $typefilter);
            }
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */

    public function _set_callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['feed_status']))
            $post_array['feed_status'] = "Pending";
        if (empty($post_array['feed_ip']))
            $post_array['feed_ip'] = $this->input->ip_address();
        if (empty($post_array['feed_agent'])) {
            $this->load->library('user_agent');

            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser() . ' ' . $this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }
            $post_array['feed_agent'] = $agent;
        }
        if (empty($post_array['feed_date']))
            $post_array['feed_date'] = date("Y-m-d H:i:s");

        $post_array['feed_type'] = 'comments';

        return $post_array;
    }

    public function _set_callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['feed_status']))
            $post_array['feed_status'] = "Pending";
        if (empty($post_array['feed_ip']))
            $post_array['feed_ip'] = $this->input->ip_address();
        if (empty($post_array['feed_agent'])) {
            $this->load->library('user_agent');

            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser() . ' ' . $this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }
            $post_array['feed_agent'] = $agent;
        }
        if (empty($post_array['feed_date']))
            $post_array['feed_date'] = date("Y-m-d H:i:s");

        $post_array['feed_type'] = 'comments';

        return $post_array;
    }

    public function _feed_date($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->feed_status == 'approved') {
            $status = '<span class="uk-badge uk-badge-success">' . $row->feed_status . '</span>';
        } else if ($row->feed_status == 'trash' || $row->feed_status == 'spam') {
            $status = '<span class="uk-badge uk-badge-danger">' . $row->feed_status . '</span>';
        } else {
            $status = '<span class="uk-badge uk-badge-warning">' . $row->feed_status . '</span>';
        }
        return $value . "<br>" . $status;
    }

    public function _callback_feed_author($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $data = array(
            'table' => 'tb_user',
            'where' => array(array('wherefield' => 'id', 'where_value' => $row->feed_user_id)),
            'return' => 'user_avatar',
        );
        $avatar = $this->M_base_config->getSimpleData($data);
        $value = '<ul class="md-list md-list-addon uk-margin-bottom"><li><div class="md-list-addon-element"><img class="md-user-image md-list-addon-avatar" src="img/load/70/70/png/' . $avatar . '" alt=""></div><div class="md-list-content"><span class="md-list-heading">' . $row->feed_author . '</span><span class="uk-text-small uk-text-muted">' . $row->feed_author_email . '<br>' . $row->feed_author_url . '-' . $row->feed_ip . '</span></div></li></ul>';
        return $value;
    }

    public function _callback_feed_id($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->feed_id . '" name="' . $row->feed_id . '" id="' . $row->feed_id . '" data-md-icheck />';
    }

    public function set_feed_status()
    {
        $this->M_base_config->cekaAuth();
        if ($_POST) {
            $post_id = $this->input->post('id');
            $post_status = $this->input->post('status');
            $data = array('feed_status' => $post_status);
            $this->db->where('feed_id', $post_id);
            $this->db->update('tb_users_feeds', $data);
        }
    }

    /*
      ============================================
      Record User Notification callback
      ============================================
      */
    public function notification_user_after_insert($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('add_notif'), 'icon' => 'add', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_update($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('update_notif'), 'icon' => 'update', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/comments/index/edit/' . $primary_key, 'title' => $post_array['feed_content']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_delete($primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }
}