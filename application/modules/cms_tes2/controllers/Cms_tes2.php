<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config M_base_config
 * @property  base_config base_config
 * @property  Ion_auth|Ion_auth_model ion_authadmin_gitscms
 * @property  CI_Lang lang
 * @property  CI_URI uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config config
 * @property  CI_Input input
 * @property  CI_User_agent $agent
 * @property  Slug slug
 * @property  CI_Security security
 * @property  Setting setting
 * @property  CI_Parser parser
 */
class Cms_tes2 extends MX_Controller
{
    protected $table = 'tb_post';
    protected $subject = 'Data Tes2';
    protected $module;

    public function __construct()
    {
        parent::__construct();
        $this->module = str_replace('cms_', '', strtolower( get_class($this) ) );
        date_default_timezone_set('Asia/Jakarta');
    }

    protected function role()
    {
        $this->M_base_config->cekaAuth();
        if( $this->base_config->groups_access_sigle('menu', $this->module) ) show_404();
    }

    protected function get_primary_key()
    {
        $fields = $this->get_fields();
        $filter = array_filter($fields,function ($element){
            if(isset( $element->primary_key ) && $element->primary_key == 1) return true;
            return false;
        });
        return reset($filter);
    }

    protected function required()
    {
        return [];
    }

    protected function where()
    {
        $where = null;
        //$where[] = ['product_name','saya'];
        //$where[] = ['product_desc','saya'];
        return $where;
    }

    protected function get_enum ($table_name, $field_name)
    {
        $sql = "desc {$table_name} {$field_name}";
        $st = $this->db->query($sql);

        if ($st->result())
        {
            $row = $st->row();
            if ($row === FALSE)
                return FALSE;

            $type_dec = $row->Type;
            if (substr($type_dec, 0, 5) !== 'enum(')
                return FALSE;

            $values = array();
            foreach(explode(',', substr($type_dec, 5, (strlen($type_dec) - 6))) AS $v)
            {
                array_push($values, trim($v, "'"));
            }

            return $values;
        }
        return FALSE;
    }

    protected function get_fields()
    {
        $show = array(); //show field
        $fields = null;
        foreach ($this->db->field_data($this->table) as $k => $item) {
            if( count($show) > 0 ){
                if( in_array($item->name,$show) || $item->primary_key==1 ){
                    $fields[$k] = $item;
                    if( $item->type == 'enum' ){
                        $fields[$k]->data = $this->get_enum($this->table, $item->name);
                    }
                }
            }else{
                $fields[$k] = $item;
                if( $item->type == 'enum' ){
                    $fields[$k]->data = $this->get_enum($this->table, $item->name);
                }
            }
        }
        return $fields;
    }

    protected function v($view, $data = array())
    {
        return str_replace(array("\r", "\n"), '', view_back( "cms_$this->module/views/$view", $data, true) );
    }

    public function index()
    {
        $this->role();
        $data = $this->setting->get_all();
        $data['nastable'] = true;
        $fields = $this->get_fields();
        $data['fields'] = $fields;
        $data['subject'] = $this->subject;
        $data['module'] = $this->module;
        $data['pk'] = $this->get_primary_key()->name;
        $data['v_list'] = $this->v('v_list', $data);
        $data['v_add'] = $this->v('v_add', $data);
        $data['v_edit'] = $this->v('v_edit', $data);
        echo view_back( "cms_$this->module/views/v_app", $data);
    }

    public function add()
    {
        $this->role();
        $data = $this->setting->get_all();
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( $_POST ){
            foreach ($this->required() as $field) {
                if ( !$_POST[$field] )
                {
                    json_response( ['status' => 0, 'message' => "Bidang $field dibutuhkan" ] );
                }
            }
            $query = $this->db->insert( $this->table, $_POST );
            json_response( ['status' => $query, 'message' => 'Sukses' ] );

        }
        $fields = $this->get_fields();

        foreach ($fields as $k => $field) {
            if( $field->name == 'image' ){
                $fields[$k]->type = 'upload';
            }
        }
        $data['nastable'] = true;
        $data['fields'] = $fields;
        $data['subject'] = $this->subject;
        $data['module'] = $this->module;
        $data['pk'] = $this->get_primary_key()->name;
        echo view_back( "cms_$this->module/views/v_add", $data);
    }

    public function edit()
    {
        $this->role();
        $id = $this->uri->segment(4);
        $data = $this->setting->get_all();
        $_POST = json_decode(file_get_contents('php://input'), true);
        if( $_POST ){
            foreach ($this->required() as $field) {
                if ( !$_POST[$field] )
                {
                    json_response( ['status' => 0, 'message' => "Bidang $field dibutuhkan" ] );
                }
            }
            $id = $_POST[$this->get_primary_key()->name];
            $this->db->where($this->get_primary_key()->name, $id );
            $query = $this->db->update( $this->table, $_POST );
            json_response( ['status' => $query, 'message' => 'Sukses' ] );
        }

        $this->db->where( $this->get_primary_key()->name, $id );
        $results = $this->db->get( $this->table,1 )->row_array();
        $fields = $this->get_fields();

        foreach ($fields as $k => $field) {
            if( $field->name == 'image' ){
                $fields[$k]->type = 'upload';
            }
        }
        $data['nastable'] = true;
        $data['subject'] = $this->subject;
        $data['module'] = $this->module;
        $data['fields'] = $fields;
        $data['row'] = $results;
        $data['pk'] = $this->get_primary_key()->name;
        echo view_back( "cms_$this->module/views/v_edit", $data);
    }

    public function get()
    {
        $this->role();
        $limit = $this->input->get('limit');
        $page = $this->input->get('page');
        $search = $this->input->get('q');
        $column = $this->input->get('col');
        if( !$limit ) $limit=20;

        if( !$page ){
            $page = 0;
        }else{
            $page = ($page-1)*$limit;
        }
        if( isset($search) && $column ){
            $this->db->like($column, $search);
        }
        if( $this->where() ){
            foreach ($this->where() as $where){
                $this->db->where($where[0],$where[1]);
            }
        }
        $data = $this->db->get( $this->table, $limit, $page)->result_array();
        json_response( ['status' => 1, 'message' => $data] );
    }

    public function rows()
    {
        $this->role();
        $search = $this->input->get('q');
        $column = $this->input->get('col');
        if( isset($search) && $column ){
            $this->db->like($column, $search);
        }
        if( $this->where() ){
            foreach ($this->where() as $where){
                $this->db->where($where[0],$where[1]);
            }
        }
        $rows = $this->db->count_all_results($this->table);
        json_response( ['status' => 1, 'message' => $rows] );
    }

    public function delete()
    {
        $this->role();
        $id = $this->uri->segment(4);
        if( $id ){
            $ids = explode('-', $id);
            $this->db->where_in( $this->get_primary_key()->name, $ids);
            $q = $this->db->delete($this->table);
            json_response( ['status' => $q, 'message' => 'Sukses menghapus data'] );
        }else{
            json_response( ['status' => 0, 'message' => 'Gagal menghapus data, Silahkan ulangi lagi!'] );
        }
    }

    public function copy()
    {
        $this->role();
        $id = $this->uri->segment(4);
        if( $id ){
            $this->db->where( $this->get_primary_key()->name, $id);
            $row = $this->db->get($this->table,1)->row_array();
            unset($row[$this->get_primary_key()->name]);
            $q = $this->db->insert($this->table,$row);
            json_response( ['status' => $q, 'message' => 'Sukses menduplikat data'] );
        }else{
            json_response( ['status' => 0, 'message' => 'Gagal mlakukan duplikat data, Silahkan ulangi lagi!'] );
        }
    }

    public function columns()
    {
        $this->role();
        json_response( $this->get_fields() );
    }

    public function upload()
    {
        $this->role();
        if( !empty($_FILES) ){
            $config['upload_path']          = 'assets/uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1024;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file'))
            {
                json_response( ['status' => 0, 'message' => strip_tags($this->upload->display_errors()) ] );
            } else {
                $query = 1;
                json_response( ['status' => $query, 'message' => $this->upload->data() ] );
            }

        }else{
            $file_name = $this->uri->segment(4);
            if( $file_name ){
                $souce = FCPATH.'assets/uploads/'.$file_name;
                if( file_exists($souce) ) unlink($souce);
            }
            json_response( ['status' => 1, 'message' => 'Sukses'] );
        }
    }

    public function view()
    {
        $data = [];
        echo view_back( "cms_$this->module/views/tes", $data);
    }

}