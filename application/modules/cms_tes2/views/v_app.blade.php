@extends('app')

@section('body')
    <ng-view></ng-view>
@endsection
@section('head')
    <script src="{{ base_url('assets/grocery_crud/texteditor/ckeditor/ckeditor.js') }}"></script>
@endsection
@section('script')
    <script type="text/javascript">

        function getLength(obj) {
            return obj.length;
        }

        function showBox() {
            document.getElementById('page_content_inner').style.display = 'block';
        }

        const app = angular.module( '{{ $module }}', ['ngSanitize','ngRoute','ngStorage']);
        app.config(function($routeProvider) {
            $routeProvider
                .when("/", {
                    template : '{!! $v_list !!}',
                    controller : 'listController'
                })
                .when("/add", {
                    template : '{!! $v_add !!}',
                    controller : 'addController'
                })
                .when("/edit", {
                    template : '{!! $v_edit !!}',
                    controller : 'editController'
                });
        });

        app.controller('listController',function($scope, $http, $localStorage, $sessionStorage)
        {
            $scope.spinner = true;
            $scope.perPage = 20;
            $scope.totalRows = 0;
            $scope.pageStart = 1;
            $scope.pageEnd = 0;
            $scope.data = [];
            $scope.columns = [];
            $scope.maxPage = 0;
            $scope.is_search = false;
            $scope.selected = [];
            $scope.alertBox = false;

            $scope.data = $localStorage.data;

            /*-------start checkbox operation---------*/
            const updateSelected = function(action, id) {
                if (action === 'add' && $scope.selected.indexOf(id) === -1) {
                    $scope.selected.push(id);
                }
                if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
                    $scope.selected.splice($scope.selected.indexOf(id), 1);
                }
            };

            $scope.updateSelection = function($event, id) {
                let checkbox = $event.target;
                let action = (checkbox.checked ? 'add' : 'remove');
                updateSelected(action, id);
            };

            $scope.selectAll = function($event) {
                let checkbox = $event.target;
                let action = (checkbox.checked ? 'add' : 'remove');
                for ( let i = 0; i < $scope.data.length; i++) {
                    let entity = $scope.data[i];
                    {{ "updateSelected(action, entity.$pk;" }}
                }
            };

            $scope.getSelectedClass = function(entity) {
                return $scope.isSelected(entity.id) ? 'selected' : '';
            };
            /*--------checkbox operation----------*/

            function readData(limit,page) {
                $scope.spinner = false;
                let query = '?limit='+limit;
                if(page)query += '&page='+page;
                if($scope.is_search){
                    query += '&col='+$scope.selectedOption;
                    query += '&q='+$scope.q;
                }
                $http({
                    method: 'GET',
                    url: cms_url+"/get"+query
                }).then(function successCallback(response) {
                    $scope.data = response.data.message;
                    $localStorage.data = response.data.message;
                    $scope.maxPage = Math.ceil($scope.totalRows/$scope.perPage);
                    updateRowData();
                }, function errorCallback(response) {
                    UIkit.modal.alert('error '+JSON.parse(response));
                });
            }

            function updateRowData() {
                let query = '';
                if($scope.is_search){
                    query += '?col='+$scope.selectedOption;
                    query += '&q='+$scope.q;
                }
                $http({
                    method: 'GET',
                    url: cms_url+"/rows"+query
                }).then(function successCallback(response) {
                    $scope.spinner = true;
                    $scope.totalRows = response.data.message;
                    $scope.maxPage = Math.ceil($scope.totalRows/$scope.perPage);
                }, function errorCallback(response) {
                    UIkit.modal.alert('error '+JSON.parse(response));
                });
            }

            function showAlert(val) {
                UIkit.modal.alert( val );
            }

            $scope.optionPerPage = [
                {
                    'name':'20',
                    'value': 20
                },{
                    'name':'25',
                    'value': 25
                },{
                    'name':'50',
                    'value': 50
                },{
                    'name':'100',
                    'value': 100
                }
            ];
            $scope.selectedOptionPerPage = $scope.optionPerPage[0];

            $scope.searchClick = function () {
                $scope.selectedOption = document.getElementById("search-column").value;
                $scope.spinner = false;
                $scope.is_search = true;
                $scope.pageStart = 1;
                readData($scope.perPage, $scope.pageStart);
            };

            $scope.reset = function () {
                $scope.spinner = false;
                $scope.is_search = false;
                $scope.perPage = 20;
                readData($scope.perPage, $scope.pageStart);
            };

            $scope.changedPerPage = function() {
                $scope.perPage = $scope.selectedOptionPerPage.value;
                readData($scope.perPage, $scope.pageStart);
            };

            $scope.deleteClickBulkAction = function() {
                let scope_selection = $scope.selected;
                if( scope_selection.length > 0 ){
                    let id_to_delete = scope_selection.join('-');
                    UIkit.modal.confirm('Are you sure that you want to delete this record?', function(){
                        $scope.spinner = false;
                        $http({
                            method: 'GET',
                            url: cms_url+"/delete/"+id_to_delete
                        }).then(function successCallback(response) {
                            $scope.selected = [];
                            $scope.spinner = true;
                            let result = response.data;
                            if( result.status ){
                                $scope.alertMessage = result.message;
                                $scope.alertClass = 'uk-alert-success';
                                $scope.alertBox = true;
                                readData($scope.perPage);
                            }else{
                                $scope.alertMessage = result.message;
                                $scope.alertClass = 'uk-alert-danger';
                                $scope.alertBox = true;
                            }
                        }, function errorCallback(response) {
                            $scope.spinner = true;
                            showAlert('error'+JSON.parse(response));
                        });
                    });
                }else{
                    showAlert('No Data Selected!');
                }
            };

            $scope.deleteClick = function(id_to_delete) {
                UIkit.modal.confirm('Are you sure that you want to delete this record?', function(){
                    $scope.spinner = false;
                    $http({
                        method: 'GET',
                        url: cms_url+"/delete/"+id_to_delete
                    }).then(function successCallback(response) {
                        $scope.spinner = true;
                        let result = response.data;
                        if( result.status ){
                            $scope.alertMessage = result.message;
                            $scope.alertClass = 'uk-alert-success';
                            $scope.alertBox = true;
                            readData($scope.perPage);
                        }else{
                            $scope.alertMessage = result.message;
                            $scope.alertClass = 'uk-alert-danger';
                            $scope.alertBox = true;
                        }
                    }, function errorCallback(response) {
                        $scope.spinner = true;
                        UIkit.modal.alert('error '+JSON.parse(response));
                    });
                });
            };

            $scope.nextPageClick = function () {
                let maxPage = Math.ceil($scope.totalRows/$scope.perPage);
                if($scope.pageStart < maxPage){
                    $scope.spinner = false;
                    $scope.pageStart +=1;
                    readData($scope.perPage, $scope.pageStart);
                }
            };
            $scope.prevPageClick = function () {
                if( $scope.pageStart > 1 ){
                    $scope.spinner = false;
                    $scope.pageStart -=1;
                    readData($scope.perPage, $scope.pageStart);
                }
            };
            $scope.startPageClick = function () {
                $scope.spinner = false;
                $scope.pageStart = 1;
                readData($scope.perPage, $scope.pageStart);
            };
            $scope.endPageClick = function () {
                $scope.spinner = false;
                $scope.pageStart = Math.ceil($scope.totalRows/$scope.perPage);
                readData($scope.perPage, $scope.pageStart);
            };

            $scope.hideAlert = function () {
                $scope.alertBox = false;
            };

           console.log(getLength($scope.data));

            if( getLength($scope.data) < 1 ){
                readData($scope.perPage);
            }
            showBox();
        });

        app.controller('addController',function($scope, $http)
        {
            $scope.alertBox = false;
            $scope.spinner = true;
            $scope.data = [];
            $scope.columns = [];
            $scope.module = '{{$module}}';

            $scope.hideAlert = function () {
                $scope.alertBox = false;
            };

            $scope.deleteFileClick = function (event) {
                let item = event.target;
                let file_name = item.attributes['data-filename'].value;
                let field = item.attributes['data-column'].value;
                bootbox.confirm('Apakah anda yakin ingin menghapus file ini?', function(confirm){
                    if(confirm){
                        $scope.spinner = false;
                        $http({
                            method: 'GET',
                            url: cms_url+"/upload/"+file_name
                        }).then(function successCallback(response) {
                            $scope.spinner = true;
                            var result = response.data;
                            $("#success_"+field).hide();
                            $("#field_"+field).val(result.message.file_name);
                        }, function errorCallback(response) {
                            bootbox.alert('error '+JSON.parse(response));
                        });
                    }
                });
            };

            $scope.uploadImage = function (files,item) {
                let field = item.attributes['data-column'].value;
                if( files.length ){
                    let fd = new FormData();
                    fd.append("file", files[0]);
                    let uploadUrl = cms_url+"/upload";

                    $http.post(uploadUrl, fd, {
                        withCredentials: true,
                        headers: {'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function successCallback(response) {
                        $scope.spinner = true;
                        let result = response.data;
                        if( result.status ){
                            $("#success_"+field).show();
                            $("#file_"+field).html( result.message.file_name ).attr('href', base_url+'assets/uploads/'+result.message.file_name);
                            $("#delete_"+field).attr('data-filename', result.message.file_name);
                            $("#field_"+field).val(result.message.file_name);
                        }else{
                            $scope.alertMessage = result.message;
                            $scope.alertClass = 'uk-alert-danger';
                            $scope.alertBox = true;
                        }
                    }, function errorCallback(response) {
                        bootbox.alert('error : '+ JSON.stringify(response));
                    });
                }
            };

            $scope.mySave = function () {
                let formDataArray = $('#form').serializeArray();
                let formData = {};
                formDataArray.forEach(function(entry) {
                    formData[entry.name]=entry.value;
                });
                $scope.spinner = false;
                $http({
                    url: cms_url+"/add",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: formData
                }).then(function successCallback(response) {
                    $scope.spinner = true;
                    let result = response.data;
                    if( result.status ){
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-success';
                        $scope.alertBox = true;
                        document.getElementById("form").reset();
                    }else{
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-danger';
                        $scope.alertBox = true;
                    }
                }, function errorCallback(response) {
                    bootbox.alert('error : '+ JSON.stringify(response));
                });
            };

            $scope.mySaveBack = function () {
                let formDataArray = $('#form').serializeArray();
                let formData = {};
                formDataArray.forEach(function(entry) {
                    formData[entry.name]=entry.value;
                });
                $scope.spinner = false;
                $http({
                    url: cms_url+"/add",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: formData
                }).then(function successCallback(response) {
                    $scope.spinner = true;
                    let result = response.data;
                    if( result.status ){
                        window.location = cms_url;
                    }else{
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-danger';
                        $scope.alertBox = true;
                    }
                }, function errorCallback(response) {
                    bootbox.alert('error : '+ JSON.stringify(response));
                });
            };

            showBox();
        });

        app.controller('editController',function($scope, $http)
        {
            $scope.alertBox = false;
            $scope.spinner = true;
            $scope.data = [];
            $scope.columns = [];
            $scope.module = '{{$module}}';

            $scope.hideAlert = function () {
                $scope.alertBox = false;
            };

            $scope.deleteFileClick = function (event) {
                let item = event.target;
                let file_name = item.attributes['data-filename'].value;
                let field = item.attributes['data-column'].value;
                bootbox.confirm('Apakah anda yakin ingin menghapus file ini?', function(confirm){
                    if(confirm){
                        $scope.spinner = false;
                        $http({
                            method: 'GET',
                            url: cms_url+"/upload/"+file_name
                        }).then(function successCallback(response) {
                            $scope.spinner = true;
                            let result = response.data;
                            $("#success_"+field).hide();
                            $("#field_"+field).val(result.message.file_name);
                        }, function errorCallback(response) {
                            bootbox.alert('error '+JSON.parse(response));
                        });
                    }
                });
            };

            $scope.uploadImage = function (files,item) {
                let field = item.attributes['data-column'].value;
                if( files.length ){
                    let fd = new FormData();
                    fd.append("file", files[0]);
                    let uploadUrl = cms_url+"/upload";

                    $http.post(uploadUrl, fd, {
                        withCredentials: true,
                        headers: {'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function successCallback(response) {
                        $scope.spinner = true;
                        let result = response.data;
                        if( result.status ){
                            $("#success_"+field).show();
                            $("#file_"+field).html( result.message.file_name ).attr('href', base_url+'assets/uploads/'+result.message.file_name);
                            $("#delete_"+field).attr('data-filename', result.message.file_name);
                            $("#field_"+field).val(result.message.file_name);
                        }else{
                            $scope.alertMessage = result.message;
                            $scope.alertClass = 'uk-alert-danger';
                            $scope.alertBox = true;
                        }
                    }, function errorCallback(response) {
                        bootbox.alert('error : '+ JSON.stringify(response));
                    });
                }
            };

            $scope.mySave = function () {
                let formDataArray = $('#form').serializeArray();
                let formData = {};
                formDataArray.forEach(function(entry) {
                    formData[entry.name]=entry.value;
                });
                $scope.spinner = false;
                $http({
                    url: cms_url+"/edit",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: formData
                }).then(function successCallback(response) {
                    $scope.spinner = true;
                    let result = response.data;
                    if( result.status ){
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-success';
                        $scope.alertBox = true;
                    }else{
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-danger';
                        $scope.alertBox = true;
                    }
                }, function errorCallback(response) {
                    bootbox.alert('error : '+ JSON.stringify(response));
                });
            };

            $scope.mySaveBack = function () {
                let formDataArray = $('#form').serializeArray();
                let formData = {};
                formDataArray.forEach(function(entry) {
                    formData[entry.name]=entry.value;
                });
                $scope.spinner = false;
                $http({
                    url: cms_url+"/edit",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: formData
                }).then(function successCallback(response) {
                    $scope.spinner = true;
                    let result = response.data;
                    if( result.status ){
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-success';
                        $scope.alertBox = true;
                        window.location = cms_url;
                    }else{
                        $scope.alertMessage = result.message;
                        $scope.alertClass = 'alert-danger';
                        $scope.alertBox = true;
                    }
                }, function errorCallback(response) {
                    bootbox.alert('error : '+ JSON.stringify(response));
                });
            };

            showBox();
        });

    </script>
@endsection