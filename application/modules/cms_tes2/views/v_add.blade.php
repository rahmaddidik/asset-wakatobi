<div id="page_content" ng-controller="addController">
        <div class="loading" ng-hide="spinner"></div>
        <div id="page_heading">
            <h1>Add <a href="{{ base_url("cms/$module#!/") }}" class="md-btn mdn-btn-small pull-right return-to-list">Back to list</a>
            </h1> <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn">You can add new data in this form</span>
        </div>
        <div id="page_content_inner" style="display: none;">
            <form id="form" action="" method="post" class="form-div uk-form-stacked" enctype="multipart/form-data" accept-charset="utf-8" onsubmit="return false;">
                <div class="uk-grid uk-grid-medium ">
                    <div class="uk-width-xLarge-8-10 uk-width-large-7-10" >

                        <div ng-click="hideAlert()" ng-class="alertClass" ng-show="alertBox" class="uk-alert" data-uk-alert>
                            <div ng-bind-html="alertMessage"></div>
                        </div>

                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"> Form Detail </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-large-1">
                                        @foreach ($fields as $field)
                                        @if($field->primary_key!=1)
                                        @if($field->type=='varchar')
                                        <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                            <label id="{{ $field->name }}_display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                            <input class="md-input" id="field-{{ $field->name }}" name="{{ $field->name }}" type="text" value="" maxlength="{{ $field->max_length }}" />
                                        </div>
                                        @endif
                                        @if($field->type=='int' || $field->type=='bigint')
                                        <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                            <label id="{{ $field->name }}_display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                            <input class="md-input" id="field-{{ $field->name }}" name="{{ $field->name }}" type="number" value="" maxlength="{{ $field->max_length }}" />
                                        </div>
                                        @endif
                                        @if($field->type=='text')

                                        <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                            <label id="{{ $field->name }}_display_as_box" for="{{ $field->name }}">{{ format_title($field->name) }}</label>
                                            <textarea id="{{ $field->name }}" name="{{ $field->name }}" ></textarea>
                                        </div>

                                        @endif
                                        @if($field->type=='tinyint')
                                        <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                            <label id="{{ $field->name }}display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                            <p id="field-{{ $field->name }}">
                                                <input type="radio" name="{{ $field->name }}" value="1" id="field-{{ $field->name }}-1" data-md-icheck />
                                                <label for="field-{{ $field->name }}-1" class="inline-label">Yes</label>
                                                <input type="radio" name="{{ $field->name }}" value="0" id="field-{{ $field->name }}-2" data-md-icheck />
                                                <label for="field-{{ $field->name }}-2" class="inline-label">No</label>
                                            </p>
                                        </div>
                                        @endif
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"><br></h3>
                            </div>
                        </div>
                    </div>
                    @php
                    $form_extra = false;
                    @endphp
                    @foreach ($fields as $field)
                        @if( $field->type=='date' || $field->type=='datetime' || $field->type=='enum' || $field->type=='special' )
                            @php
                            $form_extra = true;
                            @endphp
                        @endif
                    @endforeach


                    <div class="uk-width-xLarge-2-10 uk-width-large-3-10 uk-sortable sortable-handler" data-uk-grid-margin data-uk-sortable>
                        @if( $form_extra )
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"> Form Detail </h3>
                            </div>
                            <div class="md-card-content">
                                @foreach ($fields as $field)
                                @if($field->primary_key!=1)
                                @if($field->type=='date')
                                <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                    <label id="{{ $field->name }}_display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                    <input class="datepic" id="field-{{ $field->name }}" name="{{ $field->name }}" type="text" value="" maxlength="{{ $field->max_length }}" />
                                </div>
                                @endif
                                @if($field->type=='datetime')
                                <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                    <label id="{{ $field->name }}_display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                    <input class="timepic" id="field-{{ $field->name }}" name="{{ $field->name }}" type="text" value="" maxlength="{{ $field->max_length }}" />
                                </div>
                                @endif
                                @if($field->type=='enum')
                                <div class="uk-form-row" id="{{ $field->name }}_field_box">
                                    <label id="{{ $field->name }}_display_as_box" for="field-{{ $field->name }}">{{ format_title($field->name) }}</label>
                                    <select id="field-{{ $field->name }}" name="{{ $field->name }}" data-md-selectize>
                                        @foreach ($field->data as $value)
                                            <option value="{{ $value }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div><br>
                                @endif
                                @endif
                                @endforeach

                            </div>
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"><br></h3>
                            </div>
                        </div>
                        @endif
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"> Actions </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-grid" data-uk-grid-margin="">
                                    <div id="options-content" class="uk-width-medium-1">
                                        <button type="submit" class="md-btn md-btn-primary mdn-btn-small submit-form" ng-click="mySave()">Save</button>
                                        <button type="submit" class="md-btn mdn-btn-small md-btn-success save-and-go-back-button" ng-click="mySaveBack()">Save & Back</button>
                                        <a href="{{ base_url("cms/$module#!/") }}" class="md-btn mdn-btn-small return-to-list">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>