<div id="page_content" ng-controller="listController">
        <div class="loading" ng-hide="spinner"></div>
        <div id="page_content_inner" style="display: none;">
            <h3 class="heading_b">Data {{ $subject }}</h3>
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text"> Actions </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-grid" data-uk-grid-margin="">
                                <div id="options-content" class="uk-width-medium-1-2">
                                    <a href="{{ base_url("cms/$module#!/add") }}" title="Add Post" class="md-btn"><i class="material-icons">add</i>Add {{ $subject }}</a>
                                    <button class="md-btn"><i class="material-icons">search</i> Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-modal" id="filtering-form-search" aria-hidden="true">
                <div class="uk-modal-dialog" style="top: -15.5px;">
                    <div class="uk-modal-header">
                        <div class="uk-clearfix">
                            <div class="uk-float-left">
                                <h3 class="uk-modal-title">Search {{ $subject }}</h3>
                            </div>
                            <div class="uk-float-right">
                                <a class="md-btn md-btn-flat md-btn-wave waves-effect waves-button uk-modal-close" href="#"><i class="material-icons">close</i></a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1">
                        <form autocomplete="off" accept-charset="utf-8">
                            <div class="uk-form-row">
                                <div class="md-input-wrapper">
                                    <label for="search_text">Search</label>
                                    <input ng-model="q" type="text" class="md-input" size="30" id="search_text">
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="md-input-wrapper">
                                    <select id="search-column" class="selectize-control single" data-md-selectize>
                                        @foreach ($fields as $f)
                                            @if($f->primary_key!=1)
                                                <option value="{{ $f->name }}">{{ format_title($f->name) }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <div class="uk-grid" data-uk-grid-margin="">
                                    <div class="uk-width-medium-1">
                                        <input class="md-btn md-btn-primary uk-modal-close" type="button" value="Search" ng-click="searchClick()">
                                        <input class="md-btn md-btn-success uk-modal-close" type="reset" onclick="" ng-click="reset()" value="Clear filtering">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <br>

            <div ng-click="hideAlert()" ng-class="alertClass" ng-show="alertBox" class="uk-alert" data-uk-alert>
                <div ng-bind-html="alertMessage"></div>
            </div>

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-toolbar"> <h3 class="md-card-toolbar-heading-text"> List data  {{ $subject }}</h3> </div>
                <div class="md-card-content">
                    <div class="uk-grid">
                        <div class="uk-width-large-1-1 uk-width-medium-1-1 uk-width-small-1-1">
                            <div class="uk-button-dropdown pull-right" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                                <button class="md-btn ">Bulk Action <i class="material-icons">expand_more</i></button>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Bulk Action</li>
                                        <li><a id="deleteall" href="#" ng-click="deleteClickBulkAction()">Delete Permanently</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check" style="margin-bottom: 68px;">
                            <thead>
                            <tr>
                                <th>Actions</th>
                                @foreach ($fields as $f)
                                    @if($f->primary_key!=1)
                                        <th>{{ format_title($f->name) }}</th>
                                    @endif
                                @endforeach
                                <th class="small_col"><input class="check_all" type="checkbox" ng-model="checkbox" ng-click="selectAll($event)" title="Select All"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="t in data">
                                @foreach ($fields as $field)
                                    @if($field->primary_key==1)
                                        <td>
                                            <div class="uk-button-dropdown" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                                                <button class="md-btn md-btn-small"><i class="material-icons">more_vert</i></button>
                                                <div class="uk-dropdown uk-dropdown-small uk-dropdown-bottom" style="">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li class="edit-row">
                                                            <a href="{{ base_url("cms/$module/edit") }}/{{ "{{t.$field->name" }}}}" title="Edit"><i class="material-icons">mode_edit</i> Edit</a>
                                                        </li>
                                                        <li ng-click="deleteClick(t.{{ $field->name }})">
                                                            <a href="" title="Delete"><i class="material-icons">delete_forever</i> Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                @endforeach

                                @foreach ($fields as $field)
                                    @if($field->primary_key!=1)
                                        @if($field->type=='tinyint')
                                            <td ng-if="t.{{ $field->name }}==1"><span class="uk-badge uk-badge-success">Yes</span></td>
                                            <td ng-if="t.{{ $field->name }}!=1"><span class="uk-badge uk-badge-danger">No</span></td>
                                        @else
                                            <td>{{ "{{t.$field->name" }}}}</td>
                                        @endif
                                    @endif
                                @endforeach

                                @foreach ($fields as $f)
                                    @if($f->primary_key==1)
                                        <td><input class="check_row" type="checkbox" name="checkbox_{{ "t.$f->name" }}}}" ng-checked="checkbox" ng-click="updateSelection($event, t.{{ $f->name }})" title="Select"></td>
                                    @endif
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="uk-grid" data-uk-grid-margin="" style="margin-top: 30px;">
                        <div class="uk-width-large-1-4 uk-width-medium-1-2">
                            <div class="uk-input-group">
                                <select class="md-input" ng-change="changedPerPage()" data-ng-options="o.name for o in optionPerPage" data-ng-model="selectedOptionPerPage"></select>
                            </div>
                        </div>
                        <div class="uk-width-large-1-4 uk-width-medium-1-2">
                            <div style="margin-top:15px;" class="uk-input-group"> Page <span id="page-starts-from">@{{pageStart}}</span> from <span id="total_items">@{{maxPage}}</span> Total Pages</div>
                        </div>
                        <div class="uk-width-large-1-4 uk-width-medium-1-2">
                            <div class="uk-input-group">
                                <div class="md-input-wrapper md-input-filled">
                                    <input class="md-input" name="tb_crud_page" type="text" value="@{{pageStart}}" id="tb_crud_page" disabled>
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-large-1-4 uk-width-medium-1-2">
                            <div class="uk-input-group">
                                <div class="md-input-wrapper md-input-wrapper-disabled md-input-filled">
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="pager" style="text-align: center;">
                        <li class="md-btn md-btn-wave" ng-click="startPageClick()" style="float: left;"><a href="" >« First</a></li>
                        <li class="md-btn md-btn-wave" ng-click="prevPageClick()"><a href="" >« Prev</a></li>
                        <li class="md-btn md-btn-wave" ng-click="nextPageClick()"><a href="" >Next »</a></li>
                        <li class="md-btn md-btn-wave" ng-click="endPageClick()" style="float: right;"><a href="" >Last »</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>