<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_seo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('widgets');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('SEO Setting');
            $umbrella->columns('setting_name', 'setting_value');
            $umbrella->where('setting_type', 'setting_seo');
            $umbrella->order_by('setting_id', 'asc');
            $umbrella->field_type('setting_type', 'hidden', 'setting_seo');
            $umbrella->unset_texteditor('setting_value');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->edit_fields('setting_value');
            $umbrella->display_as('setting_value', lang('value'));
            $umbrella->display_as('setting_name', lang('setting_name'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('setting_desc', array($this, '_setting_status'));
            $umbrella->set_bulkactionfalse(true);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }
    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */

}