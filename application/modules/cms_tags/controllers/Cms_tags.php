<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_tags extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            //Get Panel setting
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('tags');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_category');
            $umbrella->set_subject('Tag');
            $umbrella->where('tb_category.category_type', 'tags');
            $umbrella->order_by('category_parent', 'desc');
            $umbrella->order_by('category_lineage', 'asc');
            $umbrella->columns('category_name', 'category_desc', 'category_slug', 'category_type');
            $umbrella->add_fields('category_name', 'category_desc', 'category_slug', 'category_type');
            $umbrella->edit_fields('category_name', 'category_desc', 'category_slug', 'category_type');
            $umbrella->field_type('category_slug', 'hidden');
            $umbrella->field_type('category_type', 'hidden');
            $umbrella->display_as('category_type', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->display_as('category_name', lang('cat_name'));
            $umbrella->display_as('category_desc', lang('cat_desc'));
            $umbrella->display_as('category_slug', lang('cat_slug'));
            $umbrella->callback_column('category_type', array($this, '_callback_category_type'))->callback_column('category_name', array($this, '_callback_category_name'));
            $umbrella->callback_edit_field('category_parent', array($this, '_set_field_category_parent_callback'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->unset_texteditor('category_desc', 'full_text');
            $umbrella->set_composer(false);
            $umbrella->set_listfilter(false);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */


    public function _callback_category_type($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<input type="checkbox" class="removelist" value="' . $row->category_id . '" name="' . $row->category_id . '" id="' . $row->category_id . '" data-md-icheck />';
    }

    public function _callback_category_name($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $type_con = $this->uri->segment(2);
        $new_name = "<a title='$value' href='" . site_url('cms/' . $type_con . '/index/edit/' . $row->category_id) . "'>" . $value . "</b></a>";
        return $new_name;
    }


    public function _set_callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();

        if (empty($post_array['category_type']))
            $post_array['category_type'] = "tags";

        $config = array(
            'field' => 'category_slug',
            'title' => 'category_name',
            'table' => 'tb_category',
            'id' => 'category_id',
        );
        $this->load->library('slug', $config);
        if (empty($post_array['category_slug']))
            $post_array['category_slug'] = $this->slug->create_uri($post_array['category_name']);
        return $post_array;
    }

    public function _set_callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['category_type']))
            $post_array['category_type'] = "tags";
        return $post_array;
    }

}