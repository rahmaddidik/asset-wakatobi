    <!-- additional styles for plugins -->
    <!-- weather icons -->
    <link rel="stylesheet" href="{asset}bower_components/weather-icons/css/weather-icons.min.css" media="all">

   <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">{_search_post}</h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <ul class="md-list">
                                <?php foreach($search_post as $post) {  ?>
                                <li>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><a href="cms/<?php echo $post->post_type;?>/index/edit/<?php echo $post->post_id;?>"><?php echo $post->post_title;?></a></span>
                                        <span class="uk-text-small uk-text-muted"><?php echo strip_tags(substr($post->post_content,0,120));?>.</span>
                                    </div>
                                </li>
                                <?php } ?>
                                <?php if(empty($search_post )) { echo '<li>'.lang('search_no').' <b><i>'.$keyword.'</i></b> '.lang('search_sugest').'</li>';} ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <h3 class="heading_b uk-margin-bottom">{_search_user}</h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <ul class="md-list">
                                <?php foreach($search_user as $user) {  ?>
                                <li>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php echo $user->user_display_name;?> - {_search_last} :  <?php echo timespan($user->last_login);?></span>
                                        <span class="uk-text-small uk-text-muted"><?php echo $user->username;?> - <?php echo $user->email;?></span>
                                    </div>
                                </li>
                                <?php } ?>
                                <?php if(empty($search_user )) { echo '<li>'.lang('search_no').' <b><i>'.$keyword.'</i></b> '.lang('search_sugest').'</li>';} ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  