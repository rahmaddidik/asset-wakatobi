<?php 
class Cms_search extends CI_Controller {

    public function index()
    {
        //Cek Auth
        $this->M_base_config->cekaAuth();
        //Get Panel Setting
        $data=$this->base_config->panel_setting();
        //Path Asset
        $data['asset'] = $this->base_config->asset_back();
        //Set navigation left
        $data['nav'] = 'yes';
        // Declare Views name
        $terms = $_GET['q'];
        $terms_clean = $this->security->xss_clean($terms);

        $data_post= array('table'=>'tb_post','nm_sort' => 'post_date','sort'=>'desc','limit'=>'0','offset'=>'10','match'=>array(array('match_value'=>$terms_clean,'matchfield'=>'post_title'),array('match_value'=>$terms_clean,'matchfield'=>'post_content')));
        $data['search_post'] = $this->M_base_config->search($data_post);

        $data_user= array('table'=>'tb_user','nm_sort' => 'user_display_name','sort'=>'asc','limit'=>'0','offset'=>'10','match'=>array(array('match_value'=>$terms_clean,'matchfield'=>'user_display_name')));
        $data['search_user'] = $this->M_base_config->search($data_user);
        $data['keyword']    =  $terms_clean;
        
        $data['viewspage'] = 'v_search';
        //Render Page 
        $this->base_config->_render_page($data);
    }
    
}