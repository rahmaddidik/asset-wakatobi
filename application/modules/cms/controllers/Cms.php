<?php 
class Cms extends CI_Controller {

    public function index()
    {
        //Cek Auth
        $this->M_base_config->cekaAuth();
        //Get Panel Setting
        $data=$this->base_config->panel_setting();
        //Path Asset
        $data['asset'] = $this->base_config->asset_back();
        $user  = $this->ion_auth->user()->row();
        //Set navigation left
        $data['nav'] = 'yes';
        if (!$this->ion_auth->is_admin()){
            $datapostval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'posts'),array('wherefield'=>'post_author','where_value'=>$user->username)));
        }else {
            $datapostval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'posts')));
        }
        $data['totalpost'] = $this->M_base_config->countDatamultiple($datapostval);

        if (!$this->ion_auth->is_admin()){
            $datapagetval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'pages'),array('wherefield'=>'post_author','where_value'=>$user->username)));
        }else {
            $datapagetval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'pages')));
        }
        $data['totalpage'] = $this->M_base_config->countDatamultiple($datapagetval);

        if (!$this->ion_auth->is_admin()){
            $datamediatval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'media'),array('wherefield'=>'post_author','where_value'=>$user->username)));
        }else {
            $datamediatval = array('table'=>'tb_post','where'=>array(array('wherefield'=>'post_type','where_value'=>'media')));
        }
        $data['totalmedia'] = $this->M_base_config->countDatamultiple($datamediatval);
        $data['totaluser'] = $this->db->count_all_results('tb_user');
        // Declare Views name
        $data['dashboard']  = true;
        $data['viewspage'] = 'cpanel';
        //Render Page 
        $this->base_config->_render_page($data);
    }
    
}