    <!-- additional styles for plugins -->
    <!-- weather icons -->
    <link rel="stylesheet" href="{asset}bower_components/weather-icons/css/weather-icons.min.css" media="all">

    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">{_nav_menu_dashboard}</h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid hierarchical_show" data-uk-grid-margin >
                        <div class="uk-width-1-1">
                           <h2>{_dash_welcome}</h2>
                           <p>{_dash_tagline}</p>  
                        </div>
                        <div class="uk-width-1-3">
                           <b>Get Started</b><br><br><br><br>
                           <a class="md-btn md-btn-primary" href="cms/posts" title="">Add Post</a>  or <a class="md-btn md-btn-primary" href="cms/profile" title="">Go to profile</a><br><br>
                        </div>
                        <div class="uk-width-1-3">
                            <b>Next Step</b><br><br>
                            <ul class="md-list">
                                <li><a href="cms/posts/index/add"> <i class="material-icons md-36">&#xE254;</i> Write your first blog post</a></li>
                                <li><a href="<?php echo base_url();?>"><i class="material-icons md-36">&#xE8F4;</i> View your site</a></li>
                                <?php $pages = $this->base_config->groups_access_sigle('menu','pages'); if($pages == false) { ?>
                                <li><a href="cms/pages/index/add"><i class="material-icons md-36">&#xE8D8;</i> Add an About page</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="uk-width-1-3">
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                            <span class="uk-text-muted uk-text-small">{_total_post}</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">{totalpost}</span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                            <span class="uk-text-muted uk-text-small">{_total_media}</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">{totalmedia}</span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                            <span class="uk-text-muted uk-text-small">{_total_page}</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">{totalpage}</span></h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span></div>
                            <span class="uk-text-muted uk-text-small">{_total_user}</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">{totaluser}</span></h2>
                        </div>
                    </div>
                </div>
            </div>
            <!-- info cards -->
            <div class="uk-grid uk-grid-medium uk-grid-width-medium-1-2 uk-grid-width-large-1-3" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
                <div>
                    <div class="md-card">
                        <div style="background:url(img/load/521/161/cover/<?php echo  $this->ion_auth->user()->row()->user_cover;?>); background-size:cover;" class="md-card-head md-bg-light-blue-600">
                            <div class="md-card-head-menu" data-uk-dropdown="{pos:'bottom-right'}">
                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="cms/profile">User profile</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="uk-text-center">
                                <img class="md-card-head-avatar" src="img/load/100/100/crop/<?php echo  $this->ion_auth->user()->row()->user_avatar ;?>" alt=""/>
                            </div>
                            <h3 class="md-card-head-text uk-text-center md-color-white">
                                <?php echo  $this->ion_auth->user()->row()->user_display_name ;?>
                                <span><?php echo  $this->ion_auth->user()->row()->email ;?></span>
                            </h3>
                        </div>
                        <div class="md-card-content">

                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php echo  $this->ion_auth->user()->row()->email ;?></span>
                                        <span class="uk-text-small uk-text-muted">{_p_email}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php echo  $this->ion_auth->user()->row()->user_mobile;?></span>
                                        <span class="uk-text-small uk-text-muted">{_p_phone}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-icon-facebook-official"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php echo  $this->ion_auth->user()->row()->user_facebook;?></span>
                                        <span class="uk-text-small uk-text-muted">{_p_fb}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon uk-icon-twitter"></i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php echo  $this->ion_auth->user()->row()->user_twitter;?></span>
                                        <span class="uk-text-small uk-text-muted">{_p_twitter}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  