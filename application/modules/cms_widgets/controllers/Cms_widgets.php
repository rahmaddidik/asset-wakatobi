<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_widgets extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('widgets');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('Widgets');
            $umbrella->columns('setting_name', 'setting_value', 'setting_desc');
            $umbrella->where('setting_type', 'front-widgets');
            $umbrella->order_by('setting_name', 'asc');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->add_fields('setting_type', 'setting_name', 'setting_value', 'setting_desc');
            $umbrella->edit_fields('setting_name', 'setting_value', 'setting_desc');
            $umbrella->field_type('setting_desc', 'enum', array('active', 'deactive'));
            $umbrella->field_type('setting_type', 'hidden');
            $umbrella->display_as('setting_value', lang('value'));
            $umbrella->display_as('setting_name', lang('widgets_name'));
            $umbrella->display_as('setting_desc', lang('com_status'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('setting_desc', array($this, '_setting_status'));
            $umbrella->set_bulkactionfalse(true);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _set_callback_before_insert($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['setting_desc']))
            $post_array['setting_desc'] = "deactive";
        $post_array['setting_type'] = 'front-widgets';
        return $post_array;
    }

    public function _set_callback_before_update($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['setting_desc']))
            $post_array['setting_desc'] = "deactive";
        $post_array['setting_type'] = 'front-widgets';
        return $post_array;
    }

    public function _setting_status($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($value == 'active') {
            $status = '<span class="uk-badge uk-badge-success">' . $value . '</span>';
        } else {
            $status = '<span class="uk-badge ">' . $value . '</span>';
        }
        return $status;
    }
}