<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_customize extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();

            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('customize');
            $state = $umbrella->getState();
            $state_info = $umbrella->getStateInfo();
            $umbrella->set_field_upload('setting_value', 'assets/uploads/');
            if ($state == 'edit') {
                $params = array('table' => 'tb_setting', 'where' => array(array('wherefield' => 'setting_id', 'where_value' => $state_info->primary_key)), 'return' => 'setting_desc');
                $get_setting_desc = $this->M_base_config->getSimpleData($params);

                if ($get_setting_desc == "page") {
                    $umbrella->set_relation('setting_value', 'tb_post', 'post_title', array('post_type' => 'pages'));
                } else if ($get_setting_desc == "color") {
                    $umbrella->field_type('setting_value', 'string');
                    $umbrella->callback_edit_field('setting_value', array($this, '_edit_color'));
                } else if ($get_setting_desc == "menu") {
                    $umbrella->set_relation('setting_value', 'tb_setting', 'setting_value', array('setting_type' => 'front-menus'));
                } else if ($get_setting_desc == "") {
                    $umbrella->field_type('setting_value', 'string');
                } else {
                }
            }
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('Customizing');
            $umbrella->columns('setting_name', 'setting_value');
            $umbrella->where('setting_type', 'front-customize');
            $umbrella->order_by('setting_desc', 'asc');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->unset_add();
            $umbrella->unset_delete();
            $umbrella->fields('setting_value');
            $umbrella->display_as('setting_type', lang('theme_name'));
            $umbrella->display_as('setting_value', lang('theme_status'));
            $umbrella->display_as('setting_desc', lang('theme_pic'));
            $umbrella->callback_column('setting_value', array($this, '_callback_setting_value'));
            $umbrella->set_bulkactionfalse(true);
            $umbrella->set_actionfalse(true);
            $output = $umbrella->render();

            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _edit_color($value, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        return '<input type="text" class="md-input colorpic" name="setting_value" id="picker" value="' . $value . '" />';
    }

    public function _callback_setting_value($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->setting_desc == 'image') {
            $return = '<div class="text-left"><a href="' . base_url() . 'img/load/100/100/full/' . $value . '" class="image-thumbnail"><img height="70px" src="' . base_url() . 'img/load/100/100/full/' . $value . '" class="img_small" alt=""></a></div>';
        } else if ($row->setting_desc == 'color') {
            $return = '<div style="background-color:' . $value . ';" class="colorvalue">' . $value . '</div>';
        } else if ($row->setting_desc == 'menu') {
            $this->db->where('setting_id', $value);
            $return = $this->db->get('tb_setting', 1)->row('setting_value');
        } else {
            $return = $value;
        }
        return $return;
    }
}