<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_menus extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
        //Hirarchy library
        $config = array('table' => 'tb_category', 'primary_key' => 'category_id', 'parent_id' => 'category_parent', 'lineage' => 'category_lineage', 'deep' => 'category_deep');
        $this->load->library('mahana_hierarchy');
        $this->mahana_hierarchy->initialize($config);
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('menu');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('Menus');
            $umbrella->columns('setting_name', 'setting_value', 'setting_desc');
            $umbrella->where('setting_type', 'front-menus-lists');
            $umbrella->order_by('setting_name', 'asc');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->unset_delete();
            $umbrella->unset_list();
            $umbrella->add_fields('setting_type', 'setting_name', 'setting_value', 'setting_desc');
            $umbrella->edit_fields('setting_name', 'setting_value', 'setting_desc');
            $umbrella->field_type('setting_value', 'string');
            $umbrella->field_type('setting_type', 'hidden');
            $umbrella->field_type('setting_name', 'hidden');
            $umbrella->display_as('setting_value', lang('menu_name'));
            $umbrella->display_as('setting_desc', lang('menu_position'));
            $umbrella->set_relation('setting_desc', 'tb_setting', 'setting_value', array('setting_type' => 'front-menus', 'setting_name' => 'menus_positions'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('setting_desc', array($this, '_setting_status'));
            $umbrella->set_bulkactionfalse(true);
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                //Cek Auth
                $this->M_base_config->cekaAuth();
                //Get Panel Setting
                $this->mahana_hierarchy->resync();
                $data = $this->base_config->panel_setting();
                //Path Asset
                $data['asset'] = $this->base_config->asset_back();
                //Set navigation left
                $data['nav'] = 'yes';
                // Declare Views name
                //Load setting menu position
                $this->db->where('setting_type', 'front-menus');
                $this->db->where('setting_name', 'menus_positions');
                $this->db->order_by('setting_id', 'asc');
                $data['menus_positions'] = $this->db->get('tb_setting')->result();
                //Load categoy list
                $data['listcategory'] = $this->mahana_hierarchy->where(array('category_type' => 'category'))->get();
                //Load pages
                $this->db->select('post_id, post_title');
                $this->db->where('post_type', 'pages');
                $this->db->where('post_status', 'publish');
                $data['pages'] = $this->db->get('tb_post')->result();
                $menus_type = $this->input->get('type', true);
                if (empty($menus_type)) {
                    $menus_type = '21';
                    $data['activemenu'] = '21';
                } else {
                    $menus_type = $menus_type;
                    $data['activemenu'] = $menus_type;
                }
                $data['accses'] = $this->base_config->groups_access_noncrud('menu');
                $data['menulist'] = $this->getmenulist($menus_type);

                $data['viewspage'] = 'v_menus';
                $data['nastable'] = true;
                $this->base_config->_render_page($data);
            } else {
                if ($e->getCode() == 14) {
                    redirect('cms/404', 'refresh');
                } else {
                    show_error($e->getMessage());
                }
            }
        }

    }

    public function _set_callback_before_insert($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $post_array['setting_type'] = 'front-menus-lists';
        $post_array['setting_type'] = 'front-menus-lists';
        return $post_array;
    }

    public function _set_callback_before_update($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $post_array['setting_type'] = 'front-menus-lists';
        $post_array['setting_type'] = 'front-menus-lists';
        return $post_array;
    }

    public function savemenu()
    {
        $this->M_base_config->cekaAuth();
        $jenis_menu = $this->input->post('jenis_menu');
        $data = $this->input->post('jsonmenudata');
        if (!empty($jenis_menu) && !empty($data))
            $this->db->where('category_slug', $jenis_menu);
        if ($this->db->delete('tb_category')) {
            $menus = json_decode($data);
            $inputmenutmp = array();
            $inputmenu = array();
            for ($x = 0; $x < count($menus); $x++) {
                $category_name = $menus[$x]->id;
                $category_type = $menus[$x]->parenttype;
                $category_desc = $menus[$x]->id;
                $category_parent = 0;
                $category_slug = $jenis_menu;
                $data = array(
                    'category_name' => $category_name,
                    'category_slug' => $category_slug,
                    'category_type' => $category_type,
                    'category_parent' => $category_parent,
                    'category_desc' => $category_desc
                );
                if ($this->db->insert('tb_category', $data)) {
                    if (isset($menus[$x]->children)) {
                        $id_parent = $this->db->insert_id();
                        $this->getparent($menus[$x]->children, $id_parent, $jenis_menu);
                    }
                }
            }

            $this->session->set_flashdata('msg', 'succsess');
            $this->db->where('category_type', 'link');
            $ceklink = $this->db->get('tb_category')->result();
            foreach ($ceklink as $link) {
                $this->db->where('category_name', $link->category_id);
                $eksis = $this->db->get('tb_category')->result();
                if (empty($eksis)) {
                    $this->db->where('category_id', $link->category_id);
                    $this->db->delete('tb_category');
                }

            }
            $this->mahana_hierarchy->resync();
            redirect(base_url() . 'cms/menus?type=' . $jenis_menu, 'refresh');
        }
        redirect(base_url() . 'cms/menus', 'refresh');
    }

    public function getparent($menus, $id_parent, $jenis_menu)
    {
        $this->M_base_config->cekaAuth();
        for ($x = 0; $x < count($menus); $x++) {
            $category_name = $menus[$x]->id;
            $category_type = $menus[$x]->parenttype;
            $category_desc = $menus[$x]->id;
            $category_parent = $id_parent;
            $category_slug = $jenis_menu;
            $data = array(
                'category_name' => $category_name,
                'category_slug' => $category_slug,
                'category_type' => $category_type,
                'category_parent' => $category_parent,
                'category_desc' => $category_desc
            );
            if ($this->db->insert('tb_category', $data)) {
                if (isset($menus[$x]->children)) {
                    $id_parent2 = $this->db->insert_id();
                    $this->getparent($menus[$x]->children, $id_parent2, $jenis_menu);
                }
            }
        }
        return true;
    }

    public function add_link()
    {
        $this->M_base_config->cekaAuth();
        if ($_POST) {
            $category_name = $this->input->post('category_name');
            $category_type = "link";
            $category_desc = $this->input->post('category_desc');

            $config = array(
                'field' => 'category_slug',
                'title' => 'category_name',
                'table' => 'tb_category',
                'id' => 'category_id',
            );
            $this->load->library('slug', $config);
            $category_slug = $this->slug->create_uri($category_name);
            $data = array(
                'category_name' => $category_name,
                'category_slug' => $category_slug,
                'category_type' => $category_type,
                'category_desc' => $category_desc
            );
            if ($this->db->insert('tb_category', $data)) {
                $data = array(
                    'category_id' => $this->db->insert_id(),
                    'category_name' => $category_name,
                    'category_slug' => $category_slug,
                    'category_type' => $category_type,
                    'category_desc' => $category_desc
                );
                echo json_encode($data);
            }
        }
        exit;
    }

    public function getmenulist($menus_type = null)
    {
        $this->M_base_config->cekaAuth();
        $data = $this->mahana_hierarchy->where(array('category_slug' => $menus_type, 'category_deep' => 0))->get();
        $listmenu = "";
        $getchild = "";
        for ($x = 0; $x < count($data); $x++) {
            if ($data[$x]['category_type'] == 1) {
                $this->db->where('category_id', $data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus = 'category_id';
                $name_plus = 'category_name';
                $parenttype = 1;
            } else if ($data[$x]['category_type'] == 2) {
                $this->db->where('category_id', $data[$x]['category_name']);
                $getcat = $this->db->get('tb_category')->result();
                $idplus = 'category_id';
                $name_plus = 'category_name';
                $parenttype = 2;
            } else {
                $this->db->where('post_id', $data[$x]['category_name']);
                $getcat = $this->db->get('tb_post')->result();
                $idplus = 'post_id';
                $name_plus = 'post_title';
                $parenttype = 3;
            }
            foreach ($getcat as $cat) {
                $children = $this->gethierarchychild($data[$x]['category_id']);
                if (!empty($children)) {
                    $classparent = "uk-parent";
                    $getchild = $children;
                } else {
                    $classparent = "";
                    $getchild = "";
                }
                $listmenu .= '<li  class="uk-nestable-item ' . $classparent . '" data-id="' . $cat->$idplus . '" data-parenttype="' . $parenttype . '"><div class="uk-nestable-panel"><div class="uk-nestable-toggle" data-nestable-action="toggle"></div><span class="uk-text-muted uk-text-small">' . $cat->$name_plus . '<a href="javascript:void(0)" title="Remove this" data-uk-tooltip class="removethis material-icons float-right">&#xE14A;</a></span></div>';
            }
            $listmenu .= $getchild;
            $listmenu .= '</li>';

        }
        return $listmenu;
    }

    public function gethierarchychild($parent_id)
    {
        $this->M_base_config->cekaAuth();
        $data = $this->mahana_hierarchy->get_children($parent_id);
        $getchild = "";
        if (!empty($data)) {
            $listmenu = '<ul class="uk-nestable-list">';
            for ($x = 0; $x < count($data); $x++) {
                if ($data[$x]['category_type'] == 1) {
                    $this->db->where('category_id', $data[$x]['category_name']);
                    $getcat = $this->db->get('tb_category')->result();
                    $idplus = 'category_id';
                    $name_plus = 'category_name';
                    $parenttype = 1;
                } else if ($data[$x]['category_type'] == 2) {
                    $this->db->where('category_id', $data[$x]['category_name']);
                    $getcat = $this->db->get('tb_category')->result();
                    $idplus = 'category_id';
                    $name_plus = 'category_name';
                    $parenttype = 2;
                } else {
                    $this->db->where('post_id', $data[$x]['category_name']);
                    $getcat = $this->db->get('tb_post')->result();
                    $idplus = 'post_id';
                    $name_plus = 'post_title';
                    $parenttype = 3;
                }
                foreach ($getcat as $cat) {
                    $children = $this->gethierarchychild($data[$x]['category_id']);
                    if (!empty($children)) {
                        $classparent = "uk-parent";
                        $getchild = $children;
                    } else {
                        $classparent = "";
                        $getchild = "";
                    }
                    $listmenu .= '<li  class="uk-nestable-item ' . $classparent . '" data-id="' . $cat->$idplus . '" data-parenttype="' . $parenttype . '"><div class="uk-nestable-panel"><div class="uk-nestable-toggle" data-nestable-action="toggle"></div><span class="uk-text-muted uk-text-small">' . $cat->$name_plus . '<a href="javascript:void(0)" title="Remove this" data-uk-tooltip class="removethis material-icons float-right">&#xE14A;</a></span></div>';
                }
                $listmenu .= $getchild;
                $listmenu .= '</li>';
            }
            $listmenu .= ' </ul>';
            return $listmenu;
        }
    }
}