  <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-medium-bottom"> <?php echo lang('menu_title'); ?></h3>
            <?php 
            if($accses['add_access'] == false){ ?>
             <form action="cms/menus/savemenu" method="post">
            <?php } ?>
                <div class="uk-grid" >
                    <div class="uk-width-medium-1">
                        <div class="md-card" data-uk-grid-margin>
                            <div class="md-card-content">
                                    <div class="uk-grid" >
                                        <div class="uk-width-medium-1-10">
                                            <?php echo lang('menu_chose'); ?>
                                        </div>
                                        <div class="uk-width-medium-2-10" autocomplate="off">
                                            <select name="jenis_menu" id="jenis_menu" data-md-selectize>
                                                <?php 
                                                    foreach($menus_positions as $position)
                                                        if($position->setting_id==$activemenu)
                                                        echo '<option selected value="'.$position->setting_id.'">'.$position->setting_value.'</option>';
                                                            else
                                                                echo '<option value="'.$position->setting_id.'">'.$position->setting_value.'</option>';

                                                ?>
                                            </select>
                                        </div> 
                                        <div class="uk-width-medium-3-10">
                                            <?php  if($accses['edit_access'] == false){ ?>
                                            <button type="button" id="changetypemenu" class="md-btn"> <?php echo lang('menu_choses'); ?></button> <?php echo lang('or') ;?> <a href="cms/menus/index/edit/{activemenu}"> <?php echo lang('menu_edit'); ?></a>
                                             <?php } ?>
                                        </div>
                                        <div class="uk-width-medium-4-10">
                                            <?php  if($accses['add_access'] == false){ ?>
                                              <!-- <a class="md-btn md-btn-primary float-right" href="cms/menus/index/add"> <?php echo lang('menu_add'); ?></a> -->
                                            <?php } ?>
                                        </div>
                                    </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid" >
                    <div class="uk-width-medium-2-10">
                        <div class="md-card" data-uk-grid-margin>
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"> <?php echo lang('menu_value'); ?></h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-accordion" data-uk-accordion>
                                    <h3 class="uk-accordion-title"><?php echo lang('menu_addcat'); ?></h3>
                                    <div class="uk-accordion-content">
                                        <?php 
                                            $data2='';
                                            for($x=0;$x<count($listcategory);$x++){
                                            $count  = $listcategory[$x]['category_deep'];
                                            $splash = "";
                                            for($y=0; $y<$count; $y++) {
                                              $splash.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                            }
                                            $data2.=$splash.'<input type="checkbox" class="catlist" title="'.strip_tags($listcategory[$x]['category_name']).'" data-md-icheck  id="'.$listcategory[$x]['category_id'].'" value="'.$listcategory[$x]['category_id'].'" /> <label for="'.$listcategory[$x]['category_id'].'" class="inline-label">'.$listcategory[$x]['category_name'].'</label></br>';
                                            }
                                          $data2.='';
                                          echo  $data2;
                                        ?><br>
                                         <div class="uk-grid" >
                                            <div class="uk-width-medium-5-10">
                                                <input type="checkbox" class="selectall" data-md-icheck  id="catlist" value="0" />
                                                <label for="catallbtn" class="inline-label"><?php echo lang('menu_all'); ?></label>
                                            </div>
                                            <div class="uk-width-medium-5-10">
                                                <button id="catlist" title="1" class="md-btn sendtolistmenu"><?php echo lang('menu_addto'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="uk-accordion-title"><?php echo lang('menu_addlink'); ?></h3>
                                    <div class="uk-accordion-content">
                                       <div class="uk-form-row">
                                            <div class="md-input-wrapper">
                                                <label><?php echo lang('menu_linkurl'); ?></label>
                                                <input name="urllink" id="urllink" type="text" class="md-input">
                                                <span  class="md-input-bar"></span>
                                            </div>
                                        </div>
                                        <div class="uk-form-row">
                                            <div class="md-input-wrapper">
                                                <label><?php echo lang('menu_linktext'); ?></label>
                                                <input name="textlink" id="textlink" type="text" class="md-input">
                                                <span class="md-input-bar"></span>
                                            </div>
                                        </div>
                                        <div class="uk-form-row">
                                            <div class="md-input-wrapper">
                                                <button type="button" onclick="savemultiple()" class="md-btn"><?php echo lang('menu_addto'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="uk-accordion-title"><?php echo lang('menu_addpage'); ?></h3>
                                    <div class="uk-accordion-content">
                                        <?php
                                        foreach($pages as $page)
                                            echo '<input type="checkbox" class="pagelist" title="'.strip_tags($page->post_title).'" data-md-icheck  id="'.$page->post_id.'" value="'.$page->post_id.'" /> <label for="'.$page->post_id.'" class="inline-label">'.$page->post_title.'</label></br>';
                                        ?>
                                        <br>
                                         <div class="uk-grid" >
                                            <div class="uk-width-medium-5-10">
                                               <input type="checkbox" class="selectall" data-md-icheck  id="pagelist" value="0" />
                                               <label for="catallbtn" class="inline-label"><?php echo lang('menu_all'); ?></label>
                                            </div>
                                            <div class="uk-width-medium-5-10">
                                                <button  id="pagelist" title="3" class="md-btn sendtolistmenu"class="md-btn"><?php echo lang('menu_addto'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-medium-8-10">
                        <div class="md-card" data-uk-grid-margin>
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"> <?php echo lang('menu_list'); ?></h3>
                            </div>
                            <div class="md-card-content">
                                <ul class="uk-nestable" id="nestable">
                                    {menulist}
                                </ul>
                                <?php if(empty($menulist)){ echo '<i class="material-icons">&#xE88F;</i> '.lang('menu_empty'); }?>
                            </div>
                            <input id="tmp" value="" name="jsonmenudata" type="hidden" class="md-input" />
                            <div class="md-card-toolbar">
                                <?php if($accses['add_access'] == false){ ?>
                                 <button type="submit" class="md-btn md-btn-primary float-right"><?php echo lang('menu_save'); ?></button>
                                 <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



  