<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_jadwal_kegiatan extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
//
        $config = array('table' => 'tb_category', 'primary_key' => 'category_id', 'parent_id' => 'category_parent', 'lineage' => 'category_lineage', 'deep' => 'category_deep');
        $this->load->library('mahana_hierarchy');
        $this->mahana_hierarchy->initialize($config);
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur

    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();

            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('jadwal_kegiatan');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_post');
            $umbrella->set_subject('Jadwal kegiatan');
            $umbrella->columns('post_title', 'post_author', 'post_date', 'post_name');
            $umbrella->where('tb_post.post_type', 'jadwal');
            $umbrella->order_by('post_id', 'desc');
            $umbrella->required_fields('category');
            $umbrella->unset_texteditor('post_title', 'full_text');
            $umbrella->unset_texteditor('post_meta_desc', 'full_text');
            $umbrella->unset_texteditor('post_meta_keyword', 'full_text');
//            $umbrella->field_type('post_position', 'hidden');
            $umbrella->field_type('post_title', 'string');
            $umbrella->field_type('post_meta_keyword', 'string');
            $umbrella->field_type('post_name', 'hidden');
            $umbrella->field_type('post_type', 'hidden');
            $umbrella->field_type('post_mime_type', 'hidden');
            $umbrella->field_type('post_author', 'hidden');
            $umbrella->field_type('post_modified', 'hidden');

            $umbrella->add_fields('post_title', 'post_content','post_position', 'post_meta_desc', 'post_date', 'media_gallery', 'post_type', 'post_author', 'post_mime_type', 'post_name', 'post_parent', 'post_modified');
            $umbrella->edit_fields('post_title', 'post_content','post_position', 'post_meta_desc', 'post_date', 'media_gallery', 'post_type', 'post_author', 'post_mime_type', 'post_name', 'post_parent', 'post_modified');
            $umbrella->set_relation_n_n('tags', 'tb_terms', 'tb_category', 'post_id', 'category_id', 'category_name', null, array('category_type' => 'tags'));
            $umbrella->set_relation_n_n('category', 'tb_terms', 'tb_category', 'post_id', 'category_id', 'category_name', null, array('category_type' => 'category'));
            $umbrella->display_as('post_title', 'kegiatan');
            $umbrella->display_as('post_content', 'detail kegiatan');
            $umbrella->display_as('post_meta_desc', 'Alamat');
            $umbrella->display_as('post_date', 'Tanggal dan Waktu kegiatan');
            $umbrella->display_as('media_gallery', '');
            $umbrella->display_as('post_parent', '');
            $umbrella->display_as('post_comment_count', 'Comment');
            $umbrella->display_as('post_name', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_column('post_title', array($this, '_full_text'));
            $umbrella->callback_column('category', array($this, '_category'));
            $umbrella->callback_column('post_comment_count', array($this, '_callback_post_comment_count'));
//            $umbrella->callback_column('post_title', array($this, '_callback_title'));
            $umbrella->callback_column('post_date', array($this, '_post_date'));
            $umbrella->callback_column('post_name', array($this, '_callback_post_id'));
            $umbrella->callback_add_field('post_meta_desc', array($this, '_petamaps'));
            $umbrella->callback_edit_field('post_meta_desc', array($this, '_petamaps_edit'));
            $umbrella->callback_edit_field('post_parent', array($this, '_edit_callback_post_parent'));
            $umbrella->callback_edit_field('media_gallery', array($this, '_edit_callback_media_gallery'));
            $umbrella->callback_edit_field('category', array($this, '_edit_callback_category'));
            $umbrella->callback_after_insert(array($this, 'notification_user_after_insert'));
            $umbrella->callback_after_update(array($this, 'notification_user_after_update'));
            $umbrella->callback_after_delete(array($this, 'notification_user_after_delete'));
            $umbrella->set_listfilter(true);
            $umbrella->callback_add_field('media_gallery', function () {
                return '';
            });
            $umbrella->callback_add_field('category', function () {
                $group_id = array();
                $input = '<select id="field-category" name="category[]" multiple="multiple" size="8" class="chosen-multiple-select field-category" style="width:400px" data-placeholder="Category">';

                $category = $this->mahana_hierarchy->where(array('category_type' => 'category'))->get();

                foreach ($category as $cat) {
                    $this->db->select('post_id');
                    $this->db->where('terms_type', 'category_role');
                    $this->db->where('tb_terms.category_id', $cat['category_id']);
                    $group_role_id = $this->db->get('tb_terms')->result();
                    if (!empty($group_role_id)) {
                        foreach ($group_role_id as $role_id) {
                            $group_id[] = (int)$role_id->post_id;
                        }
                        if ($this->ion_auth->in_group($group_id)) {
                            $splash = '';
                            $count = $cat['category_deep'];
                            if ($cat['category_id'])
                                for ($y = 0; $y < $count; $y++) {
                                    $splash .= "|--";
                                }
                            $input .= '<option value="' . $cat['category_id'] . '">' . $splash . ' ' . $cat['category_name'] . '</option>';
                        }
                    }
                    unset($group_id);
                }
                $input .= '</select>';
                return $input;
            });
            $typefilter = $this->uri->segment(4);
            if ($typefilter == "publish" || $typefilter == "draf" || $typefilter == "trash") {
                $umbrella->where('tb_post.post_status', $typefilter);
            }
            $umbrella->callback_add_field('post_parent', function () {
                return '';
            });
            //Set Column Abigous
            $state = $umbrella->getState();
            if ($state != 'ajax_list' && $state != 'ajax_list_info') {
                $umbrella->set_relation('post_parent', 'tb_post', 'post_name', array('post_status' => 'Publish'));
            }
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *<select id='field-{$field_info_type->name}' name='{$field_info_type->name}[]' multiple='multiple' size='8' class='$css_class field-{$field_info_type->name}' data-placeholder='$select_title' style='$width_style' >
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return
    *====================================================
    *
    */
    function _petamaps()
    {

        return '<div id="googleMap" style="width:100%;height:400px;"></div>
        <p id="demo" style="width:100%;height:10px;"></p>
        
        <script>
        var x = document.getElementById("demo");
        getLocation();
     
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else { 
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            
            function showPosition(position) {
                myMap( position.coords.latitude,position.coords.longitude) 
            }
            
            function myMap(get_lat,get_long) {
            var mylocation = {lat: get_lat, lng: get_long};
            var mapProp= {center:mylocation,zoom:15,};
            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            var infowindow = new google.maps.InfoWindow;
            var address=[];
            
            var marker = new google.maps.Marker({map: map,   draggable: true,animation: google.maps.Animation.DROP, position: mylocation});
            address["lat_address"]="";
            address["lng_address"]="";
             infowindow.setContent(\'<b>Lokasi Kegiatan</b>\');
             marker.addListener(\'click\', function() {infowindow.open(map, marker);});
             marker.addListener(\'click\', toggleBounce);
               
            google.maps.event.addListener(marker, \'dragend\', function(evt){
            document.getElementById("field-post_position").value =evt.latLng.lat().toFixed(3)+"|"+evt.latLng.lng().toFixed(3) ;
             address["lat_address"]=evt.latLng.lat().toFixed(3);
             address["lng_address"]=evt.latLng.lng().toFixed(3);
            });
           
             function toggleBounce() {
                if (marker.getAnimation() !== null) {
                  marker.setAnimation(null);
                } else {
                  marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }
                      
          }
                
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3S_xwvhUJRITkYQNQzE_v9Si06TpGIsE&callback=myMap"></script>';

    }
    function _petamaps_edit()
    {

//        return '<div id="googleMap" style="width:100%;height:400px;"></div>
//        <p id="demo" style="width:100%;height:10px;"></p>
//
//        <script>
//        var x = document.getElementById("demo");
//        var geolocation = document.getElementById("field-post_position").value;
//        var geo_explode=geolocation.split("|");
//     console.log(geo_explode);
//            myMap();
//
//            function myMap() {
//            var mylocation = {lat: geo_explode[0], lng: geo_explode[1]};
//            var mapProp= {center:mylocation,zoom:15,};
//            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
//            var infowindow = new google.maps.InfoWindow;
//            var address=[];
//
//            var marker = new google.maps.Marker({map: map,   draggable: true,animation: google.maps.Animation.DROP, position: mylocation});
//            address["lat_address"]="";
//            address["lng_address"]="";
//             infowindow.setContent(\'<b>Lokasi Kegiatan</b>\');
//             marker.addListener(\'click\', function() {infowindow.open(map, marker);});
//             marker.addListener(\'click\', toggleBounce);
//
//            google.maps.event.addListener(marker, \'dragend\', function(evt){
//            document.getElementById("field-post_position").value =evt.latLng.lat().toFixed(3)+"|"+evt.latLng.lng().toFixed(3) ;
//             address["lat_address"]=evt.latLng.lat().toFixed(3);
//             address["lng_address"]=evt.latLng.lng().toFixed(3);
//            });
//
//             function toggleBounce() {
//                if (marker.getAnimation() !== null) {
//                  marker.setAnimation(null);
//                } else {
//                  marker.setAnimation(google.maps.Animation.BOUNCE);
//                }
//            }
//
//          }
//
//        </script>
//
//        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3S_xwvhUJRITkYQNQzE_v9Si06TpGIsE&callback=myMap"></script>';

        return '<div id="googleMap" style="width:100%;height:400px;"></div>
        <p id="demo" style="width:100%;height:10px;"></p>
        
        <script>
        var x = document.getElementById("demo");
        var geolocation = document.getElementById("field-post_position").value;
        var geo_explode=geolocation.split("|");
       
        getLocation();
     
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else { 
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            
            function showPosition(position) {
                myMap( position.coords.latitude,position.coords.longitude) 
               
            }
            
            function myMap(get_lat,get_long) {
            var mylocation = {lat: parseFloat(geo_explode[0]), lng:  parseFloat(geo_explode[1])};
            var mapProp= {center:mylocation,zoom:15,};
            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            var infowindow = new google.maps.InfoWindow;
            var address=[];
            
            var marker = new google.maps.Marker({map: map,   draggable: true,animation: google.maps.Animation.DROP, position: mylocation});
            address["lat_address"]="";
            address["lng_address"]="";
             infowindow.setContent(\'<b>Lokasi Kegiatan</b>\');
             marker.addListener(\'click\', function() {infowindow.open(map, marker);});
             marker.addListener(\'click\', toggleBounce);
               
            google.maps.event.addListener(marker, \'dragend\', function(evt){
            document.getElementById("field-post_position").value =evt.latLng.lat().toFixed(3)+"|"+evt.latLng.lng().toFixed(3) ;
             address["lat_address"]=evt.latLng.lat().toFixed(3);
             address["lng_address"]=evt.latLng.lng().toFixed(3);
            });
           
             function toggleBounce() {
                if (marker.getAnimation() !== null) {
                  marker.setAnimation(null);
                } else {
                  marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }
                      
          }
                
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3S_xwvhUJRITkYQNQzE_v9Si06TpGIsE&callback=myMap"></script>';

    }
    function _full_text($value, $row)
    {
        return $value = substr(strip_tags($row->post_title), 0, 100) . '...';
        // $this->character_limiter(strip_tags($row->post_title),30,"...");
    }

    public function _edit_callback_category($value, $primary_key, $row, $values)
    {
        $group_id = array();
        $input = '<select id="field-category" name="category[]" multiple="multiple" size="8" class="chosen-multiple-select field-category" style="width:400px" data-placeholder="Category">';

        $category = $this->mahana_hierarchy->where(array('category_type' => 'category'))->get();

        foreach ($category as $cat) {
            $this->db->select('post_id');
            $this->db->where('terms_type', 'category_role');
            $this->db->where('tb_terms.category_id', $cat['category_id']);
            $group_role_id = $this->db->get('tb_terms')->result();
            if (!empty($group_role_id)) {
                foreach ($group_role_id as $role_id) {
                    $group_id[] = (int)$role_id->post_id;
                }
                if ($this->ion_auth->in_group($group_id)) {
                    $splash = '';
                    $count = $cat['category_deep'];
                    if ($cat['category_id'])
                        for ($y = 0; $y < $count; $y++) {
                            $splash .= "-";
                        }
                    if (isset($value[$cat['category_id']])) {
                        $input .= '<option value="' . $cat['category_id'] . '" selected="selected">' . $splash . ' ' . $cat['category_name'] . '</option>';
                    } else {
                        $input .= '<option value="' . $cat['category_id'] . '">' . $splash . ' ' . $cat['category_name'] . '</option>';
                    }

                }
            }
            unset($group_id);
        }

        $input .= '</select>';
        return $input;
    }

    public function _set_callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        $post_array['post_modified'] = date("Y-m-d H:i:s");

        if (empty($post_array['post_date']))
            $post_array['post_date'] = date("Y-m-d H:i:s");

        $config = array(
            'field' => 'post_name',
            'title' => 'post_title',
            'table' => 'tb_post',
            'id' => 'post_id',
        );
        $post_array['post_mime_type'] = 'text';
        $post_array['post_type'] = 'jadwal';
        $post_array['post_author'] = $this->ion_auth->user()->row()->username;
        $this->load->library('slug', $config);
        $post_array['post_name'] = $this->slug->create_uri($post_array['post_title']);

        return $post_array;
    }

    public function _set_callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        $post_array['post_modified'] = date("Y-m-d H:i:s");
        if (empty($post_array['post_status']))
            $post_array['post_priority'] = "Draft";
        if (empty($post_array['post_priority']))
            $post_array['post_priority'] = "Normal";
        if (empty($post_array['post_comment']))
            $post_array['post_comment'] = "Enable";
        if (empty($post_array['post_seo_title']))
            $post_array['post_seo_title'] = strip_tags(substr($post_array['post_title'], 0, 70));
        if (empty($post_array['post_meta_desc']))
            $post_array['post_meta_desc'] = strip_tags(substr($post_array['post_content'], 0, 156));
        if (empty($post_array['post_meta_keyword']))
            $post_array['post_meta_keyword'] = $post_array['post_seo_title'];
        if (empty($post_array['post_date'])) {
            $post_array['post_date'] = date("Y-m-d H:i:s");
        } else {
            $post_array['post_date'] = date("Y-m-d H:i:s", strtotime($post_array['post_date']));
        }

        $config = array(
            'field' => 'post_name',
            'title' => 'post_title',
            'table' => 'tb_post',
            'id' => 'post_id',
        );

        $this->load->library('slug', $config);
        $post_array['post_name'] = $this->slug->create_uri($post_array['post_title']);

        return $post_array;
    }

    public function _category($value)
    {
        $this->M_base_config->cekaAuth();
        if (empty($value)) {
            return '-';
        } else {
            return $value;
        }

    }

    public function _post_date($value, $row)
    {
        $this->M_base_config->cekaAuth();
        if ($row->post_status == 'Publish') {
            $status = '<span class="uk-badge uk-badge-success">' . lang('colum_publish') . '</span>';
        } else if ($row->post_status == 'Trash') {
            $status = '<span class="uk-badge uk-badge-danger">' . lang('colum_trash') . '</span>';
        } else {
            $status = '<span class="uk-badge uk-badge-warning">' . lang('colum_draf') . '</span>';
        }
        return $value . "<br><small>" . lang('updatein') . " : " . $this->base_config->timeAgo($row->post_modified) . "</small><br>" . $status;
    }

    public function _callback_title($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $type_con = $this->uri->segment(2);
        //return "<a title='$value' href='" . site_url('cms/' . $type_con . '/index/edit/' . $row->post_id) . "'><b>$value</b></a>";
        return "<a target='_blank' title='$value' href='" . site_url('/' . $row->post_name) . "'><b>$value</b></a>";
    }

    public function _callback_post_comment_count($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return lang('comment') . " : " . $row->post_comment_count . " <br> " . lang('views') . ' : ' . $row->post_view;
    }

    public function _callback_post_id($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->post_id . '" name="' . $row->post_id . '" id="' . $row->post_id . '" data-md-icheck />';
    }

    public function _edit_callback_post_parent($value, $primary_key, $row, $values)
    {
        $this->M_base_config->cekaAuth();
        $data = "";
        if (!empty($value)) {
            $url = $this->M_base_config->getSimpleData(array('table' => 'tb_post', 'return' => 'post_name', 'where' => array(array('wherefield' => 'post_id', 'where_value' => $value))));
            $data .= '<li class="uk-position-relative"><button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute mediaempety"></button><img src="img/load/200/200/png/' . $url . '" alt="" class="img_medium"/><input type="hidden" value="' . $value . '" id="field-post_parent" name="post_parent" /></li>';
        }
        return $data;
    }

    public function _edit_callback_media_gallery($value, $primary_key, $row, $values)
    {
        $this->M_base_config->cekaAuth();
        $data = "";
        if (!empty($value)) {
            foreach ($value as $key => $url) {
                $data .= '<li class="uk-position-relative"><button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute mediaempety"></button><img src="img/load/200/200/png/' . $url . '" alt="" class="img_small"/><input type="hidden" value="' . $key . '" id="field-media_gallery" name="media_gallery[]" /></li>';
            }
        }
        return $data;
    }

    public function set_post_status()
    {
        $this->M_base_config->cekaAuth();
        if ($_POST) {
            $post_id = $this->input->post('id');
            $post_status = $this->input->post('status');
            $data = array('post_status' => $post_status);
            $this->db->where('post_id', $post_id);
            $this->db->update('tb_post', $data);
        }
    }

    /*
   ============================================
   Record User Notification callback
   ============================================
   */
    public function notification_user_after_insert($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('add_notif'), 'icon' => 'add', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_update($post_array, $primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i>  ' . lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('update_notif'), 'icon' => 'update', 'link' => 'cms/posts/index/edit/' . $primary_key, 'title' => $post_array['post_title']);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    public function notification_user_after_delete($primary_key)
    {
        $user = $this->ion_auth->user()->row();
        if (!$this->ion_auth->is_admin()) {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
            $admin = $this->ion_auth->users(1)->result();
            foreach ($admin as $valadmin) {
                $val2 = array('type' => 'timeline', 'user' => $valadmin->id, 'parent' => $primary_key, 'desc' => '<i>' . $user->user_display_name . '</i> ' . lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
                $this->M_base_config->insertnotif($val2);
            }
        } else {
            $val = array('type' => 'timeline', 'user' => $user->id, 'parent' => $primary_key, 'desc' => lang('delete_notif'), 'link' => 'javascript:void(0)', 'icon' => 'delete', 'title' => $primary_key);
            $this->M_base_config->insertnotif($val);
        }

        return true;
    }

    /*
    ============================================
    Add Multiple
    ============================================
    */
    public function add_multiple()
    {
        $this->M_base_config->cekaAuth();
        if ($_POST) {
            $category_name = $this->input->post('category_name');
            $category_type = $this->input->post('category_type');

            $config = array(
                'field' => 'category_slug',
                'title' => 'category_name',
                'table' => 'tb_category',
                'id' => 'category_id',
            );
            $this->load->library('slug', $config);
            $category_slug = $this->slug->create_uri($category_name);
            $data = array(
                'category_name' => $category_name,
                'category_slug' => $category_slug,
                'category_type' => $category_type,
            );
            if ($this->db->insert('tb_category', $data)) {
                $data = array(
                    'category_id' => $this->db->insert_id(),
                    'category_name' => $category_name,
                    'category_slug' => $category_slug,
                    'category_type' => $category_type,
                );
                echo json_encode($data);
                exit;
            }
        }
    }

}