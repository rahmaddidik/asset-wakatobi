<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
//require APPPATH . '/libraries/Umbrella_REST_Controller.php';
//require APPPATH . '/libraries/Umbrella_Api_CRUD_Action.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Parser $parser
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property M_base_config $M_base_config
 * @property base_config $base_config
 * @property CI_Lang $lang
 * @property CI_URI $uri
 * @property CI_DB_query_builder|CI_DB_mysqli_driver $db
 * @property CI_Config $config
 * @property CI_User_agent $agent
 * @property CI_Email $email
 * @property Front front
 * @property Base_config Base_config
 * @property Slug slug
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_base_config');
    }



    public function maintenance_get()
    {
        $data = $this->base_config->front_setting();
        $company = array(
            'where' => array(
                array(
                    'wherefield' => 'setting_type',
                    'where_value' => 'setting_company'
                )
            )
        );
        $company_result = $this->mymodel->get_multi_setting_sigle($company);
        $data = array_merge($data, $company_result);
        if( isset( $data['maintenance'] ) && $data['maintenance'] ){
            $msg = '';
            $setting = [
                'maintenance' => $data['maintenance'],
                'maintenance_content' => $data['maintenance_content']
            ];
        }else{
            $msg = 'Tidak ada data';
            $setting = [
                'maintenance' => 0,
                'maintenance_content' => ""
            ];
        }
        $this->set_response(['status' => 1, 'message' => $msg, 'data' => $setting ], REST_Controller::HTTP_OK);
    }

    public function tes_get()
    {
        $this->set_response(['status' => 1, 'message' => 'testing', 'data' => [] ], REST_Controller::HTTP_OK);
    }

    public function setting_post()
    {
        
    }

    public function posts($args=NULL){
        $umbrella=new Umbrella_Api_CRUD_Action('tb_post');
        return $umbrella;
    }

    public function jadwal_kegiatan_get()
    {
        $ip=$_SERVER['REMOTE_ADDR'];

        $this->set_response(['status' => 1, 'message' => $ip, 'data' =>[] ], REST_Controller::HTTP_OK);


    }
}
