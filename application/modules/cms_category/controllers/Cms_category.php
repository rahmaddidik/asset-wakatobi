<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $config = array('table' => 'tb_category', 'primary_key' => 'category_id', 'parent_id' => 'category_parent', 'lineage' => 'category_lineage', 'deep' => 'category_deep');
        $this->load->library('mahana_hierarchy');
        $this->mahana_hierarchy->initialize($config);
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            //Get Panel setting
            /*echo $this->uri->segment(3);
            exit;*/
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('category');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_category');
            $umbrella->set_subject('Category');
            $umbrella->where('tb_category.category_type', 'category');
            $umbrella->order_by('category_parent', 'desc');
            $umbrella->order_by('category_lineage', 'asc');
            $umbrella->columns('category_name', 'category_desc', 'category_slug', 'category_role', 'category_type');
            $umbrella->add_fields('category_name', 'category_desc', 'category_slug', 'category_parent', 'category_type', 'category_lineage', 'category_deep', 'category_role');
            $umbrella->edit_fields('category_name', 'category_desc', 'category_slug', 'category_parent', 'category_type', 'category_lineage', 'category_deep', 'category_role');
            $umbrella->field_type('category_slug', 'hidden');
            $umbrella->field_type('category_type', 'hidden');
            $umbrella->field_type('category_lineage', 'hidden');
            $umbrella->field_type('category_deep', 'hidden');
            $umbrella->set_relation_n_n('category_role', 'tb_terms', 'tb_groups', 'category_id', 'post_id', 'name');
            $umbrella->display_as('category_type', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->display_as('category_name', lang('cat_name'));
            $umbrella->display_as('category_desc', lang('cat_desc'));
            $umbrella->display_as('category_slug', lang('cat_slug'));
            $umbrella->display_as('category_parent', lang('cat_parent'));
            $umbrella->callback_column('category_type', array($this, '_callback_category_type'));
            $umbrella->callback_column('category_name', array($this, '_callback_category_name'));
            $umbrella->required_fields('category_role');
            $umbrella->callback_edit_field('category_parent', array($this, '_set_field_category_parent_callback'));
            $umbrella->callback_before_insert(array($this, '_set_callback_before_insert'));
            $umbrella->callback_before_update(array($this, '_set_callback_before_update'));
            $umbrella->callback_after_insert(array($this, '_set_callback_after_insert'));
            $umbrella->callback_after_update(array($this, '_set_callback_after_update'));
            $umbrella->callback_before_delete(array($this, '_set_callback_before_delete'));
            $umbrella->callback_after_delete(array($this, '_set_callback_after_delete'));
            $umbrella->unset_texteditor('category_desc', 'full_text');
            $umbrella->set_composer(false);
            $umbrella->set_listfilter(false);
            $umbrella->callback_add_field('category_parent', function () {
                return $this->_set_add_category_parent_callback();
            });
            $state = $umbrella->getState();
            if ($state != 'ajax_list' && $state != 'ajax_list_info') {
                $umbrella->set_relation('category_parent', 'tb_category', 'category_name', array('category_type' => 'category'));
            } else {
                $this->mahana_hierarchy->resync();
            }

            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) {
                redirect('cms/404', 'refresh');
            } else {
                show_error($e->getMessage());
            }
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _set_add_category_parent_callback()
    {
        $listcategory = $this->mahana_hierarchy->where(array('category_type' => 'category'))->get();
        $data = '<select name="category_parent" id="field-category_parent" data-md-selectize><option>====</option>';
        for ($x = 0; $x < count($listcategory); $x++) {
            $count = $listcategory[$x]['category_deep'];
            $splash = "";
            for ($y = 0; $y < $count; $y++) {
                $splash .= "&nbsp;-&nbsp;";
            }
            $data .= '<option value="' . $listcategory[$x]['category_id'] . '">' . $splash . $listcategory[$x]['category_name'] . '</option>';
        }
        $data .= '</select>';
        return $data;
    }

    public function _set_field_category_parent_callback($value, $primary_key)
    {
        $listcategory = $this->mahana_hierarchy->where(array('category_type' => 'category'))->get();
        $data = '<select name="category_parent" id="field-category_parent" data-md-selectize><option>====</option>';
        for ($x = 0; $x < count($listcategory); $x++) {
            $count = $listcategory[$x]['category_deep'];
            $splash = "";
            for ($y = 0; $y < $count; $y++) {
                $splash .= "&nbsp;-&nbsp;";
            }
            if ($value == $listcategory[$x]['category_id']) {
                $data .= '<option selected value="' . $listcategory[$x]['category_id'] . '">' . $splash . $listcategory[$x]['category_name'] . '</option>';
            } else if ($primary_key == $listcategory[$x]['category_id']) {

            } else {
                $data .= '<option value="' . $listcategory[$x]['category_id'] . '">' . $splash . $listcategory[$x]['category_name'] . '</option>';
            }
        }
        $data .= '</select>';
        return $data;
    }

    public function _callback_category_type($value, $row)
    {
        $this->M_base_config->cekaAuth();
        return '<input type="checkbox" class="removelist" value="' . $row->category_id . '" name="' . $row->category_id . '" id="' . $row->category_id . '" data-md-icheck />';
    }

    public function _callback_category_name($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $value = $this->security->xss_clean($value);
        $splashhori = "";
        $splashmargin = "";
        $splashitem = "";
        $count = $row->category_deep;
        for ($x = 0; $x < $count; $x++) {
            $splashmargin .= "<span class='splashmargin'>&nbsp;</span>";
            if ($x == 0) {
                $splashhori .= "<span class='splashcatopen'>&nbsp;</span>";
            }
            $splashitem .= "<span class='splashcat'>&nbsp;</span>";
        }
        $splash = $splashmargin . $splashhori . $splashitem;
        $type_con = $this->uri->segment(2);
        $new_name = "<b>" . $splash . " <a title='$value' href='" . site_url('cms/' . $type_con . '/index/edit/' . $row->category_id) . "'>" . $value . "</b></a>";
        return $new_name;
    }

    public function _set_callback_before_insert($post_array)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['category_type']))
            $post_array['category_type'] = "category";

        $config = array(
            'field' => 'category_slug',
            'title' => 'category_name',
            'table' => 'tb_category',
            'id' => 'category_id',
        );
        $this->load->library('slug', $config);
        if (empty($post_array['category_slug']))
            $post_array['category_slug'] = $this->slug->create_uri($post_array['category_name']);
        return $post_array;
    }

    public function _set_callback_before_update($post_array)
    {
        $this->M_base_config->cekaAuth();
        if (empty($post_array['category_type']))
            $post_array['category_type'] = "category";

        $config = array(
            'field' => 'category_slug',
            'title' => 'category_name',
            'table' => 'tb_category',
            'id' => 'category_id',
        );
        $this->load->library('slug', $config);
        $post_array['category_slug'] = $this->slug->create_uri($post_array['category_name']);

        return $post_array;
    }

    public function _set_callback_before_delete($primary_key)
    {
        $getparent = $this->mahana_hierarchy->get_parent($primary_key);
        $getchild = $this->mahana_hierarchy->get_children($primary_key);
        if (!empty($getchild) && !empty($getparent)) {
            for ($x = 0; $x < count($getchild); $x++) {
                $data = array('category_parent' => $getparent->category_id);
                $this->db->where('category_id', $getchild[$x]['category_id']);
                $this->db->update('tb_category', $data);
            }
        } else if (!empty($getchild) && empty($getparent)) {
            for ($x = 0; $x < count($getchild); $x++) {
                $data = array('category_parent' => 0);
                $this->db->where('category_id', $getchild[$x]['category_id']);
                $this->db->update('tb_category', $data);
            }
        } else {

        }
        $this->mahana_hierarchy->resync();
        return true;
    }

    public function _set_callback_after_update($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $this->mahana_hierarchy->resync();
        return true;
    }

    public function _set_callback_after_insert($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $this->mahana_hierarchy->resync();
        return true;
    }

    public function _set_callback_after_delete($post_array, $primary_key)
    {
        $this->M_base_config->cekaAuth();
        $this->mahana_hierarchy->resync();
        $this->db->where('category_desc', $primary_key)->delete('tb_category');
        return true;
    }


}