<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property  M_base_config $M_base_config
 * @property  base_config $base_config
 * @property  Ion_auth|Ion_auth_model $ion_auth
 * @property  CI_Lang $lang
 * @property  CI_URI $uri
 * @property  CI_DB_query_builder $db
 * @property  CI_Config $config
 * @property  CI_Input $input
 * @property  CI_User_agent $agent
 * @property  Mahana_hierarchy $mahana_hierarchy
 * @property  Slug $slug
 * @property  CI_Security $security
 */
class Cms_mitra extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // load base config library
        date_default_timezone_set('Asia/Jakarta');

        //load not all reqruitment library
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        // load lang
        $this->lang->load('auth');
        //Instance class to make amazing fitur
    }

    public function index()
    {
        try {
            //cek auth
            $this->M_base_config->cekaAuth();
            $data = $this->base_config->panel_setting();
            $umbrella = $this->base_config->groups_access('widgets');
            $umbrella->set_theme('twitter-bootstrap');
            $umbrella->set_table('tb_setting');
            $umbrella->set_subject('Mitra');
            $umbrella->columns('setting_value', 'setting_name', 'setting_desc', 'setting_type');
            $umbrella->where('setting_type', 'setting_mitra');
            $umbrella->order_by('setting_id', 'asc');
            $umbrella->display_as('setting_type', '<input type="checkbox" name="removeall" id="removeall" data-md-icheck />');
            $umbrella->field_type('setting_type', 'hidden', 'setting_mitra');
            $umbrella->set_field_upload('setting_value', 'assets/uploads/');
            $umbrella->unset_export();
            $umbrella->unset_print();
            $umbrella->edit_fields('setting_name','setting_desc','setting_value');
            $umbrella->display_as('setting_value', lang('mitra_img'));
            $umbrella->display_as('setting_name', lang('mitra_title'));
            $umbrella->display_as('setting_desc', lang('mitra_link'));
            $umbrella->callback_column('setting_value', array($this, '_callback_setting_value'));
            $umbrella->callback_column('setting_type', array($this, '_callback_setting_type'));
            $output = $umbrella->render();
            $data['asset'] = $this->base_config->asset_back();
            $data['viewspage'] = 'crud';
            $data['nav'] = 'yes';
            $this->base_config->_render_crud($data, $output);
        } catch (Exception $e) {
            show_error($e->getMessage());
        }

    }

    /*
    *
    *====================================================
    * Dibawah ini merupakan Method/ call back yang dibutuhkan oleh grocery sebagai setingan tambahan
    * Return 
    *====================================================
    *
    */
    public function _callback_setting_value($value, $row)
    {
        $this->M_base_config->cekaAuth();
        $return = '<div class="text-left"><a href="' . base_url() . 'img/load/100/100/full/' . $value . '" class="image-thumbnail"><img src="' . base_url() . 'img/load/100/10/full/' . $value . '"  alt=""></a></div>';

        return $return;
    }

    public function _callback_setting_type($value, $row)
    {
        return '<input type="checkbox" class="removelist" value="' . $row->setting_id . '" name="' . $row->setting_id . '" id="' . $row->setting_id . '" data-md-icheck />';
    }
}