<?php $__env->startSection('title_bar'); ?>
    <br>
    <h3 class="bold" style="letter-spacing: 1.2px">TENTANG KAMI</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body'); ?>
    <div class="container ">
        <div class="row content-singgel_page about">
            <div class="col-sm-8 offset-sm-2">
                <img src="assets//images/tentang-pemerintah-wakatobi.jpg" alt="" >
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
                    at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
                    as opposed to using 'Content here, content here', making it look like readable English. Many desktop
                    publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search
                    for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over
                    the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>