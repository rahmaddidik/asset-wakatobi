<!-- secondary sidebar -->
<aside id="sidebar_secondary" class="tabbed_sidebar">
    <ul class="uk-tab uk-tab-icons uk-tab-grid" data-uk-tab="{connect:'#dashboard_sidebar_tabs', animation:'slide-horizontal'}">
        <li class="uk-active uk-width-1-2"><a href="#"><i class="material-icons">&#xE422;</i></a></li>
        <li class="uk-width-1-2 chat_sidebar_tab"><a href="#"><i class="material-icons">&#xE0B7;</i></a></li>
    </ul>

    <div class="scrollbar-inner">
        <ul style="margin-top:40px;" id="dashboard_sidebar_tabs" class="uk-switcher">
            <li>
                <div class="timeline timeline_small uk-margin-bottom">
                    <?php
                        $notif = get_instance()->base_config->notificationlist();
                        $nonotif2 =false;
                    ?>
                    <?php $__currentLoopData = $notif; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notifval2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($notifval2->notification_type  == 'timeline'): ?>
                            <?php $nonotif2 =true   ?>
                            <div class="timeline_item">
                                <div class="timeline_icon timeline_icon_success"><i class="material-icons"><?php if($notifval2->notification_icon == 'update'): ?> &#xE150; <?php elseif($notifval2->notification_icon == 'delete'): ?> &#xE15C; <?php else: ?> &#xE147; <?php endif; ?></i></div>
                                <div class="timeline_date">
                                    <?php echo e(get_instance()->base_config->timeAgo($notifval2->notification_date)); ?>

                                </div>
                                <div class="timeline_content"><?php echo $notifval2->notification_desc; ?></div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </li>
            <li>
                <ul class="md-list md-list-addon ">
                    <?php
                        $userpanellist = get_instance()->base_config->userpanellist();
                    ?>
                    <?php $__currentLoopData = $userpanellist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <div class="md-list-addon-element">
                                <span class="element-status element-status-<?php if($user->active == 1): ?> success <?php else: ?> danger <?php endif; ?>"></span>
                                <img class="md-user-image md-list-addon-avatar" src="img/load/80/80/crop/<?php echo e($user->user_avatar); ?>" alt=""/>
                            </div>
                            <div class="md-list-content">
                                <div class="md-list-action-placeholder"></div>
                                <span class="md-list-heading"><?php echo e($user->user_display_name); ?></span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate"><?php echo e($user->user_company); ?></span>
                            </div>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </li>
        </ul>
    </div>

    <button type="button" class="chat_sidebar_close uk-close"></button>
    <div class="chat_submit_box">
        <div class="uk-input-group">
            <input type="text" class="md-input" name="submit_message" id="submit_message" placeholder="Send message">
            <span class="uk-input-group-addon">
                    <a href="#"><i class="material-icons md-24">&#xE163;</i></a>
                </span>
        </div>
    </div>
</aside>
<!-- secondary sidebar end -->