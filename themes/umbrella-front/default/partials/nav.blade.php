<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #FFFFFF !important;">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="{{ assets_front() }}img/logo_pemkot_wakatobi.png" alt="" style="width:auto;height:45px;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarNavDropdown">
            <ul class="nav navbar-nav ml-auto">
                <a class="mx-2 nav-item nav-link {{ $menu_active=='Beranda' ? 'active' :'' }}" href="{{base_url()}}">Beranda</a>
                <a class="mx-2 nav-item nav-link {{ $menu_active=='Tentang' ? 'active' :'' }}" href="{{base_url()}}tentang">Tentang</a>
                <a class="mx-2 nav-item nav-link {{ $menu_active=='Hubungi Kami' ? 'active' :'' }}" href="{{base_url()}}hubungikami">Hubungi kami</a>
                <a href="{{base_url('cms')}}"><button type="button" class="mx-2 btn btn-outline-primary" >LOGIN ADMIN</button></a>

            </ul>
        </div>
    </div>
</nav>