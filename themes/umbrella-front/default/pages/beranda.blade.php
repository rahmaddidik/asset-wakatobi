@extends('layouts.app')
@section('title_bar')
    <h4 class="bold" style="letter-spacing: 1.2px">SIKAP WAKATOBI</h4>
    <h4 class="thin" style="font-weight: 600;letter-spacing: 0.5px">Sistem Informasi Kegiatan Pemda
        Wakatobi</h4>
@endsection

@section('body')
    <div class="container">
        <div class="row content-kegiatan">
            {{--MODAL--}}
            <div class="modal fade" id="modal-kegiatan" style="cursor: no-drop !important;">
                <div class="modal-dialog" style="max-width: 95% !important; ">
                    <div class="modal-content" style="cursor: default; !important;">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title" id="title-modal"></h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body" id="body-modal">

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
            {{--MODAL--}}

            <!-- CALENDAR kegiatan -->
            <div class="col-sm-5">
                <ul class="nav w-100 bg-white selectdate">
                    <li class="nav-item col-3">
                        <select class="custom_select" id="tgl">
                            <option value="" selected disabled hidden>Tgl</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                    </li>
                    <li class="nav-item col-4">
                        <select class="custom_select" id="bulan">
                            <option value="" selected disabled hidden>Bln</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </li>
                    <li class="nav-item col-3">
                        <select class="custom_select" id="tahun">
                            <option value="" selected disabled hidden>Thn</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                            <option value="2030">2030</option>
                            <option value="2031">2031</option>
                            <option value="2032">2032</option>
                            <option value="2033">2033</option>
                            <option value="2034">2034</option>
                            <option value="2035">2035</option>
                            <option value="2036">2036</option>
                            <option value="2037">2037</option>
                            <option value="2038">2038</option>
                            <option value="2039">2039</option>
                            <option value="2040">2040</option>
                        </select>
                    </li>
                    <li class="nav-item col-1 ">
                        <button class="btn text-center" style="background-color: white;height: 41px">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </li>
                </ul>

                <div id="custom_calendar">
                    <div class="container noselect">
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-2">
                                <button class="fa fa-chevron-left btn left-btn" onclick="select_month('previous')"></button>
                            </div>
                            <div class="col-8">
                                <div class="moth_select">

                                    <div class="text-center">
                                        <h5 id="month-select" style="line-height: 10px;"></h5>
                                        <span id="year-select" class="medium "></span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-2">
                                <button class="fa fa-chevron-right btn" onclick="select_month('next')"></button>
                            </div>
                        </div>
                        <div class="row bg-white">
                            <table class="text-center table_custom ">
                                <thead class="thin">
                                <tr style="background-color: #e8e8e8">
                                    <th width="14.3%" id="sen">Sen</th>
                                    <th width="14.3%" id="sel">Sel</th>
                                    <th width="14.3%" id="rab">Rab</th>
                                    <th width="14.3%" id="kam">Kam</th>
                                    <th width="14.3%" id="jum">Jum</th>
                                    <th width="14.3%" id="sab">Sab</th>
                                    <th width="14.3%" id="ming">Ming</th>
                                </tr>
                                </thead>
                                <tbody id="content_costum_calendar">
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">26</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">27</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">28</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">29</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">30</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">31</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">1</td>
                                </tr>
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">2</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">3</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">4</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">5</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">6</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">7</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">8</td>
                                </tr>
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">9</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">10</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">11</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">12</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">13 </td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">14</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">16</td>
                                </tr>
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">17</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">18</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">19</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">20</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">21</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">22</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">23</td>
                                </tr>
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">24</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">25</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">26</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">27</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">28</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">29</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">30</td>
                                </tr>
                                <tr>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">1</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">2</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">3</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">4</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">5</td>
                                    <td class="tgl_costum_calendar" onclick="select_day(this)">6</td>
                                    <td class="tgl_costum_calendar ming" onclick="select_day(this)">7</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- CALENDAR KEGIATAN -->
            <br>
            <br>
            <!-- CONTENT KEGIATAN -->
            <div class="col-sm-7 list_kegiatan noselect">
                <h5 class="bold">Daftar Kegiatan</h5>
                    <h6 class="thin" style="font-weight: bold">5 Total Jadwal</h6>
                    <div class="container-fluid">
                        <div class="row bg-white">
                            <div class="col-sm-12 scroll-box" style="height: 373px">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3 date_kegiatan text-center">
                                            <h6>08:00 WITA</h6>
                                            <h6>18/04/2018</h6>
                                        </div>

                                        <div class="col-md-8 content_kegiatan">
                                            <a  onclick="select_journal(this)">
                                                <p class="desc thin">Menerima Kunjungan Lapangan dan Sharing Diskusi dari
                                                    Balai Uji Coba Sistem Pendidikan
                                                    dan Pelatihan Perumahan dan Pemukiman</p>
                                            </a>
                                            <p class="location_label">
                                                <span class="fa fa-map-marker" aria-hidden="true"></span>Kawasan Kota Lama
                                                Semarang</p>

                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-3 date_kegiatan text-center">
                                            <h6>08:00 WITA</h6>
                                            <h6>18/04/2018</h6>
                                        </div>

                                        <div class="col-md-8 content_kegiatan">
                                            <a onclick="select_journal(this)">
                                                <p class="desc thin">Menerima Kunjungan Lapangan dan Sharing Diskusi dari
                                                    Balai Uji Coba Sistem Pendidikan
                                                    dan Pelatihan Perumahan dan Pemukiman</p>
                                            </a>
                                            <p class="location_label">
                                                <span class="fa fa-map-marker" aria-hidden="true"></span>Kawasan Kota Lama
                                                Semarang</p>

                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-3 date_kegiatan text-center">
                                            <h6>08:00 WITA</h6>
                                            <h6>18/04/2018</h6>
                                        </div>

                                        <div class="col-md-8 content_kegiatan">
                                            <a  onclick="select_journal(this)">
                                                <p class="desc thin">Menerima Kunjungan Lapangan dan Sharing Diskusi dari
                                                    Balai Uji Coba Sistem Pendidikan
                                                    dan Pelatihan Perumahan dan Pemukiman</p>
                                            </a>
                                            <p class="location_label">
                                                <span class="fa fa-map-marker" aria-hidden="true"></span>Kawasan Kota Lama
                                                Semarang</p>

                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-3 date_kegiatan text-center">
                                            <h6>08:00 WITA</h6>
                                            <h6>18/04/2018</h6>
                                        </div>

                                        <div class="col-md-8 content_kegiatan">
                                            <a  onclick="select_journal(this)">
                                                <p class="desc thin">Menerima Kunjungan Lapangan dan Sharing Diskusi dari
                                                    Balai Uji Coba Sistem Pendidikan
                                                    dan Pelatihan Perumahan dan Pemukiman</p>
                                            </a>
                                            <p class="location_label">
                                                <span class="fa fa-map-marker" aria-hidden="true"></span>Kawasan Kota Lama
                                                Semarang</p>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="container load_more">
                                            <button type="button" class="col-md-12 btn btn-outline-primary">Load More
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
            <!-- CONTENT kegiatan -->

        </div>
    </div>
@endsection

@section('script-body')
    <script src="{{ $asset }}js/calendar_custom.js?v2"></script>
@endsection
