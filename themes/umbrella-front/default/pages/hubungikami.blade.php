@extends('layouts.app')


@section('title_bar')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <br>
    <h3 class="bold" style="letter-spacing: 1.2px">HUBUNGI KAMI</h3>
@endsection
@section('body')
    {{--{{var_dump($setting_company)}}--}}
    <div class="container ">
        <div class="row content-singgel_page about">
            <div class="col-sm-10 offset-sm-1">
                <div class="row">
                    <div class="offset-sm-1 col-sm-5 ">
                        <ul class="list-icon-info bold">
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>{{$setting_company['company_telephone']}}</span>
                            </li>
                            <li>
                                <i class="fa fa-fax" aria-hidden="true"></i>
                                </i>
                                <span>{{$setting_company['company_faksimili']}}</span>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>{{$setting_company['company_email']}}</span>
                            </li>
                            <li>
                                <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                <span>{{$setting_company['company_mobile_phone']}}</span>
                            </li>
                            <li>
                                <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                </i>
                                <span>{{$setting_company['company_whatssApp']}}</span>
                            </li>

                        </ul>
                    </div>
                    <div class="col-sm-5 form-pesan">
                        <p>kritik dan saran Anda sangat membantu dalam pengembangan aplikasi ini kirim kritik dan saran anda
                            pada form di bawah :</p>
                        <form action="{{base_url('hubungikami/kritik_dan_saran')}}" method="POST">
                            <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama">
                            <input type="email" class="form-control" id="email" placeholder="emailanda@gmail.com" name="email">
                            <textarea name="pesan" id="pesan" class="form-control" placeholder="Tuliskan pesan disini"></textarea>
                           <div class="form-group" style="margin-top: -15px !important; margin-bottom: 10px !important;">{!! $captcha !!}</div>
                            <button type="reset"  class="col-sm-5 mx-2 btn btn-outline-primary" onclick="grecaptcha.reset();">RESET</button>
                            <button type="submit" class="col-sm-5 mx-2 btn btn-primary">KIRIM</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-body')

    <script type="application/javascript" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection