let bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
var days_label = ["Ming", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"];
var days_template = ["Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Ming"];
var template = [];
var date_now = new Date();
var disable_day = [];
var selectday = '';
$(document).ready(function () {
    $("#month-select").attr('data-month', date_now.getMonth());
    $('#year-select').text(date_now.getFullYear());
    select_month('now');


});

function getDaysInMonth(month, year) {
    var date = new Date(year, month, 1);
    var days = [];
    while (date.getMonth() === month) {

        days.push(days_label[new Date(date).getDay()]);
        date.setDate(date.getDate() + 1);
    }
    return days;
}

function select_month(action) {

    var month = $("#month-select");
    var month_value = $("#month-select").attr("data-month");
    var year_value = $("#year-select").text();
    var list_day = [];
    var template_day = [];
    if (action == 'next') {

        month_value = parseInt(month_value) + 1;
        if (month_value == bulan.length) {
            $("#month-select").attr('data-month', 0);
            $("#month-select").text(bulan[0]);
            year_value = parseInt(year_value) + 1;
            $("#year-select").text(year_value);
            month_value = 0;
        } else {
            month.attr('data-month', bulan.indexOf(bulan[month_value]));
            month.text(bulan[month_value]);
        }

    } else if (action == 'previous') {

        month_value = parseInt(month_value) - 1;
        if (month_value == -1) {
            $("#month-select").attr('data-month', 11);
            $("#month-select").text(bulan[11]);
            year_value = parseInt(year_value) - 1;
            $("#year-select").text(year_value);
            month_value = 11;
        } else {
            month.attr('data-month', bulan.indexOf(bulan[month_value]));
            month.text(bulan[month_value]);
        }

    } else if (action == 'now') {
        month.attr('data-month', bulan.indexOf(bulan[month_value]));
        month.text(bulan[month_value]);
    }

    list_day[0] = getDaysInMonth(parseInt(month_value), parseInt(year_value));

    if (month_value == 0) {
        list_day[1] = getDaysInMonth(parseInt(11), parseInt(year_value) - 1);
    } else {
        list_day[1] = getDaysInMonth(parseInt(month_value) - 1, parseInt(year_value));
    }

    if (month_value == 11) {
        list_day[2] = getDaysInMonth(parseInt(0), parseInt(year_value) + 1);
    } else {
        list_day[2] = getDaysInMonth(parseInt(month_value) + 1, parseInt(year_value));
    }

    index_awal = days_template.indexOf(list_day[0][0]);
    var i = 0;
    var j = 1;

    for (i = index_awal; i < list_day[0].length + index_awal; i++) {
        template_day[i] = j;

        j++;
    }


    j = 1;
    disable_day[1] = i;
    for (i = i; i < 42; i++) {
        template_day[i] = j;
        j++;
    }

    j = list_day[1].length;
    disable_day[0] = index_awal;
    for (i = index_awal - 1; i >= 0; i--) {
        template_day[i] = j;
        j--;
    }

    var tr_index = 0;
    i = 0;
    $.each(template_day, function (index, val) {
        if (i == 7) {
            i = 0;
            tr_index++;
        }
        if (index < disable_day[0] || index >= disable_day[1]) {
            $($($('#content_costum_calendar').find('tr')[tr_index]).find('td')[i]).addClass('disable_day');

        } else {
            $($($('#content_costum_calendar').find('tr')[tr_index]).find('td')[i]).removeClass('disable_day');
        }
        $($($('#content_costum_calendar').find('tr')[tr_index]).find('td')[i]).text(val);
        i++;
    });

}

function select_day(el_day) {
    selectday=$('#content_costum_calendar').find('.select_date');
    selectday.removeClass('select_date');
    selectday = $(el_day);
    selectday.addClass('select_date');
}
