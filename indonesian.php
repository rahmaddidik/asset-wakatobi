<?php
/**
 * WHMCS Language File
 * Indonesian (id)
 *
 * Please Note: These language files are overwritten during software updates
 * and therefore editing of these files directly is not advised. Instead we
 * recommend that you use overrides to customise the text displayed in a way
 * which will be safely preserved through the upgrade process.
 *
 * For instructions on overrides, please visit:
 *   http://docs.whmcs.com/Language_Overrides
 *
 * @package    WHMCS
 * @author     WHMCS Limited <development@whmcs.com>
 * @copyright  Copyright (c) WHMCS Limited 2005-2015
 * @license    http://www.whmcs.com/license/ WHMCS Eula
 * @version    $Id$
 * @link       http://www.whmcs.com/
 */

//if (!defined("WHMCS")) die("This file cannot be accessed directly");

$_LANG['locale'] = "id_ID";

$_LANG['accountinfo'] = "Informasi Akun";
$_LANG['accountstats'] = "Account Statistics";
$_LANG['addfunds'] = "Add Funds";
$_LANG['addfundsamount'] = "Amount to Add";
$_LANG['addfundsmaximum'] = "Maximum Deposit";
$_LANG['addfundsmaximumbalance'] = "Maximum Balance";
$_LANG['addfundsmaximumbalanceerror'] = "Maximum Balance amount is";
$_LANG['addfundsmaximumerror'] = "Maximum Deposit amount is";
$_LANG['addfundsminimum'] = "Minimum Deposit";
$_LANG['addfundsminimumerror'] = "Minimum Deposit amount is";
$_LANG['addmore'] = "Add More";
$_LANG['addtocart'] = "Add to Cart";
$_LANG['affiliatesactivate'] = "Activate Affiliate Account";
$_LANG['affiliatesamount'] = "Amount";
$_LANG['affiliatesbalance'] = "Current Balance";
$_LANG['affiliatesbullet1'] = "Receive an initial bonus deposit in your affiliate account of";
$_LANG['affiliatesbullet2'] = "of every payment each customer you refer to us makes for the entire duration of their hosting account";
$_LANG['affiliatescommission'] = "Commission";
$_LANG['affiliatesdescription'] = "Join our affiliate program or view earnings";
$_LANG['affiliatesdisabled'] = "We do not currently offer an affiliate system to our clients.";
$_LANG['affiliatesearn'] = "Earn";
$_LANG['affiliatesearningstodate'] = "Total Earnings to Date";
$_LANG['affiliatesfootertext'] = "When you refer someone to our website with your unique referral ID, a cookie is placed on their computer containing this ID so if they bookmark the site and come back later you will still receive the commission.";
$_LANG['affiliateshostingpackage'] = "Hosting Package";
$_LANG['affiliatesintrotext'] = "Activate your affiliate account today to:";
$_LANG['affiliateslinktous'] = "Link to Us";
$_LANG['affiliatesnosignups'] = "You have currently not received any signups";
$_LANG['affiliatesrealtime'] = "These statistics are in real time and update instantly";
$_LANG['affiliatesreferallink'] = "Your Unique Referral Link";
$_LANG['affiliatesreferals'] = "Your referrals";
$_LANG['affiliatesregdate'] = "Registration Date";
$_LANG['affiliatesrequestwithdrawal'] = "Request Withdrawal";
$_LANG['affiliatessignupdate'] = "Signup Date";
$_LANG['affiliatesstatus'] = "Status";
$_LANG['affiliatestitle'] = "Affiliates";
$_LANG['affiliatesvisitorsreferred'] = "Number of Visitors Referred";
$_LANG['affiliateswithdrawalrequestsuccessful'] = "Your request for a withdrawal has been submitted. You will be contacted shortly.";
$_LANG['affiliateswithdrawn'] = "Total Amount Withdrawn";
$_LANG['all'] = "All";
$_LANG['alreadyregistered'] = "Sudah Registrasi?";
$_LANG['announcementsdescription'] = "View our latest news & announcements";
$_LANG['announcementsnone'] = "No Announcements to Display";
$_LANG['announcementsrss'] = "View RSS Feed";
$_LANG['announcementstitle'] = "Pengumuman";
$_LANG['bannedbanexpires'] = "Ban Expires";
$_LANG['bannedbanreason'] = "Ban Reason";
$_LANG['bannedhasbeenbanned'] = "has been banned";
$_LANG['bannedtitle'] = "IP Banned";
$_LANG['bannedyourip'] = "Your IP";
$_LANG['cartaddons'] = "Addons";
$_LANG['cartbrowse'] = "Browse Products &amp; Services";
$_LANG['cartconfigdomainextras'] = "Configure Domain Extras";
$_LANG['cartconfigoptionsdesc'] = "This product/service has some options which you can choose from below to customise your order.";
$_LANG['cartconfigserver'] = "Configure Server";
$_LANG['cartcustomfieldsdesc'] = "This product/service requires some additional information from you to allow us to process your order.";
$_LANG['cartdomainsconfig'] = "Domains Configuration";
$_LANG['cartdomainsconfigdesc'] = "Below you can configure the domain names in your shopping cart selecting the addon services you would like, providing required information for them and defining the nameservers that they will use.";
$_LANG['cartdomainshashosting'] = "Has Hosting";
$_LANG['cartdomainsnohosting'] = "No Hosting! Click to Add";
$_LANG['carteditproductconfig'] = "Edit Configuration";
$_LANG['cartempty'] = "Keranjang Belanja Kosong";
$_LANG['cartemptyconfirm'] = "Are you sure you want to empty your shopping cart?";
$_LANG['cartexistingclientlogin'] = "Existing Client Login";
$_LANG['cartexistingclientlogindesc'] = "To add this order to your existing account, you will need to login below.";
$_LANG['cartnameserversdesc'] = "If you want to use custom nameservers then enter them below. By default, new domains will use our nameservers for hosting on our network.";
$_LANG['cartproductaddons'] = "Product Addons";
$_LANG['cartproductaddonschoosepackage'] = "Choose Package";
$_LANG['cartproductaddonsnone'] = "No Addons Available for your Products &amp; Services";
$_LANG['cartproductconfig'] = "Product Configuration";
$_LANG['cartproductdesc'] = "The product/service you have chosen has the following configuration options for you to choose from.";
$_LANG['cartproductdomain'] = "Domains";
$_LANG['cartproductdomainchoose'] = "Choose Domain";
$_LANG['cartproductdomaindesc'] = "The product/service you have chosen requires a domain name. Please enter your domain name below.";
$_LANG['cartproductdomainuseincart'] = "Use a domain already in my shopping cart";
$_LANG['cartremove'] = "Remove";
$_LANG['cartremoveitemconfirm'] = "Are you sure you want to remove this item from your cart?";
$_LANG['carttaxupdateselections'] = "Tax may be charged depending upon the state and country selections you make. Click to recalculate after making your choices.";
$_LANG['carttaxupdateselectionsupdate'] = "Update";
$_LANG['carttitle'] = "Shopping Cart";
$_LANG['changessavedsuccessfully'] = "Changes Saved Successfully!";
$_LANG['checkavailability'] = "Check Availability";
$_LANG['checkout'] = "Checkout";
$_LANG['choosecurrency'] = "Choose Currency";
$_LANG['choosedomains'] = "Choose Domains";
$_LANG['clickheretologin'] = "Click here to login";
$_LANG['clientareaaccountaddons'] = "Account Addons";
$_LANG['clientareaactive'] = "Active";
$_LANG['clientareaaddfundsdisabled'] = "We do not allow depositing funds in advance with us at the current time.";
$_LANG['clientareaaddfundsnotallowed'] = "You must have at least one active order before you can add funds so you cannot proceed at the current time!";
$_LANG['clientareaaddon'] = "Addon";
$_LANG['clientareaaddonorderconfirmation'] = "Thank You. Your order for the addon shown below has been placed. Please choose your method of payment from below.";
$_LANG['clientareaaddonpricing'] = "Pricing";
$_LANG['clientareaaddonsfor'] = "Addons for";
$_LANG['clientareaaddress1'] = "Address 1";
$_LANG['clientareaaddress2'] = "Address 2";
$_LANG['clientareabwlimit'] = "Bandwidth Limit";
$_LANG['clientareabwusage'] = "Bandwidth usage";
$_LANG['clientareacancel'] = "Cancel Changes";
$_LANG['clientareacancelconfirmation'] = "Thank You. Your cancellation request has been submitted. If you have done this in error, open a support ticket to notify us immediately or your account may be terminated.";
$_LANG['clientareacancelinvalid'] = "This account has already had a cancellation request submitted so you cannot submit another.";
$_LANG['clientareacancellationendofbillingperiod'] = "End of Billing Period";
$_LANG['clientareacancellationimmediate'] = "Immediate";
$_LANG['clientareacancellationtype'] = "Cancellation Type";
$_LANG['clientareacancelled'] = "Cancelled";
$_LANG['clientareacancelproduct'] = "Requesting Cancellation for";
$_LANG['clientareacancelreason'] = "Briefly Describe your reason for Cancellation";
$_LANG['clientareacancelrequest'] = "Account Cancellation Request";
$_LANG['clientareacancelrequestbutton'] = "Request Cancellation";
$_LANG['clientareachangepassword'] = "Change Your Password";
$_LANG['clientareachangesuccessful'] = "Your Details Were Changed Successfully";
$_LANG['clientareachoosecontact'] = "Choose Contact";
$_LANG['clientareacity'] = "City";
$_LANG['clientareacompanyname'] = "Company Name";
$_LANG['clientareaconfirmpassword'] = "Confirm Password";
$_LANG['clientareacontactsemails'] = "Email Preferences";
$_LANG['clientareacontactsemailsdomain'] = "Domain Emails - Renewal Notices, Registration Confirmations, etc...";
$_LANG['clientareacontactsemailsgeneral'] = "General Emails - General Announcements &amp; Password Reminders";
$_LANG['clientareacontactsemailsinvoice'] = "Invoice Emails - Invoices &amp; Billing Reminders";
$_LANG['clientareacontactsemailsproduct'] = "Product Emails - Order Details, Welcome Emails, etc...";
$_LANG['clientareacontactsemailssupport'] = "Support Emails - Allow this user to open tickets in your account";
$_LANG['clientareacountry'] = "Country";
$_LANG['clientareacurrentsecurityanswer'] = "Please enter your current answer";
$_LANG['clientareacurrentsecurityquestion'] = "Please choose your current security question";
$_LANG['clientareadeletecontact'] = "Delete Contact";
$_LANG['clientareadeletecontactareyousure'] = "Are you sure you want to delete this contact?";
$_LANG['clientareadescription'] = "View & update your account details";
$_LANG['clientareadisklimit'] = "Disk Space Limit";
$_LANG['clientareadiskusage'] = "Disk Space Usage";
$_LANG['clientareadomainexpirydate'] = "Expiry Date";
$_LANG['clientareadomainnone'] = "No Domains Registered With Us";
$_LANG['clientareaemail'] = "Email Address";
$_LANG['clientareaemails'] = "My Emails";
$_LANG['clientareaemailsdate'] = "Date Sent";
$_LANG['clientareaemailsintrotext'] = "Below is a history of all messages we have sent to you. It allows you to easily read any correspondance relating to your account in case you lose any of your emails.";
$_LANG['clientareaemailssubject'] = "Message Subject";
$_LANG['clientareaerroraddress1'] = "You did not enter your address (line 1)";
$_LANG['clientareaerroraddress12'] = "Your address can only contain letters, numbers and spaces";
$_LANG['clientareaerrorbannedemail'] = "We don't allow users with the email address provider you entered. Please try another email address.";
$_LANG['clientareaerrorcity'] = "You did not enter your city";
$_LANG['clientareaerrorcity2'] = "Your city can only contain letters and spaces";
$_LANG['clientareaerrorcountry'] = "Please choose your country from the drop down box";
$_LANG['clientareaerroremail'] = "You did not enter your email address";
$_LANG['clientareaerroremailinvalid'] = "The email address you entered was not valid";
$_LANG['clientareaerrorfirstname'] = "You did not enter your first name";
$_LANG['clientareaerrorfirstname2'] = "Your first name can only contain letters";
$_LANG['clientareaerrorisrequired'] = "is required";
$_LANG['clientareaerrorlastname'] = "You did not enter your last name";
$_LANG['clientareaerrorlastname2'] = "Your last name can only contain letters";
$_LANG['clientareaerroroccured'] = "An error occurred, please try again later.";
$_LANG['clientareaerrorpasswordconfirm'] = "You did not confirm your password";
$_LANG['clientareaerrorpasswordnotmatch'] = "The passwords you entered did not match";
$_LANG['clientareaerrorphonenumber'] = "You did not enter your phone number";
$_LANG['clientareaerrorphonenumber2'] = "Your phone number is not valid";
$_LANG['clientareaerrorpostcode'] = "You did not enter your postcode";
$_LANG['clientareaerrorpostcode2'] = "Your postcode can only contain letters, numbers and spaces";
$_LANG['clientareaerrors'] = "The following errors occurred:";
$_LANG['clientareaerrorstate'] = "You did not enter your state";
$_LANG['clientareaexpired'] = "Expired";
$_LANG['clientareafirstname'] = "Nama Depan";
$_LANG['clientareafraud'] = "Fraud";
$_LANG['clientareafullname'] = "Client Name";
$_LANG['clientareaheader'] = "Welcome to our client area where you can manage your account with us. This page provides a brief overview of your account including any open support requests and unpaid invoices.  Please ensure you keep your contact details up to date.";
$_LANG['clientareahostingaddons'] = "Addons";
$_LANG['clientareahostingaddonsintro'] = "You have the following addons for this product.";
$_LANG['clientareahostingaddonsview'] = "View";
$_LANG['clientareahostingamount'] = "Amount";
$_LANG['clientareahostingdomain'] = "Domain";
$_LANG['clientareahostingnextduedate'] = "Next Due Date";
$_LANG['clientareahostingpackage'] = "Package";
$_LANG['clientareahostingregdate'] = "Tanggal Registrasi";
$_LANG['clientarealastname'] = "Last Name";
$_LANG['clientarealastupdated'] = "Last Updated";
$_LANG['clientarealeaveblank'] = "Leave blank unless you want to change your password.";
$_LANG['clientareamodifydomaincontactinfo'] = "Modify Domain Contact Information";
$_LANG['clientareamodifynameservers'] = "Ubah Nameservers";
$_LANG['clientareamodifywhoisinfo'] = "Modify WHOIS Contact Information";
$_LANG['clientareanameserver'] = "Nameserver";
$_LANG['clientareanavaddcontact'] = "Add New Contact";
$_LANG['clientareanavchangecc'] = "Change Credit Card Details";
$_LANG['clientareanavchangepw'] = "Ubah Password";
$_LANG['clientareanavdetails'] = "My Details";
$_LANG['clientareanavdomains'] = "My Domains";
$_LANG['clientareanavhome'] = "Home";
$_LANG['clientareanavlogout'] = "Logout";
$_LANG['clientareanavorder'] = "Order Additional Items";
$_LANG['clientareanavsecurityquestions'] = "Change Security Question";
$_LANG['clientareanavservices'] = "My Services";
$_LANG['clientareanavsupporttickets'] = "My Support Tickets";
$_LANG['clientareanocontacts'] = "No Contacts Found";
$_LANG['clientareapassword'] = "Password";
$_LANG['clientareapending'] = "Pending";
$_LANG['clientareapendingtransfer'] = "Pending Transfer";
$_LANG['clientareaphonenumber'] = "Phone Number";
$_LANG['clientareapostcode'] = "Kodepos";
$_LANG['clientareaproductdetails'] = "Product Details";
$_LANG['clientareaproducts'] = "My Products &amp; Services";
$_LANG['clientareaproductsnone'] = "No Products/Services Ordered";
$_LANG['clientarearegistrationperiod'] = "Registration Period";
$_LANG['clientareasavechanges'] = "Save Changes";
$_LANG['clientareasecurityanswer'] = "Please enter an answer";
$_LANG['clientareasecurityconfanswer'] = "Please confirm your answer";
$_LANG['clientareasecurityquestion'] = "Please choose a security question";
$_LANG['clientareaselectcountry'] = "Select Country";
$_LANG['clientareasetlocking'] = "Set Locking";
$_LANG['clientareastate'] = "State/Region";
$_LANG['clientareastatus'] = "Status";
$_LANG['clientareasuspended'] = "Suspended";
$_LANG['clientareaterminated'] = "Terminated";
$_LANG['clientareaticktoenable'] = "Tick to enable";
$_LANG['clientareatitle'] = "Client Area";
$_LANG['clientareaunlimited'] = "Unlimited";
$_LANG['clientareaupdatebutton'] = "Update";
$_LANG['clientareaupdateyourdetails'] = "Update Your Details";
$_LANG['clientareaused'] = "Used";
$_LANG['clientareaviewaddons'] = "View Available Addons";
$_LANG['clientareaviewdetails'] = "View Details";
$_LANG['clientlogin'] = "Client Login";
$_LANG['clientregisterheadertext'] = "Please fill in the fields below to register for a new account.";
$_LANG['clientregistertitle'] = "Register";
$_LANG['clientregisterverify'] = "Verify Registration";
$_LANG['clientregisterverifydescription'] = "Please enter the text you see in the image below into the text box provided. This is required to prevent automated registrations.";
$_LANG['clientregisterverifyinvalid'] = "Incorrect Verification Image Code Entered";
$_LANG['closewindow'] = "Close Window";
$_LANG['completeorder'] = "Complete Order";
$_LANG['confirmnewpassword'] = "Confirm New Password";
$_LANG['contactemail'] = "Email";
$_LANG['contacterrormessage'] = "You did not enter a message";
$_LANG['contacterrorname'] = "You did not enter your name";
$_LANG['contacterrorsubject'] = "You did not enter a subject";
$_LANG['contactheader'] = "If you have any pre-sales questions or want to contact us, please use the form below.";
$_LANG['contactmessage'] = "Message";
$_LANG['contactname'] = "Name";
$_LANG['contactsend'] = "Send Message";
$_LANG['contactsent'] = "Your Message has been Sent";
$_LANG['contactsubject'] = "Subject";
$_LANG['contacttitle'] = "Pre-Sales Contact Us";
$_LANG['continueshopping'] = "Continue Shopping";
$_LANG['creditcard'] = "Pay by Credit Card";
$_LANG['creditcard3dsecure'] = "As part of our fraud prevention measures, you will now be asked to perform the Verified by Visa or Mastercard SecureCode verification to complete your payment.<br /><br />Do not click the refresh or back button or this transaction may be interrupted or cancelled.";
$_LANG['creditcardcardexpires'] = "Expiry Date";
$_LANG['creditcardcardissuenum'] = "Issue Number";
$_LANG['creditcardcardnumber'] = "Card Number";
$_LANG['creditcardcardstart'] = "Start Date";
$_LANG['creditcardcardtype'] = "Card Type";
$_LANG['creditcardccvinvalid'] = "The cards CVV number is required";
$_LANG['creditcardconfirmation'] = "Thank You! Your new card details have been accepted and the first payment for your account has been taken. You have been sent a confirmation email about this.";
$_LANG['creditcardcvvnumber'] = "CVV/CVC2 Number";
$_LANG['creditcardcvvwhere'] = "Where do I find this?";
$_LANG['creditcarddeclined'] = "The credit card details you entered were declined. Please try a different card or contact support.";
$_LANG['creditcarddetails'] = "Credit Card Details";
$_LANG['creditcardenterexpirydate'] = "You did not enter the card expiry date";
$_LANG['creditcardenternewcard'] = "Enter New Card Information Below";
$_LANG['creditcardenternumber'] = "You did not enter your card number";
$_LANG['creditcardinvalid'] = "The credit card details you entered were invalid. Please try a different card or contact support.";
$_LANG['creditcardnumberinvalid'] = "The credit card number you entered is invalid";
$_LANG['creditcardsecuritynotice'] = "Any data you enter here is submitted securely and is encrypted to reduce the risk of fraud";
$_LANG['creditcarduseexisting'] = "Use Existing Card";
$_LANG['customfieldvalidationerror'] = "value is not valid";
$_LANG['days'] = "Days";
$_LANG['hours'] = "Hours";
$_LANG['minutes'] = "Minutes";
$_LANG['seconds'] = "Seconds";
$_LANG['defaultbillingcontact'] = "Default Billing Contact";
$_LANG['domainalternatives'] = "Try these alternatives:";
$_LANG['domainavailable'] = "Available! Order Now";
$_LANG['domainavailable1'] = "Congratulations!";
$_LANG['domainavailable2'] = "is available!";
$_LANG['domainavailableexplanation'] = "To register this domain click on the link below";
$_LANG['domainbulksearch'] = "Bulk Domain Search";
$_LANG['domainbulksearchintro'] = "The bulk real-time domain name search allows you to search up to 20 domains at once.  Enter the domains in the field below, one per line - do not enter www. or http:// in front.";
$_LANG['domainbulktransferdescription'] = "You can transfer your existing domains to us today.  To get started, simply enter the domains below, one per line - do not include the www. or http://";
$_LANG['domainbulktransfersearch'] = "Bulk Domain Transfer";
$_LANG['domaincontactinfo'] = "Contact Information";
$_LANG['domaincurrentrenewaldate'] = "Current Renewal Date";
$_LANG['domaindnsaddress'] = "Address";
$_LANG['domaindnshostname'] = "Host Name";
$_LANG['domaindnsmanagement'] = "DNS Management";
$_LANG['domaindnsmanagementdesc'] = "Point your domain to a web site by pointing to an IP Address, or forward to another site, or point to a temporary page (known as Parking), and more. These records are also known as sub-domains.";
$_LANG['domaindnsrecordtype'] = "Record Type";
$_LANG['domainemailforwarding'] = "Email Forwarding";
$_LANG['domainemailforwardingdesc'] = "If the Email Forwarding Server determines the Forward To address is invalid, we will disable the forwarding record automatically. Please check the Forward To address before you enable it again. The changes on any existing forwarding record may not take effect for up to 1 hour.";
$_LANG['domainemailforwardingforwardto'] = "Forward To";
$_LANG['domainemailforwardingprefix'] = "Prefix";
$_LANG['domaineppcode'] = "EPP Code";
$_LANG['domaineppcodedesc'] = "This needs to be obtained from the current registrar for authorisation";
$_LANG['domaineppcoderequired'] = "You must enter the EPP code for";
$_LANG['domainerror'] = "There was a problem connecting to the domain registry. Please try again later.";
$_LANG['domainerrornodomain'] = "Please enter a Valid Domain Name";
$_LANG['domainerrortoolong'] = "The domain you entered is too long. Domains can only be up to 67 characters in length.";
$_LANG['domaingeteppcode'] = "Get EPP Code";
$_LANG['domaingeteppcodeemailconfirmation'] = "The EPP Code request was successful! It has been sent to the registrant email address for your domain.";
$_LANG['domaingeteppcodeexplanation'] = "The EPP Code is basically a password for a domain name. It is a security measure, ensuring that only the domain name owner can transfer a domain name. You will need it if you are wanting to transfer the domain to another registrar.";
$_LANG['domaingeteppcodefailure'] = "There was an error in requesting the EPP Code:";
$_LANG['domaingeteppcodeis'] = "The EPP Code for your domain is:";
$_LANG['domainidprotection'] = "ID Protection";
$_LANG['domainintrotext'] = "Enter the domain and tld you wish to use in the boxes below and click Lookup to see whether the domain is available for purchase.";
$_LANG['domainlookupbutton'] = "Lookup";
$_LANG['domainmanagementtools'] = "Management Tools";
$_LANG['domainminyears'] = "Min. Years";
$_LANG['domainmoreinfo'] = "More Info";
$_LANG['domainname'] = "Domain Name";
$_LANG['domainnameserver1'] = "Nameserver 1";
$_LANG['domainnameserver2'] = "Nameserver 2";
$_LANG['domainnameserver3'] = "Nameserver 3";
$_LANG['domainnameserver4'] = "Nameserver 4";
$_LANG['domainnameserver5'] = "Nameserver 5";
$_LANG['domainnameservers'] = "Nameservers";
$_LANG['domainordernow'] = "Order Now!";
$_LANG['domainorderrenew'] = "Order Renewal";
$_LANG['domainprice'] = "Price";
$_LANG['domainregisterns'] = "Register Nameservers";
$_LANG['domainregisternscurrentip'] = "Current IP Address";
$_LANG['domainregisternsdel'] = "Delete a NameServer";
$_LANG['domainregisternsdelsuccess'] = "The nameserver  was successfully deleted";
$_LANG['domainregisternsexplanation'] = "From here you can create and manage custom nameservers for your domain (eg. NS1.example.com, NS2.example.com...).";
$_LANG['domainregisternsip'] = "IP Address";
$_LANG['domainregisternsmod'] = "Modify a NameServer IP";
$_LANG['domainregisternsmodsuccess'] = "The nameserver  was successfully modified";
$_LANG['domainregisternsnewip'] = "New IP Address";
$_LANG['domainregisternsns'] = "Nameserver";
$_LANG['domainregisternsreg'] = "Register a NameServer Name";
$_LANG['domainregisternsregsuccess'] = "The nameserver  was successfully registered";
$_LANG['domainregistrantchoose'] = "Select the contact you want to use here";
$_LANG['domainregistrantinfo'] = "Domain Registrant Information";
$_LANG['domainregistrarlock'] = "Registrar Lock";
$_LANG['domainregistrarlockdesc'] = "Enable registrar lock (Recommended). Unauthorized transfer will be prevented if lock is set.";
$_LANG['domainregistration'] = "Domain Registration";
$_LANG['domainregistryinfo'] = "Domain Registry Information";
$_LANG['domainregnotavailable'] = "N/A";
$_LANG['domainrenew'] = "Renew Domain";
$_LANG['domainrenewal'] = "Domain Renewal";
$_LANG['domainrenewalprice'] = "Renewal";
$_LANG['domainrenewdesc'] = "Secure your domain(s) by adding more years to them. Choose how many years you want to renew for and then submit to continue.";
$_LANG['domainsautorenew'] = "Auto Renew";
$_LANG['domainsautorenewdisable'] = "Disable Auto Renew";
$_LANG['domainsautorenewdisabled'] = "Disabled";
$_LANG['domainsautorenewdisabledwarning'] = "WARNING! This domain has auto renewal disabled.<br />It will therefore expire and become inactive at the end of the current term unless manually renewed.";
$_LANG['domainsautorenewenable'] = "Enable Auto Renew";
$_LANG['domainsautorenewenabled'] = "Enabled";
$_LANG['domainsautorenewstatus'] = "Current Status";
$_LANG['domainsimplesearch'] = "Simple Domain Search";
$_LANG['domainspricing'] = "Domain Pricing";
$_LANG['domainsregister'] = "Register";
$_LANG['domainsrenew'] = "Renew";
$_LANG['domainsrenewnow'] = "Renew Now";
$_LANG['domainstatus'] = "Status";
$_LANG['domainstransfer'] = "Transfer";
$_LANG['domaintitle'] = "Domain Checker";
$_LANG['domaintld'] = "TLD";
$_LANG['domaintransfer'] = "Domain Transfer";
$_LANG['domainunavailable'] = "Unavailable";
$_LANG['domainunavailable1'] = "Sorry!";
$_LANG['domainunavailable2'] = "is already taken!";
$_LANG['domainreserved'] = "Reserved";
$_LANG['domainreserved1'] = "Domain";
$_LANG['domainreserved2'] = "is available, but reserved.";
$_LANG['domainviewwhois'] = "view whois report";
$_LANG['downloaddescription'] = "Description";
$_LANG['downloadloginrequired'] = "Access Denied - You must be logged in to download this file";
$_LANG['downloadname'] = "Download";
$_LANG['downloadpurchaserequired'] = "Access Denied - You must purchase the associated product before you can download this";
$_LANG['downloadscategories'] = "Kategori";
$_LANG['downloadsdescription'] = "View our library of downloads";
$_LANG['downloadsfiles'] = "Files";
$_LANG['downloadsfilesize'] = "Filesize";
$_LANG['downloadsintrotext'] = "The download library has all the manuals, programs and other files that you may need to get your website up and running.";
$_LANG['downloadspopular'] = "Most Popular Downloads";
$_LANG['downloadsnone'] = "No Downloads to Display";
$_LANG['downloadstitle'] = "Downloads";
$_LANG['email'] = "Email";
$_LANG['emptycart'] = "Empty Cart";
$_LANG['existingpassword'] = "Existing Password";
$_LANG['existingpasswordincorrect'] = "Your existing password was not correct";
$_LANG['firstpaymentamount'] = "First Payment Amount";
$_LANG['flashtutorials'] = "Flash Tutorials";
$_LANG['flashtutorialsdescription'] = "Click here to view tutorials showing you how to use your hosting control panel";
$_LANG['flashtutorialsheadertext'] = "Our Flash Tutorials are here to help you fully utilise your web hosting control panel. Choose a task from below to see a step by step tutorial on how to complete it.";
$_LANG['forwardingtogateway'] = "Please wait while you are redirected to the gateway you chose to make payment...";
$_LANG['globalsystemname'] = "Portal Home";
$_LANG['globalyouarehere'] = "You are here";
$_LANG['go'] = "Go";
$_LANG['headertext'] = "Welcome to our Support Portal.";
$_LANG['hometitle'] = "Home";
$_LANG['imagecheck'] = "Please enter the security code shown in the image - this is required to prevent automated submissions";
$_LANG['invoiceaddcreditamount'] = "Enter the amount to apply";
$_LANG['invoiceaddcreditapply'] = "Apply Credit";
$_LANG['invoiceaddcreditdesc1'] = "Your credit balance is";
$_LANG['invoiceaddcreditdesc2'] = "This can be applied to the invoice using the form below.";
$_LANG['invoiceaddcreditoverbalance'] = "You cannot apply more credit than the balance due";
$_LANG['invoiceaddcreditovercredit'] = "You cannot apply more credit than you have in your account";
$_LANG['invoicenumber'] = "Invoice #";
$_LANG['invoiceofflinepaid'] = "Offline Credit Card Payments are processed manually.<br />You will receive confirmation by email once your payment has been processed.";
$_LANG['invoicerefnum'] = "Reference Number";
$_LANG['invoices'] = "My Invoices";
$_LANG['invoicesamount'] = "Amount";
$_LANG['invoicesattn'] = "ATTN";
$_LANG['invoicesbacktoclientarea'] = "&laquo; Back to Client Area";
$_LANG['invoicesbalance'] = "Balance";
$_LANG['invoicesbefore'] = "before";
$_LANG['invoicescancelled'] = "Cancelled";
$_LANG['invoicescollections'] = "Collections";
$_LANG['invoicescredit'] = "Credit";
$_LANG['invoicesdatecreated'] = "Invoice Date";
$_LANG['invoicesdatedue'] = "Due Date";
$_LANG['invoicesdescription'] = "Description";
$_LANG['invoicesdownload'] = "Download";
$_LANG['invoiceserror'] = "An Error Occurred. Please Try Again.";
$_LANG['invoicesinvoicedto'] = "Invoiced To";
$_LANG['invoicesinvoicenotes'] = "Invoice Notes";
$_LANG['invoicesnoinvoices'] = "No Invoices";
$_LANG['invoicesnotes'] = "Notes";
$_LANG['invoicesoutstandinginvoices'] = "Outstanding Invoices";
$_LANG['invoicespaid'] = "Paid";
$_LANG['invoicespaynow'] = "Pay Now";
$_LANG['invoicespayto'] = "Pay To";
$_LANG['invoicesrefunded'] = "Refunded";
$_LANG['invoicesstatus'] = "Status";
$_LANG['invoicessubtotal'] = "Sub Total";
$_LANG['invoicestax'] = "Tax Due";
$_LANG['invoicestaxindicator'] = "Indicates a taxed item.";
$_LANG['invoicestitle'] = "Invoice #";
$_LANG['invoicestotal'] = "Total";
$_LANG['invoicestransactions'] = "Transactions";
$_LANG['invoicestransamount'] = "Amount";
$_LANG['invoicestransdate'] = "Transaction Date";
$_LANG['invoicestransgateway'] = "Gateway";
$_LANG['invoicestransid'] = "Transaction ID";
$_LANG['invoicestransnonefound'] = "No Related Transactions Found";
$_LANG['invoicesunpaid'] = "Unpaid";
$_LANG['invoicesdraft'] = "Draft";
$_LANG['invoicesview'] = "View Invoice";
$_LANG['jobtitle'] = "Job Title";
$_LANG['kbsuggestions'] = "Knowledgebase Suggestions";
$_LANG['kbsuggestionsexplanation'] = "The following articles were found in the knowledgebase which may answer your question. Please review the suggestions before submission.";
$_LANG['knowledgebasearticles'] = "Artikel";
$_LANG['knowledgebasecategories'] = "Kategori";
$_LANG['nokbcategories'] = "No categories exist";
$_LANG['knowledgebasedescription'] = "Browse our KB for answers to FAQs";
$_LANG['knowledgebasefavorites'] = "Add to Favourites";
$_LANG['knowledgebasehelpful'] = "Was this answer helpful?";
$_LANG['knowledgebaseintrotext'] = "The knowledgebase is organized into different categories. Either choose a category from below or search the knowledgebase for the answer to your question.";
$_LANG['knowledgebasemore'] = "More";
$_LANG['knowledgebaseno'] = "No";
$_LANG['knowledgebasenoarticles'] = "No Articles Found";
$_LANG['knowledgebasenorelated'] = "There are no Related Articles";
$_LANG['knowledgebasepopular'] = "Most Popular Articles";
$_LANG['knowledgebaseprint'] = "Print this Article";
$_LANG['knowledgebaserating'] = "Rating:";
$_LANG['knowledgebaseratingtext'] = "Users Found This Useful";
$_LANG['knowledgebaserelated'] = "Related Articles";
$_LANG['knowledgebasesearch'] = "Pencarian";
$_LANG['knowledgebasetitle'] = "Knowledgebase";
$_LANG['knowledgebaseviews'] = "Views";
$_LANG['knowledgebasevote'] = "Vote";
$_LANG['knowledgebasevotes'] = "Votes";
$_LANG['knowledgebaseyes'] = "Yes";
$_LANG['knowledgebaseArticleRatingThanks'] = "Thanks for rating the article for us";
$_LANG['language'] = "Language";
$_LANG['latefee'] = "Late Fee";
$_LANG['latefeeadded'] = "Added";
$_LANG['latestannouncements'] = "Pengumuman Terbaru";
$_LANG['loginbutton'] = "Login";
$_LANG['loginemail'] = "Email Address";
$_LANG['loginforgotten'] = "Forgotten Your Password?";
$_LANG['loginforgotteninstructions'] = "Request a Password Reset";
$_LANG['loginincorrect'] = "Login Details Incorrect. Please try again.";
$_LANG['loginintrotext'] = "You must login to access this page. These login details differ from your websites control panel username and password.";
$_LANG['loginpassword'] = "Password";
$_LANG['loginrememberme'] = "Remember Me";
$_LANG['logoutcontinuetext'] = "Click here to continue...";
$_LANG['logoutsuccessful'] = "You have been successfully logged out.";
$_LANG['logouttitle'] = "Logout";
$_LANG['maxmind_anonproxy'] = "We do not allow orders to be placed using an Anonymous Proxy";
$_LANG['maxmind_callingnow'] = "We are placing an automated call to your phone number now. This is part of our fraud checking measures. You will be given a 4 digit security code which you need to enter below to complete your order.";
$_LANG['maxmind_countrymismatch'] = "The country of your IP address did not match the billing address country you entered so we cannot accept your order";
$_LANG['maxmind_error'] = "Error";
$_LANG['maxmind_faileddescription'] = "The code you entered was incorrect. If you feel this to be an error, please contact our support department as soon as possible.";
$_LANG['maxmind_highfraudriskscore'] = "Your order has been flagged as potentially high risk and therefore it has been held for manual review.<br /><br />If you feel you have received this message in error, then please accept our apologies and <a href=\"submitticket.php\">submit a support ticket</a> to our Customer Service Team.  Thank you.";
$_LANG['maxmind_highriskcountry'] = "Unfortunately, we are unable to accept your order as there has been a lot of fraudulent activity from your country. If you want to arrange an alternative means of payment, please contact us.";
$_LANG['maxmind_incorrectcode'] = "Incorrect Code";
$_LANG['maxmind_pincode'] = "Pin Code";
$_LANG['maxmind_rejectemail'] = "We do not permit orders using a free email address, please try again using a different email address";
$_LANG['maxmind_title'] = "MaxMind";
$_LANG['more'] = "More";
$_LANG['morechoices'] = "More Choices";
$_LANG['networkissuesaffecting'] = "Affecting";
$_LANG['networkissuesaffectingyourservers'] = "Please Note: Issues affecting servers you have accounts on will be highlighted with a gold background";
$_LANG['networkissuesdate'] = "Date";
$_LANG['networkissuesdescription'] = "Read about current and scheduled network outages";
$_LANG['networkissueslastupdated'] = "Last Updated";
$_LANG['networkissuesnonefound'] = "No network issues found";
$_LANG['networkissuespriority'] = "Priority";
$_LANG['networkissuesprioritycritical'] = "Critical";
$_LANG['networkissuespriorityhigh'] = "High";
$_LANG['networkissuesprioritylow'] = "Low";
$_LANG['networkissuesprioritymedium'] = "Medium";
$_LANG['networkissuesstatusinprogress'] = "In Progress";
$_LANG['networkissuesstatusinvestigating'] = "Investigating";
$_LANG['networkissuesstatusopen'] = "Open";
$_LANG['networkissuesstatusoutage'] = "Outage";
$_LANG['networkissuesstatusreported'] = "Reported";
$_LANG['networkissuesstatusresolved'] = "Resolved";
$_LANG['networkissuesstatusscheduled'] = "Scheduled";
$_LANG['networkissuestitle'] = "Network Issues";
$_LANG['networkissuestypeother'] = "Other";
$_LANG['networkissuestypeserver'] = "Server";
$_LANG['networkissuestypesystem'] = "System";
$_LANG['newpassword'] = "New Password";
$_LANG['nextpage'] = "Next Page";
$_LANG['no'] = "No";
$_LANG['nocarddetails'] = "No existing card details on record";
$_LANG['none'] = "None";
$_LANG['norecordsfound'] = "No Records Found";
$_LANG['or'] = "or";
$_LANG['orderadditionalrequiredinfo'] = "Additional Required Information";
$_LANG['orderaddon'] = "Addon";
$_LANG['orderaddondescription'] = "The following addons are available for this product. Choose the addons you wish to order below.";
$_LANG['orderavailable'] = "Available";
$_LANG['orderavailableaddons'] = "Click to view available addons";
$_LANG['orderbillingcycle'] = "Billing Cycle";
$_LANG['ordercategories'] = "Categories";
$_LANG['orderchangeaddons'] = "Change Addons";
$_LANG['orderchangeconfig'] = "Change Configurable Options";
$_LANG['orderchangedomain'] = "Change Domain";
$_LANG['orderchangenameservers'] = "Change Nameservers Only";
$_LANG['orderchangeproduct'] = "Change Product";
$_LANG['ordercheckout'] = "Checkout";
$_LANG['orderchooseaddons'] = "Choose Product Addons";
$_LANG['orderchooseapackage'] = "Choose a Package";
$_LANG['ordercodenotfound'] = "The promotion code entered does not exist";
$_LANG['ordercompletebutnotpaid'] = "Attention! Your order has been completed but you have not yet paid for it so it will not be activated.<br />Click on the link below to go to your invoice to make payment.";
$_LANG['orderconfigpackage'] = "Configurable Options";
$_LANG['orderconfigure'] = "Configure";
$_LANG['orderconfirmation'] = "Order Confirmation";
$_LANG['orderconfirmorder'] = "Confirm Order";
$_LANG['ordercontinuebutton'] = "Click to Continue >>";
$_LANG['orderdesc'] = "Description";
$_LANG['orderdescription'] = "Place a new order with us";
$_LANG['orderdiscount'] = "Discount";
$_LANG['orderdomain'] = "Domain";
$_LANG['orderdomainoption1part1'] = "I want";
$_LANG['orderdomainoption1part2'] = "to register a new domain for me.";
$_LANG['orderdomainoption2'] = "I will update my nameservers on an existing domain Or I will register a new domain.";
$_LANG['orderdomainoption3'] = "I want to transfer my domain to";
$_LANG['orderdomainoption4'] = "I want to use a free subdomain.";
$_LANG['orderdomainoptions'] = "Domain Options";
$_LANG['orderdomainregistration'] = "Domain Registration";
$_LANG['orderdomainregonly'] = "Domain Registration";
$_LANG['orderdomaintransfer'] = "Domain Transfer";
$_LANG['orderdontusepromo'] = "Don't use Promotional Code";
$_LANG['ordererroraccepttos'] = "You must accept our Terms of Service";
$_LANG['ordererrordomainalreadyexists'] = "The domain you entered is already registered with us - you will need to cancel it prior to placing a new order";
$_LANG['ordererrordomaininvalid'] = "The domain you entered is not valid";
$_LANG['ordererrordomainnotld'] = "You must enter the domain TLD";
$_LANG['ordererrordomainnotregistered'] = "You cannot transfer a domain that isn't registered";
$_LANG['ordererrordomainregistered'] = "The domain you entered is already registered";
$_LANG['ordererrornameserver1'] = "You must enter nameserver 1";
$_LANG['ordererrornameserver2'] = "You must enter nameserver 2";
$_LANG['ordererrornodomain'] = "You did not enter a domain name";
$_LANG['ordererrorpassword'] = "You did not enter a password";
$_LANG['ordererrorserverhostnameinuse'] = "The hostname you entered is already in use. Please choose another.";
$_LANG['ordererrorservernohostname'] = "You must enter a hostname for your server";
$_LANG['ordererrorservernonameservers'] = "You must enter a prefix for both nameservers";
$_LANG['ordererrorservernorootpw'] = "You must enter your desired root password";
$_LANG['ordererrorsubdomaintaken'] = "The subdomain you entered is already taken - please try again";
$_LANG['ordererrortransfersecret'] = "You must enter the transfer secret";
$_LANG['ordererroruserexists'] = "A user already exists with that email address";
$_LANG['orderexistinguser'] = "I am an existing customer and wish to add this order to my account";
$_LANG['orderfailed'] = "Order Failed";
$_LANG['orderfinalinstructions'] = "If you have any questions about your order, please open a support ticket from your client area and quote your order number.";
$_LANG['orderfree'] = "FREE!";
$_LANG['orderfreedomainappliesto'] = "applies to the following extensions only";
$_LANG['orderfreedomaindescription'] = "on selected payment terms";
$_LANG['orderfreedomainonly'] = "Free Domain";
$_LANG['orderfreedomainregistration'] = "Free Domain Registration";
$_LANG['ordergotoclientarea'] = "Click here to go to your Client Area";
$_LANG['orderinvalidcodeforbillingcycle'] = "This code does not apply to the selected billing cycle";
$_LANG['orderlogininfo'] = "Login Information";
$_LANG['orderlogininfopart1'] = "Please enter the password that you wish to use to login to your";
$_LANG['orderlogininfopart2'] = "Client Area. This will differ from your website control panel username &amp; password.";
$_LANG['ordernewuser'] = "I am a new customer and would like to create an account";
$_LANG['ordernoproducts'] = "No Products Found";
$_LANG['ordernotes'] = "Notes / Additional Information";
$_LANG['ordernotesdescription'] = "You can enter any additional notes or information you want included with your order here...";
$_LANG['ordernowbutton'] = "Order Now";
$_LANG['ordernumberis'] = "Your Order Number is:";
$_LANG['orderpaymentmethod'] = "Payment Method";
$_LANG['orderpaymentterm12month'] = "12 Month Price";
$_LANG['orderpaymentterm1month'] = "1 Month Price";
$_LANG['orderpaymentterm24month'] = "24 Month Price";
$_LANG['orderpaymentterm3month'] = "3 Month Price";
$_LANG['orderpaymentterm6month'] = "6 Month Price";
$_LANG['orderpaymenttermannually'] = "Annually";
$_LANG['orderpaymenttermbiennially'] = "Biennially";
$_LANG['orderpaymenttermfreeaccount'] = "Free Account";
$_LANG['orderpaymenttermmonthly'] = "Monthly";
$_LANG['orderpaymenttermonetime'] = "One Time";
$_LANG['orderpaymenttermquarterly'] = "Quarterly";
$_LANG['orderpaymenttermsemiannually'] = "Semi-Annually";
$_LANG['orderprice'] = "Price";
$_LANG['orderproduct'] = "Product/Service";
$_LANG['orderprogress'] = "Progress";
$_LANG['orderpromoexpired'] = "The promotion code entered has expired";
$_LANG['orderpromoinvalid'] = "The promotion code entered does not apply to any items in your order";
$_LANG['orderpromomaxusesreached'] = "The promotion code entered has already been used";
$_LANG['orderpromotioncode'] = "Promotional Code";
$_LANG['orderpromovalidatebutton'] = "Validate Code";
$_LANG['orderPromoCodePlaceholder'] = "Enter promo code if you have one";
$_LANG['orderprorata'] = "Pro Rata";
$_LANG['orderreceived'] = "Thank you for your order. You will receive a confirmation email shortly.";
$_LANG['orderregisterdomain'] = "Registrasi domain baru";
$_LANG['orderregperiod'] = "Registration Period";
$_LANG['ordersecure'] = "This order form is provided in a secure environment and to help protect against fraud your current IP address";
$_LANG['ordersecure2'] = "is being logged.";
$_LANG['orderserverhostname'] = "Server Hostname";
$_LANG['orderservernameservers'] = "Nameservers";
$_LANG['orderservernameserversdescription'] = "The prefixes you enter here will determine the default nameservers for the server eg. ns1.example.com and ns2.example.com";
$_LANG['orderservernameserversprefix1'] = "Prefix 1";
$_LANG['orderservernameserversprefix2'] = "Prefix 2";
$_LANG['orderserverrootpassword'] = "Root Password";
$_LANG['ordersetupfee'] = "Setup Fee";
$_LANG['orderstartover'] = "Start Over";
$_LANG['ordersubdomaininuse'] = "The subdomain you entered is already in use";
$_LANG['ordersubtotal'] = "Subtotal";
$_LANG['ordersummary'] = "Order Summary";
$_LANG['ordertaxcalculations'] = "Tax Calculations";
$_LANG['ordertaxstaterequired'] = "You must enter your state for tax calculations to take place";
$_LANG['ordertitle'] = "Order";
$_LANG['ordertos'] = "Terms of Service";
$_LANG['ordertosagreement'] = "I have read and agree to the";
$_LANG['ordertotalduetoday'] = "Total Due Today";
$_LANG['ordertotalrecurring'] = "Total Recurring";
$_LANG['ordertransferdomain'] = "Transfer an Existing Domain Name";
$_LANG['ordertransfersecret'] = "Transfer Secret";
$_LANG['ordertransfersecretexplanation'] = "Please enter the Domain Transfer Secret which can be obtained from the current Registrar of the Domain Name. ";
$_LANG['orderusesubdomain'] = "Use Subdomain";
$_LANG['orderyears'] = "Year/s";
$_LANG['orderyourinformation'] = "Your Information";
$_LANG['orderyourorder'] = "Your Order";
$_LANG['organizationname'] = "Organization Name";
$_LANG['outofstock'] = "Out of Stock";
$_LANG['outofstockdescription'] = "We are currently out of stock on this item so orders for it have been suspended until more stock is available. For further information, please contact us.";
$_LANG['page'] = "Page";
$_LANG['pageof'] = "of";
$_LANG['please'] = "Please";
$_LANG['pleasewait'] = "Please Wait...";
$_LANG['presalescontactdescription'] = "Place any pre-sales enquiries here";
$_LANG['previouspage'] = "Prev Page";
$_LANG['proformainvoicenumber'] = "Proforma Invoice #";
$_LANG['promoexistingclient'] = "You must have an active product/service to use this code";
$_LANG['promoonceperclient'] = "This code can only be used once per client";
$_LANG['pwstrengthfail'] = "The password you entered is not strong enough - please enter a more complex password";
$_LANG['pwdoesnotmatch'] = "The passwords entered do not match";
$_LANG['quicknav'] = "Quick Navigation";
$_LANG['recordsfound'] = "Records Found";
$_LANG['recurring'] = "Recurring";
$_LANG['recurringamount'] = "Recurring Amount";
$_LANG['every'] = "Every";
$_LANG['registerdomain'] = "Register Domain";
$_LANG['registerdomaindesc'] = "Type in the domain you wish to register below to check for availability.";
$_LANG['registerdomainname'] = "Register a Domain Name";
$_LANG['relatedservice'] = "Related Service";
$_LANG['rssfeed'] = "Feed";
$_LANG['securityanswerrequired'] = "You are required to enter a security answer";
$_LANG['securitybothnotmatch'] = "Your answer and confirm answer do not match";
$_LANG['securitycurrentincorrect'] = "Your current question and answer is incorrect";
$_LANG['serverchangepassword'] = "Change Password";
$_LANG['serverchangepasswordintro'] = "From here you can change the password of the product/service (note: this does not affect your password for our client area)";
$_LANG['serverchangepasswordconfirm'] = "Confirm Password";
$_LANG['serverchangepasswordenter'] = "Enter New Password";
$_LANG['serverchangepasswordfailed'] = "Password Change Failed!";
$_LANG['serverchangepasswordsuccessful'] = "Password Changed Successfully!";
$_LANG['serverchangepasswordupdate'] = "Update";
$_LANG['serverhostname'] = "Hostname";
$_LANG['serverlogindetails'] = "Login Details";
$_LANG['servername'] = "Server";
$_LANG['serverns1prefix'] = "NS1 Prefix";
$_LANG['serverns2prefix'] = "NS2 Prefix";
$_LANG['serverpassword'] = "Password";
$_LANG['serverrootpw'] = "Root Password";
$_LANG['serverstatusdescription'] = "View live status info for our servers";
$_LANG['serverstatusnoservers'] = "No Servers are currently being monitored";
$_LANG['serverstatusnotavailable'] = "Not Available";
$_LANG['serverstatusoffline'] = "Offline";
$_LANG['serverstatusonline'] = "Online";
$_LANG['serverstatusphpinfo'] = "PHP Info";
$_LANG['serverstatusserverload'] = "Server Load";
$_LANG['serverstatustitle'] = "Server Status";
$_LANG['serverstatusuptime'] = "Uptime";
$_LANG['serverusername'] = "Username";
$_LANG['show'] = "Show";
$_LANG['ssladmininfo'] = "Administrative Contact Information";
$_LANG['ssladmininfodetails'] = "The contact information below will not be displayed on the Certificate - it is used only for contacting you regarding this order. The SSL Certificate and future renewal reminders will be sent to the email address specified below.";
$_LANG['sslcertapproveremail'] = "Certificate Approver Email";
$_LANG['sslcertapproveremaildetails'] = "You must now choose from the options below where you would like the approval email request for this certificate to be sent.";
$_LANG['sslcertinfo'] = "SSL Certificate Information";
$_LANG['pleasechooseone'] = "Please choose one...";
$_LANG['sslcerttype'] = "Certificate Type";
$_LANG['sslconfigcomplete'] = "Configuration Complete";
$_LANG['sslconfigcompletedetails'] = "Your SSL certificate configuration has now been completed and sent to the Certificate Authority for validation. You should receive an email from them shortly to approve it.";
$_LANG['sslconfsslcertificate'] = "Configure SSL Certificate";
$_LANG['sslcsr'] = "CSR";
$_LANG['sslerrorapproveremail'] = "You must choose an approver email address";
$_LANG['sslerrorentercsr'] = "You must enter your certificate signing request (CSR)";
$_LANG['sslerrorselectserver'] = "You must select your server type";
$_LANG['sslinvalidlink'] = "Invalid Link Followed. Please go back and try again.";
$_LANG['sslorderdate'] = "Order Date";
$_LANG['sslserverinfo'] = "Server Information";
$_LANG['sslserverinfodetails'] = "You must have a valid \"CSR\" (Certificate Signing Request) to configure your SSL Certificate. The CSR is an encrypted piece of text that is generated by the web server where the SSL Certificate will be installed. If you do not already have a CSR, you must generate one or ask your web hosting provider to generate one for you. Also please ensure you enter the correct information as it cannot be changed after the SSL Certificate has been issued.";
$_LANG['sslservertype'] = "Web Server Type";
$_LANG['sslstatus'] = "Configuration Status";
$_LANG['statscreditbalance'] = "Account Credit Balance";
$_LANG['statsdueinvoicesbalance'] = "Due Invoices Balance";
$_LANG['statsnumdomains'] = "Number of Domains";
$_LANG['statsnumproducts'] = "Number of Products/Services";
$_LANG['statsnumreferredsignups'] = "Number of Referred Signups";
$_LANG['statsnumtickets'] = "Number of Support Tickets";
$_LANG['submitticketdescription'] = "Submit a trouble ticket";
$_LANG['supportclickheretocontact'] = "click here to contact us";
$_LANG['supportpresalesquestions'] = "If you have pre-sales questions";
$_LANG['supportticketinvalid'] = "An error occurred. The requested ticket could not be found.";
$_LANG['supportticketsallowedextensions'] = "Allowed File Extensions";
$_LANG['supportticketschoosedepartment'] = "Choose Department";
$_LANG['supportticketsclient'] = "Client";
$_LANG['supportticketsclientemail'] = "Email Address";
$_LANG['supportticketsclientname'] = "Name";
$_LANG['supportticketsdate'] = "Date";
$_LANG['supportticketsdepartment'] = "Department";
$_LANG['supportticketsdescription'] = "View and respond to existing tickets";
$_LANG['supportticketserror'] = "Error";
$_LANG['supportticketserrornoemail'] = "You did not enter your email address";
$_LANG['supportticketserrornomessage'] = "You did not enter a message";
$_LANG['supportticketserrornoname'] = "You did not enter your name";
$_LANG['supportticketserrornosubject'] = "You did not enter a subject";
$_LANG['supportticketsfilenotallowed'] = "The file you tried to upload is not allowed.";
$_LANG['supportticketsheader'] = "If you can't find a solution to your problems in our knowledgebase, you can submit a ticket by selecting the appropriate department below.";
$_LANG['supportticketsnotfound'] = "Ticket Not Found";
$_LANG['supportticketsopentickets'] = "Open Support Tickets";
$_LANG['supportticketspagetitle'] = "Support Tickets";
$_LANG['supportticketsposted'] = "Posted";
$_LANG['supportticketsreply'] = "Reply";
$_LANG['supportticketsstaff'] = "Staff";
$_LANG['supportticketsstatus'] = "Status";
$_LANG['supportticketsstatusanswered'] = "Answered";
$_LANG['supportticketsstatusclosed'] = "Closed";
$_LANG['supportticketsstatuscloseticket'] = "If resolved, click here to close the ticket";
$_LANG['supportticketsstatuscustomerreply'] = "Customer-Reply";
$_LANG['supportticketsstatusinprogress'] = "In Progress";
$_LANG['supportticketsstatusonhold'] = "On Hold";
$_LANG['supportticketsstatusopen'] = "Open";
$_LANG['supportticketssubject'] = "Subject";
$_LANG['supportticketssubmitticket'] = "Submit Ticket";
$_LANG['supportticketssystemdescription'] = "The support ticket system allows us to respond to your problems and enquiries as quickly as possible. When we make a response to your support ticket, you will be notified via email.";
$_LANG['supportticketsticketattachments'] = "Attachments";
$_LANG['supportticketsticketcreated'] = "Ticket Created";
$_LANG['supportticketsticketcreateddesc'] = "Your ticket has been successfully created. An email has been sent to your address with the ticket information. If you would like to view this ticket now you can do so.";
$_LANG['supportticketsticketid'] = "Ticket ID";
$_LANG['supportticketsticketsubject'] = "Subject";
$_LANG['supportticketsticketsubmit'] = "Submit";
$_LANG['supportticketsticketurgency'] = "Urgency";
$_LANG['supportticketsticketurgencyhigh'] = "High";
$_LANG['supportticketsticketurgencylow'] = "Low";
$_LANG['supportticketsticketurgencymedium'] = "Medium";
$_LANG['supportticketsuploadfailed'] = "Could not upload attachment file";
$_LANG['supportticketsuploadtoolarge'] = "Uploaded file was too large. Please try uploading a smaller file.";
$_LANG['supportticketsviewticket'] = "View Ticket";
$_LANG['supportticketclosedmsg'] = "This ticket is closed.  You may reply to this ticket to reopen it.";
$_LANG['telesignincorrectpin'] = "Incorrect Pin!";
$_LANG['telesigninitiatephone'] = "We can't initiate phone verification for your number. Please contact us.";
$_LANG['telesigninvalidnumber'] = "Invalid phone number";
$_LANG['telesigninvalidpin'] = "The PIN you entered was invalid!";
$_LANG['telesigninvalidpin2'] = "The pin you entered was not valid.";
$_LANG['telesigninvalidpinmessage'] = "Pin Code Verification Failed";
$_LANG['telesignmessage'] = "Phone verification initiated for number %s . Please wait...";
$_LANG['telesignphonecall'] = "Phone call";
$_LANG['telesignpin'] = "Enter your PIN: ";
$_LANG['telesignsms'] = "Sms";
$_LANG['telesignsmstextmessage'] = "Thank you for using our SMS verification system. Your code is: %s Please enter this code on your computer now.!";
$_LANG['telesigntitle'] = "TeleSign phone verification.";
$_LANG['telesigntype'] = "Choose verification type for number %s:";
$_LANG['telesignverificationcanceled'] = "There is a temporary problem with the phone verification service and phone verification has been canceled.";
$_LANG['telesignverificationproblem'] = "There was a problem with the phone verification service and your order could not be validated. Please try again later.";
$_LANG['telesignverify'] = "Your phone number %s needs to be verified to complete the order.";
$_LANG['ticketratingexcellent'] = "Excellent";
$_LANG['ticketratingpoor'] = "Poor";
$_LANG['ticketratingquestion'] = "How would you rate this reply?";
$_LANG['ticketreatinggiven'] = "You rated this response";
$_LANG['transferdomain'] = "Transfer Domain";
$_LANG['transferdomaindesc'] = "Want to move your domain to us? If so, enter your domain below to begin.";
$_LANG['transferdomainname'] = "Transfer a Domain Name";
$_LANG['updatecart'] = "Update Cart";
$_LANG['upgradechooseconfigoptions'] = "Upgrade/Downgrade the configurable options on this product.";
$_LANG['upgradechoosepackage'] = "Choose the package you want to upgrade/downgrade your current package to from the options below.";
$_LANG['upgradecurrentconfig'] = "Current Configuration";
$_LANG['upgradedowngradeconfigoptions'] = "Upgrade/Downgrade Options";
$_LANG['upgradenewconfig'] = "New Configuration";
$_LANG['upgradenochange'] = "No Change";
$_LANG['upgradeproductlogic'] = "Upgrade price is calculated from a credit of the unused portion of the current plan and billing of the new plan for the same period";
$_LANG['upgradesummary'] = "Below is a summary of your upgrade order.";
$_LANG['usedefaultcontact'] = "Use Default Contact (Details Above)";
$_LANG['varilogixfraudcall_callnow'] = "Call Now!";
$_LANG['varilogixfraudcall_description'] = "As part of our fraud prevention measures, we will now call the phone number registered for your account and ask you to enter the above pin code. Please make a note of the pin code and when you are ready for us to place the phone call, please click on the button below.";
$_LANG['varilogixfraudcall_error'] = "An error occurred and we could not call your phone number to verify your order. Please contact our support department as soon as possible to complete your order.";
$_LANG['varilogixfraudcall_fail'] = "The call to verify your order failed. This could be because your phone number was incorrectly entered or is blacklisted on our system. Please contact our support department as soon as possible to complete your order.";
$_LANG['varilogixfraudcall_failed'] = "Failed";
$_LANG['varilogixfraudcall_pincode'] = "Pin Code";
$_LANG['varilogixfraudcall_title'] = "VariLogix FraudCall";
$_LANG['viewcart'] = "View Cart";
$_LANG['welcomeback'] = "Selamat Datang";
$_LANG['whoisresults'] = "WHOIS Results for";
$_LANG['yes'] = "Yes";
$_LANG['yourdetails'] = "Your Details";

# Version 4.1

$_LANG['clientareafiles'] = "Attached Files";
$_LANG['clientareafilesdate'] = "Date Added";
$_LANG['clientareafilesfilename'] = "Filename";

$_LANG['pwreset'] = "Reset Password";
$_LANG['pwresetdesc'] = "If you have forgotten your password, you can reset it here. When you fill in your registered email address (and answer your account security question if set), you will be sent instructions on how to reset your password.";
$_LANG['pwresetemailrequired'] = "Please enter your email address";
$_LANG['pwresetemailnotfound'] = "No client account was found with the email address you entered";
$_LANG['pwresetsecurityquestionrequired'] = "As you have a security question setup on your account, you must enter the answer to this question below.";
$_LANG['pwresetsecurityquestionincorrect'] = "The security question answer you entered does not match the answer set in your account";
$_LANG['pwresetsubmit'] = "Submit";
$_LANG['pwresetvalidationsent'] = "Validation Email Sent";
$_LANG['pwresetvalidationcheckemail'] = "The password reset process has now been started. Please check your email for instructions on what to do next.";
$_LANG['pwresetkeyinvalid'] = "The reset link you have followed is invalid. Please try again.";
$_LANG['pwresetkeyexpired'] = "The reset link you have followed has expired. Please try again.";
$_LANG['pwresetvalidationsuccess'] = "Password Reset Successful";

$_LANG['overagescharges'] = "Overage Charge";
$_LANG['overagestotaldiskusage'] = "Total Disk Usage";
$_LANG['overagestotalbwusage'] = "Total Bandwidth Usage";

$_LANG['affiliatescommissionspending'] = "Commissions Pending Maturation";
$_LANG['affiliatescommissionsavailable'] = "Available Commissions Balance";
$_LANG['affiliatessignups'] = "Number of Signups";
$_LANG['affiliatesconversionrate'] = "Conversion Rate";

$_LANG['configoptionqtyminmax'] = "%s has a minimum requirement of %s and maximum of %s";

$_LANG['creditcardnostore'] = "Tick this box if you do NOT want us to store your credit card details for recurring billing";
$_LANG['creditcarddelete'] = "Delete Saved Card Details";
$_LANG['creditcarddeleteconfirmation'] = "The stored credit card details have now been removed from your account";
$_LANG['creditcardupdatenotpossible'] = "Credit Card Details cannot be updated at the current time. Please try again later.";

$_LANG['invoicepaymentsuccessconfirmation'] = "Thank You! Your payment was successful.";
$_LANG['invoicepaymentfailedconfirmation'] = "Unfortunately your payment attempt was not successful.<br />Please try again or contact support.";

# Version 4.2

$_LANG['promoappliedbutnodiscount'] = "The promotion code you entered has been applied to your cart but no items qualify for the discount yet - please check the promotion terms";

$_LANG['upgradeerroroverdueinvoice'] = "You cannot currently upgrade or downgrade this product because an invoice has already been generated for the next renewal.<br /><br />To proceed, please first pay the outstanding invoice and then you will be able to upgrade or downgrade immediately following that and be charged the difference or credited as appropriate.";
$_LANG['upgradeexistingupgradeinvoice'] = "You cannot currently upgrade or downgrade this product because an upgrade or downgrade is already in progress.<br /><br />To proceed, please first pay the outstanding invoice and then you will be able to upgrade or downgrade immediately following that and be charged the difference or credited as appropriate.<br/><br/>If you believe you are receiving this message in error, please submit a trouble ticket.";

$_LANG['subaccountactivate'] = "Activate Sub-Account";
$_LANG['subaccountactivatedesc'] = "Tick to configure as a sub-account with client area access";
$_LANG['subaccountpermissions'] = "Sub-Account Permissions";
$_LANG['subaccountpermsprofile'] = "Modify Master Account Profile";
$_LANG['subaccountpermscontacts'] = "View & Manage Contacts";
$_LANG['subaccountpermsproducts'] = "View Products & Services";
$_LANG['subaccountpermsmanageproducts'] = "View & Modify Product Passwords";
$_LANG['subaccountpermsdomains'] = "View Domains";
$_LANG['subaccountpermsmanagedomains'] = "Manage Domain Settings";
$_LANG['subaccountpermsinvoices'] = "View & Pay Invoices";
$_LANG['subaccountpermstickets'] = "View & Open Support Tickets";
$_LANG['subaccountpermsaffiliates'] = "View & Manage Affiliate Account";
$_LANG['subaccountpermsemails'] = "View Emails";
$_LANG['subaccountpermsorders'] = "Place New Orders/Upgrades/Cancellations";
$_LANG['subaccountpermissiondenied'] = "You do not have the required permissions to access this page";
$_LANG['subaccountallowedperms'] = "Your allowed permissions are:";
$_LANG['subaccountcontactmaster'] = "Contact the master account owner if you feel this to be an error.";
$_LANG['subaccountSsoDenied'] = "You do not have permission to login using Single Sign-On.";

$_LANG['knowledgebasealsoread'] = "Also Read";

$_LANG['orderpaymenttermtriennially'] = "Triennially";
$_LANG['orderpaymentterm36month'] = "36 Month Price";

$_LANG['domainrenewals'] = "Domain Renewals";
$_LANG['domaindaysuntilexpiry'] = "Days Until Expiry";
$_LANG['domainrenewalsnoneavailable'] = "There are no domains eligible for renewal in your account";
$_LANG['domainrenewalspastgraceperiod'] = "Past Renewable Period";
$_LANG['domainrenewalsingraceperiod'] = "Last Chance to Renew!";
$_LANG['domainrenewalsdays'] = "Days";
$_LANG['domainrenewalsdaysago'] = "Days Ago";

$_LANG['invoicespartialpayments'] = "Partial Payments";
$_LANG['invoicestotaldue'] = "Total Due";

$_LANG['masspaytitle'] = "Mass Payment";
$_LANG['masspaydescription'] = "Below is a summary of the selected invoices and the total due to pay all of them. To submit payment please just choose your desired payment method below and then submit.";
$_LANG['masspayselected'] = "Pay Selected";
$_LANG['masspayall'] = "Pay All";
$_LANG['masspaymakepayment'] = "Make Payment";

# Version 4.3

$_LANG['searchenterdomain'] = "Enter Domain to Find";
$_LANG['searchfilter'] = "Filter";

$_LANG['suspendreason'] = "Suspension Reason";
$_LANG['suspendreasonoverdue'] = "Overdue on Payment";

$_LANG['vpsnetmanagement'] = "VPS Management";
$_LANG['vpsnetpowermanagement'] = "Power Management";
$_LANG['poweron'] = "Power On";
$_LANG['poweroffforced'] = "Power Off (Forced)";
$_LANG['powerreboot'] = "Reboot";
$_LANG['powershutdown'] = "Shutdown";
$_LANG['vpsnetcpugraphs'] = "CPU Graphs";
$_LANG['vpsnetnetworkgraphs'] = "Network Graphs";
$_LANG['vpsnethourly'] = "Hourly";
$_LANG['vpsnetdaily'] = "Daily";
$_LANG['vpsnetweekly'] = "Weekly";
$_LANG['vpsnetmonthly'] = "Monthly";
$_LANG['view'] = "View";
$_LANG['vpsnetbackups'] = "Backup Options";
$_LANG['vpsnetgenbackup'] = "Generate Backup";
$_LANG['vpsnetrestorebackup'] = "Restore Backup";
$_LANG['vpsnetrestorebackupwarning'] = "Restoring the backup will over write your VPS server";
$_LANG['vpsnetnobackups'] = "There are no backups";
$_LANG['vpsnetrunning'] = "Running";
$_LANG['vpsnetnotrunning'] = "Not Running";
$_LANG['vpsnetpowercycling'] = "Power is cycling";
$_LANG['vpsnetcloud'] = "Cloud";
$_LANG['vpsnettemplate'] = "Template";
$_LANG['vpsnetstatus'] = "System Status";
$_LANG['vpsnetbwusage'] = "Bandwidth Usage";

$_LANG['twitterlatesttweets'] = "Our Latest Tweets";
$_LANG['twitterfollow'] = "Follow Us on Twitter";
$_LANG['twitterfollowus'] = "Follow us";
$_LANG['twitterfollowuswhy'] = "to stay up to date with our latest news &amp; offers";

$_LANG['chatlivehelp'] = "Live Help";

$_LANG['domainrelease'] = "Release Domain";
$_LANG['domainreleasedescription'] = "Enter a new TAG here to move your domain name to another registrar";
$_LANG['domainreleasetag'] = "New Registrar Tag";

# Ajax Order Form

$_LANG['orderformtitle'] = "Order Form";

$_LANG['signup'] = "Signup";
$_LANG['loading'] = "Loading...";

$_LANG['ordersummarybegin'] = "Please choose a product to begin";

$_LANG['cartchooseproduct'] = "Choose Product";
$_LANG['cartconfigurationoptions'] = "Configuration Options";

$_LANG['ordererrorsoccurred'] = "The following errors occurred and must be corrected before checkout:";
$_LANG['ordererrortermsofservice'] = "The Terms of Service must be agreed to";
$_LANG['ordertostickconfirm'] = "Please tick to confirm you agree to the";

$_LANG['cartnewcustomer'] = "I'm a New Customer";
$_LANG['cartexistingcustomer'] = "I'm an Existing Customer";

$_LANG['cartpromo'] = "Promotion";
$_LANG['cartenterpromo'] = "Enter Promotion Code";
$_LANG['cartremovepromo'] = "Remove Promo";

$_LANG['cartrecurringcharges'] = "Recurring Charges";

$_LANG['cartenterdomain'] = "Please enter the domain you would like to use below.";

$_LANG['cartdomainavailableoptions'] = "Congratulations, this domain is available!";
$_LANG['cartdomainavailableregister'] = "Please register this domain for";
$_LANG['cartdomainavailablemanual'] = "I will register it myself seperately";

$_LANG['cartdomainunavailableoptions'] = "Sorry, this domain is already taken. If you are the owner, please choose an option below...";
$_LANG['cartdomainunavailabletransfer'] = "Please transfer my domain for";
$_LANG['cartdomainunavailablemanual'] = "I already own this domain and will update the nameservers";

$_LANG['cartdomaininvalid'] = "The domain you entered is not valid. Enter only the part after the www. and include the TLD";

# Version 4.4

$_LANG['dlinvalidlink'] = "Invalid Link Followed. Please Contact Support";

$_LANG['domaindnsmanagementlaunch'] = "Launch DNS Manager";
$_LANG['domainemailforwardinglaunch'] = "Launch Mail Forwarding Manager";

# Version 4.5

$_LANG['domaindnspriority'] = "Priority";
$_LANG['domaindnsmxonly'] = "Priority Record for MX Only";

$_LANG['orderpromoprestart'] = "This promotion has not yet started. Please try again later.";

$_LANG['ticketmerge'] = "MERGED";

$_LANG['quote'] = "Quote";
$_LANG['quotestitle'] = "My Quotes";
$_LANG['quoteview'] = "View";
$_LANG['quotedownload'] = "Download";
$_LANG['quoteacceptbtn'] = "Accept Quote";
$_LANG['quotedlpdfbtn'] = "Download PDF";
$_LANG['quotediscountheading'] = "Discount (%)";
$_LANG['noquotes'] = "There are currently no quotes saved under your account.<br />To request a quote, please open a ticket.";
$_LANG['quotenumber'] = "Quote #";
$_LANG['quotesubject'] = "Subject";
$_LANG['quotedatecreated'] = "Date Created";
$_LANG['quotevaliduntil'] = "Valid Until";
$_LANG['quotestage'] = "Stage";
$_LANG['quoterecipient'] = "Recipient";
$_LANG['quoteqty'] = "Qty";
$_LANG['quotedesc'] = "Description";
$_LANG['quoteunitprice'] = "Unit Price";
$_LANG['quotediscount'] = "Discount %";
$_LANG['quotelinetotal'] = "Total";
$_LANG['quotestagedraft'] = "Draft";
$_LANG['quotestagedelivered'] = "Delivered";
$_LANG['quotestageonhold'] = "On Hold";
$_LANG['quotestageaccepted'] = "Accepted";
$_LANG['quotestagelost'] = "Expired";
$_LANG['quotestagedead'] = "Expired";
$_LANG['quoteref'] = "Re Quote #";
$_LANG['quotedeposit'] = "Deposit";
$_LANG['quotefinalpayment'] = "Balance from Deposit";

$_LANG['invoiceoneoffpayment'] = "Make One Off Payment";
$_LANG['invoicesubscriptionpayment'] = "Create Automated Recurring Subscription";

$_LANG['invoicepaymentpendingreview'] = "Thank You! Your payment was successful and will be applied to your invoice as soon as 2CheckOut's Review Process has completed.<br /><br />This can take up to a few hours so your patience is appreciated.";

$_LANG['step'] = "Step %s";
$_LANG['cartdomainexists'] = "This domain already exists in our database so cannot be ordered again";
$_LANG['cartcongratsdomainavailable'] = "Congratulations, %s is available!";
$_LANG['cartregisterhowlong'] = "How long do you want to register this for?";
$_LANG['cartdomaintaken'] = "Sorry, %s is already taken";
$_LANG['carttransfernotregistered'] = "%s does not appear to be registered yet";
$_LANG['carttransferpossible'] = "Congratulations, we can transfer %s to us for just %s";
$_LANG['cartotherdomainsuggestions'] = "Other domains you might be interested in...";
$_LANG['cartdomainsconfiginfo'] = "The following options and settings are available for the domains you have chosen. Required fields are indicated with a *.";
$_LANG['cartnameserverchoice'] = "Nameserver Choice";
$_LANG['cartnameserverchoicedefault'] = "Use default nameservers for our hosting";
$_LANG['cartnameserverchoicecustom'] = "Use custom nameservers";
$_LANG['cartfollowingaddonsavailable'] = "The following addons are available for your active products & services.";
$_LANG['cartregisterdomainchoice'] = "Registrasi domain baru";
$_LANG['carttransferdomainchoice'] = "Transfer your domain from another registrar";
$_LANG['cartexistingdomainchoice'] = "I will use my existing domain and update my nameservers";
$_LANG['cartsubdomainchoice'] = "Use a subdomain from %s";
$_LANG['carterrordomainconfigskipped'] = "You must go back and complete the required domain configuration fields above";
$_LANG['cartproductchooseoptions'] = "Choose Options";
$_LANG['cartproductselection'] = "Product Selection";
$_LANG['cartreviewcheckout'] = "Review & Checkout";
$_LANG['cartchoosecycle'] = "Choose Billing Cycle";
$_LANG['cartavailableaddons'] = "Available Addons";
$_LANG['cartsetupfees'] = "Setup Fees";
$_LANG['cartchooseanotherproduct'] = "Choose Another Product";
$_LANG['cartaddandcheckout'] = "Add to Cart & Checkout";
$_LANG['cartchooseanothercategory'] = "Choose Another Category";
$_LANG['carttryanotherdomain'] = "Try another domain";
$_LANG['cartmakedomainselection'] = "Please provide us with the domain you want to use with your hosting service by selecting an option from the selections below.";
$_LANG['cartfraudcheck'] = "Fraud Check";

$_LANG['newcustomer'] = "New Customer";
$_LANG['existingcustomer'] = "Existing Customer";
$_LANG['newcustomersignup'] = "<strong>Not Yet Registered?</strong> %sClick here to signup...%s";

$_LANG['upgradeonselectedoptions'] = "(On Selected Options)";
$_LANG['recurringpromodesc'] = "This promotion code also includes a %s Recurring Discount<br />(This discount will apply to future renewals of the product's total price)";

# Version 4.5.2

$_LANG['ajaxcartcheckout'] = "Jump straight to checkout &raquo;";
$_LANG['ordersummarybegin'] = "Shopping Cart is Empty<br/>Please choose a product and domain name option to begin...";
$_LANG['ajaxcartconfigreqnotice'] = "You're on the way to signing up with us, but you must choose a domain before you can add the selected product to your cart...";

# Version 5.0.0

$_LANG['cancelrequestdomain'] = "Cancel Domain Renewal?";
$_LANG['cancelrequestdomaindesc'] = "You also have an active domain registration for the domain associated with this product<br />This domain is due to renew on %s at a cost of %s for %s Year/s<br /><br />If you would like to cancel the domain as well, and let it expire at the end of the current registration, then simply tick the box below.";
$_LANG['cancelrequestdomainconfirm'] = "I confirm I do not want to renew this domain again";

$_LANG['startingfrom'] = "Starting from";

$_LANG['orderpromopriceoverride'] = "Price Override";
$_LANG['orderpromofreesetup'] = "Free Setup";

$_LANG['thereisaproblem'] = "Oops, there's a problem...";
$_LANG['problemgoback'] = "Go back & try again";

$_LANG['quantity'] = "Quantity";
$_LANG['cartqtyenterquantity'] = "Want more than 1 of this item? Enter Quantity Here:";
$_LANG['cartqtyupdate'] = "Update";
$_LANG['invoiceqtyeach'] = "/each";

$_LANG['nschoicedefault'] = "Use default nameservers";
$_LANG['nschoicecustom'] = "Use custom nameservers (enter below)";

$_LANG['jumpto'] = "Jump to";
$_LANG['top'] = "Top";

$_LANG['domaincontactusexisting'] = "Use existing account contact";
$_LANG['domaincontactusecustom'] = "Specify custom information below";
$_LANG['domaincontactchoose'] = "Choose Contact";
$_LANG['domaincontactprimary'] = "Primary Profile Data";

$_LANG['invoicepdfgenerated'] = "PDF Generated on";

$_LANG['domainrenewalsbeforerenewlimit'] = "Minimum Advance Renewal is %s Days";

$_LANG['promonewsignupsonly'] = "This promotion code is only valid for new customers";

# Bulk Domain Management

$_LANG['domainbulkmanagement'] = "Bulk Management Actions";
$_LANG['domainbulkmanagementchangesaffect'] = "The changes made below will affect the following domains:";
$_LANG['domainbulkmanagementchangeaffect'] = "This change will apply to the following domains:";
$_LANG['domaincannotbemanaged'] = "cannot be managed automatically - please contact support regarding any changes you want to make";
$_LANG['domainbulkmanagementnotpossible'] = "Unfortunately these settings cannot be edited from our client area at the current time. Please contact support regarding any changes you wanted to make.";

$_LANG['domainmanagens'] = "Manage Nameservers";

$_LANG['domainautorenewstatus'] = "Auto Renewal Status";
$_LANG['domainautorenewinfo'] = "Auto renew helps protect your domain. When enabled, we will automatically send you a renewal invoice a few weeks before your domain expires, and  renew the domain should payment be successful.";
$_LANG['domainautorenewrecommend'] = "We recommend keeping auto renew enabled to avoid losing your domain.";

$_LANG['domainreglockstatus'] = "Registrar Lock Status";
$_LANG['domainreglockinfo'] = "Registrar Lock (also known as Theft Protection) secures your Domain from unauthorized transfers.";
$_LANG['domainreglockrecommend'] = "We recommend that you keep this enabled, except when transferring your Domain Name away.";
$_LANG['domainreglockenable'] = "Enable Registrar Lock";
$_LANG['domainreglockdisable'] = "Disable Registrar Lock";

$_LANG['domaincontactinfoedit'] = "Edit Contact Information";

$_LANG['domainmassrenew'] = "Renew Domains";

# reCAPTCHA

$_LANG['captchatitle'] = "Spam Bot Verification";
$_LANG['captchaverify'] = "Please enter the characters you see in the image below into the text box provided. This is required to prevent automated submissions.";
$_LANG['captchaverifyincorrect'] = "The characters you entered didn't match the image shown. Please try again.";
$_LANG['googleRecaptchaIncorrect'] = "Please complete the captcha and try again.";
$_LANG['recaptcha-invalid-site-private-key'] = "An error occurred, please contact support (error code: cap1)";
$_LANG['recaptcha-invalid-request-cookie'] = "An error occurred, please try again (error code: cap2)";
$_LANG['recaptcha-incorrect-captcha-sol'] = "The characters you entered didn't match the word verification. Please try again.";

# Product Bundles

$_LANG['bundledeal'] = "Bundle Deal!";
$_LANG['bundlevaliddateserror'] = "Bundle Unavailable";
$_LANG['bundlevaliddateserrordesc'] = "This bundle is either not yet active or has expired. If you feel this message to be an error, please contact support.";
$_LANG['bundlemaxusesreached'] = "Bundle Unavailable";
$_LANG['bundlemaxusesreacheddesc'] = "This bundle offer has reached the maximum number of uses allowed and so unfortunately is no longer available. Please contact us if you you're interested in our services to discuss.";
$_LANG['bundlereqsnotmet'] = "Bundle Requirements Not Met";
$_LANG['bundlewarningpromo'] = "The selected bundle cannot be used in conjunction with any other promotions or offers";
$_LANG['bundlewarningproductcycle'] = "The selected bundle requires you choose the billing cycle '%s' for product %s to qualify";
$_LANG['bundlewarningproductconfopreq'] = "The selected bundle requires you select '%s' for '%s' in order to qualify";
$_LANG['bundlewarningproductconfopyesnoenable'] = "The selected bundle requires you enable the option '%s' in order to qualify";
$_LANG['bundlewarningproductconfopyesnodisable'] = "The selected bundle requires you deselect the option '%s' in order to qualify";
$_LANG['bundlewarningproductconfopqtyreq'] = "The selected bundle requires you choose a quantity of '%s' for '%s' in order to qualify";
$_LANG['bundlewarningproductaddonreq'] = "The selected bundle requires you select the addon '%s' for product %s to qualify";
$_LANG['bundlewarningdomainreq'] = "The selected bundle requires you register or transfer a domain with the product %s to qualify";
$_LANG['bundlewarningdomaintld'] = "The selected bundle requires you choose a domain with the extension(s) '%s' for domain %s to qualify";
$_LANG['bundlewarningdomainregperiod'] = "The selected bundle requires you select the registration period '%s' for domain %s to qualify";
$_LANG['bundlewarningdomainaddon'] = "The selected bundle requires you select the addon '%s' for domain %s to qualify";

# New Client Area Template  Lines

$_LANG['navservices'] = "Services";
$_LANG['navservicesorder'] = "Order New Services";
$_LANG['navservicesplaceorder'] = "Place a New Order";
$_LANG['navdomains'] = "Domains";
$_LANG['navrenewdomains'] = "Renew Domains";
$_LANG['navregisterdomain'] = "Registrasi domain baru";
$_LANG['navtransferdomain'] = "Transfer Domain ke kami";
$_LANG['navdomainsearch'] = "Cari Domain";
$_LANG['navbilling'] = "Billing";
$_LANG['navinvoices'] = "Invoices";
$_LANG['navsupport'] = "Support";
$_LANG['navtickets'] = "Tiket";
$_LANG['navopenticket'] = "Open Ticket";
$_LANG['navmanagecc'] = "Atur Kartu Kredit";
$_LANG['navemailssent'] = "Riwayat Email";

$_LANG['hello'] = "Hai";
$_LANG['helloname'] = "Hai, %s!";
$_LANG['account'] = "Akun";
$_LANG['login'] = "Login";
$_LANG['register'] = "Register";
$_LANG['forgotpw'] = "Lupa Password?";
$_LANG['editaccountdetails'] = "Edit Akun";

$_LANG['clientareanavccdetails'] = "Detail Kartu Kredit";
$_LANG['clientareanavcontacts'] = "Contacts/Sub-Accounts";

$_LANG['manageyouraccount'] = "Manage Your Account";
$_LANG['accountoverview'] = "Account Overview";
$_LANG['paymentmethod'] = "Pilihan Pembayaran";
$_LANG['paymentmethoddefault'] = "Use Default (Set Per Order)";
$_LANG['productmanagementactions'] = "Management Actions";
$_LANG['clientareanoaddons'] = "No Addons Purchased Yet";
$_LANG['downloadssearch'] = "Search Downloads";
$_LANG['emailviewmessage'] = "View Message";
$_LANG['resultsperpage'] = "Results Per Page";
$_LANG['accessdenied'] = "Access Denied";
$_LANG['search'] = "Cari";
$_LANG['cancel'] = "Batal";
$_LANG['clientareabacklink'] = "&laquo; Kembali";
$_LANG['backtoserviceslist'] = "&laquo; Kembali ke Services List";
$_LANG['backtodomainslist'] = "&laquo; Kembali ke Domains List";

$_LANG['clientareahomeorder'] = "Visit the Order Form to browse the Products & Services we offer. Existing customers can also purchase optional extras and addons here.";
$_LANG['clientareahomelogin'] = "Sudah Registrasi? jika sudah, klik tombol dibawah ini untuk login di area klien dimana anda dapat mengatur akun anda.";
$_LANG['clientareahomeorderbtn'] = "Go to Order Form";
$_LANG['clientareahomeloginbtn'] = "Secure Client Login";

$_LANG['clientareaproductsintro'] = "These are all the services you have registered in this account.";
$_LANG['clientareaproductdetailsintro'] = "Here is an overview of your product/service with us.";
$_LANG['clientareadomainsintro'] = "View &amp; manage all the domains you have registered with us from here...";
$_LANG['invoicesintro'] = "Below you can review your entire invoice history with us.";
$_LANG['quotesintro'] = "Here are all the quotes we've generated for you.";
$_LANG['emailstagline'] = "Here's a copy of the recent emails we've sent you...";
$_LANG['supportticketsintro'] = "Submit and track any enquiries with us here...";
$_LANG['addfundsintro'] = "Deposit money in advance";
$_LANG['registerintro'] = "Buat akun baru ...";
$_LANG['masspayintro'] = "Pay all the invoices listed below in a single easy transaction by choosing a payment method";
$_LANG['networkstatusintro'] = "Service Status Information and Network Announcements";

$_LANG['creditcardyourinfo'] = "Billing Information";
$_LANG['ourlatestnews'] = "Our Latest News";
$_LANG['ccexpiringsoon'] = "Credit Card Expiring Soon";
$_LANG['ccexpiringsoondesc'] = "Your credit card is expiring soon so please ensure you %supdate your card details%s with us when you can";
$_LANG['availcreditbal'] = "Available Credit Balance";
$_LANG['availcreditbaldesc'] = "You have a credit balance of %s and this will be automatically applied to any new invoices";
$_LANG['youhaveoverdueinvoices'] = "You have %s Overdue Invoice(s)";
$_LANG['overdueinvoicesdesc'] = "To avoid service interruption, please pay your outstanding invoices as soon as possible. %sPay Now &raquo;%s";
$_LANG['supportticketsnoneopen'] = "There are currently no open support tickets";
$_LANG['invoicesnoneunpaid'] = "There are currently no unpaid invoices";

$_LANG['registerdisablednotice'] = "To register please place an <strong><a href=\"cart.php\">order</a></strong>";
$_LANG['registerCreateAccount'] = "To create an account, please";
$_LANG['registerCreateAccountOrder'] = "place an order with us";

$_LANG['pwstrength'] = "Password Strength";
$_LANG['pwstrengthenter'] = "Enter a Password";
$_LANG['pwstrengthweak'] = "Weak";
$_LANG['pwstrengthmoderate'] = "Moderate";
$_LANG['pwstrengthstrong'] = "Strong";

$_LANG['managing'] = "Managing";
$_LANG['information'] = "Information";
$_LANG['withselected'] = "With Selected";
$_LANG['managedomain'] = "Manage Domain";
$_LANG['changenameservers'] = "Change Nameservers";
$_LANG['clientareadomainmanagedns'] = "Manage DNS";
$_LANG['clientareadomainmanageemailfwds'] = "Manage Email Forwards";
$_LANG['moduleactionsuccess'] = "Action Completed Successfully!";
$_LANG['moduleactionfailed'] = "Action Failed";

$_LANG['domaininfoexp'] = "To the right you can find the details of your domain. You can manage your domain using the tabs above.";
$_LANG['domainrenewexp'] = "Enable auto renew to have us automatically send you a renewal invoice before your domain expires.";
$_LANG['domainnsexp'] = "You can change where your domain points to here. Please be aware changes can take up to 24 hours to propagate.";
$_LANG['domainlockingexp'] = "Lock your domain to prevent it from being transferred away without your authorization.";
$_LANG['domaincurrentlyunlocked'] = "Domain Currently Unlocked!";
$_LANG['domaincurrentlyunlockedexp'] = "You should enable the registrar lock unless you are transferring the domain.";
$_LANG['searchmultipletlds'] = "Search Multiple TLDs";

$_LANG['networkstatustitle'] = "Network Status";
$_LANG['networkstatusnone'] = "There are no %s Network Issues Currently";
$_LANG['serverstatusheadingtext'] = "Below is a real-time overview of our servers where you can check if there's any known issues.";

$_LANG['clientareacancelreasonrequired'] = "You must enter a cancellation reason";

$_LANG['addfundsdescription'] = "Add funds to your account with us to avoid lots of small transactions and to automatically take care of any new invoices to generate.";
$_LANG['addfundsnonrefundable'] = "* All deposits are non-refundable.";

$_LANG['creditcardexpirydateinvalid'] = "The expiry date must be entered in the format MM/YY and must not be in the past";

$_LANG['domaincheckerchoosedomain'] = "Choose a Domain...";
$_LANG['domaincheckerchecknewdomain'] = "Check Availability of a New Domain";
$_LANG['domaincheckerdomainexample'] = " eg. example.com";
$_LANG['domaincheckerhostingonly'] = "Order Hosting Only";
$_LANG['domaincheckerenterdomain'] = "Start your web hosting experience with us by entering the domain name you want to register, transfer or simply purchase hosting for below...";

$_LANG['kbquestionsearchere'] = "Have a question? Start your search here.";
$_LANG['contactus'] = "Contact Us";

$_LANG['opennewticket'] = "Open New Ticket";
$_LANG['searchtickets'] = "Enter Ticket # or Subject";
$_LANG['supportticketspriority'] = "Priority";
$_LANG['supportticketsubmitted'] = "Submitted";
$_LANG['supportticketscontact'] = "Contact";
$_LANG['supportticketsticketlastupdated'] = "Last Updated";

$_LANG['upgradedowngradepackage'] = "Upgrade/Downgrade";
$_LANG['upgradedowngradechooseproduct'] = "Choose Product";

$_LANG['jobtitlereqforcompany'] = "(Required if Organization Name is set)";

$_LANG['downloadproductrequired'] = "Downloading this item requires you to have an active instance of the following product/service:";

$_LANG['affiliatesignuptitle'] = "Get Paid for Referring Customers to Us";
$_LANG['affiliatesignupintro'] = "Activate your affiliate account and start earning money today...";
$_LANG['affiliatesignupinfo1'] = "We pay commissions for every signup that comes via your custom signup link.";
$_LANG['affiliatesignupinfo2'] = "We track the visitors you refer to us using cookies, so users you refer don't have to purchase instantly for you to receive your commission.  Cookies last for up to 90 days following the initial visit.";
$_LANG['affiliatesignupinfo3'] = "If you would like to find out more, please contact us.";

# Version 5.1

$_LANG['copyright'] = "Copyright";
$_LANG['allrightsreserved'] = "All Rights Reserved";
$_LANG['supportticketsclose'] = "Close";
$_LANG['affiliatesinitialthen'] = "Initially then";
$_LANG['invoicesoutstandingbalance'] = "Outstanding Balance";

$_LANG['cpanellogin'] = "Login to cPanel";
$_LANG['cpanelwhmlogin'] = "Login to WHM";
$_LANG['cpanelwebmaillogin'] = "Login to Webmail";
$_LANG['enkompasslogin'] = "Login to Enkompass";
$_LANG['plesklogin'] = "Login to Plesk Control Panel";
$_LANG['helmlogin'] = "Login to Helm Control Panel";
$_LANG['hypervmrestart'] = "Restart VPS Server";
$_LANG['siteworxlogin'] = "Login to SiteWorx Control Panel";
$_LANG['nodeworxlogin'] = "Login to NodeWorx Control Panel";
$_LANG['veportallogin'] = "Login to vePortal";
$_LANG['virtualminlogin'] = "Login to Control Panel";
$_LANG['websitepanellogin'] = "Login to Control Panel";
$_LANG['whmsoniclogin'] = "Login to Control Panel";
$_LANG['xpanelmaillogin'] = "Login to Webmail";
$_LANG['xpanellogin'] = "Login to XPanel";
$_LANG['heartinternetlogin'] = "Login to Control Panel";
$_LANG['gamecplogin'] = "Login to GameCP";
$_LANG['fluidvmrestart'] = "Restart VPS Server";
$_LANG['enomtrustedesc'] = "The TRUSTe Control Panel contains the set up wizard to get your Privacy Policy up and running.";
$_LANG['enomtrustelogin'] = "Login to TrustE Control Panel";
$_LANG['directadminlogin'] = "Login to DirectAdmin";
$_LANG['centovacastlogin'] = "Login to Centova Cast";
$_LANG['castcontrollogin'] = "Login to Control Panel";

$_LANG['sslconfigurenow'] = "Configure Now";
$_LANG['sslprovisioningdate'] = "SSL Provisioning Date";
$_LANG['globalsignvoucherscode'] = "Your OneClickSSL Voucher Code";
$_LANG['globalsignvouchersnotissued'] = "Not Yet Issued";

$_LANG['domaintrffailreasonunavailable'] = "Failure Reason Unavailable";

$_LANG['clientareaprojects'] = "My Projects";

$_LANG['clientgroupdiscount'] = "Client Discount";
$_LANG['billableitemshours'] = "Hours";
$_LANG['billableitemshour'] = "Hour";

$_LANG['invoicefilename'] = "Invoice-";
$_LANG['quotefilename'] = "Quote-";

# Domain Addons

$_LANG['domainaddons'] = "Addons";
$_LANG['domainaddonsinfo'] = "The following addons are available for your domain(s)...";
$_LANG['domainaddonsdnsmanagement'] = "DNS Host Record Management";
$_LANG['domainaddonsidprotectioninfo'] = "Protect your personal information and reduce the amount of spam to your inbox by enabling ID Protection.";
$_LANG['domainaddonsdnsmanagementinfo'] = "External DNS Hosting can help speed up your website and improve availability with reduced redundancy.";
$_LANG['domainaddonsemailforwardinginfo'] = "Get emails forwarded to alternate email addresses of your choice so that you can monitor all from a single account.";
$_LANG['domainaddonsbuynow'] = "Buy Now for";
$_LANG['domainaddonsperyear'] = "/Year";
$_LANG['domainaddonscancelareyousure'] = "Are you sure you want to disable & cancel this domain addon?";
$_LANG['domainaddonsconfirm'] = "Confirm Cancellation";
$_LANG['domainaddonscancelsuccess'] = "Addon Deactivated Successfully!";
$_LANG['domainaddonscancelfailed'] = "Failed to deactivate addon. Please contact support.";

# Version 5.2

$_LANG['yourclientareahostingaddons'] = "You have the following addons for this product.";
$_LANG['loginrequired'] = "Login Required";
$_LANG['unsubscribe'] = "Unsubscribe";
$_LANG['emailoptout'] = "Newsletter Opt-out";
$_LANG['newsletterunsubscribe'] = "Newsletter Unsubscribe";
$_LANG['emailoptoutdesc'] = "Tick to unsubscribe from our newsletters";
$_LANG['alreadyunsubscribed'] = "You have already unsubscribed from our newsletter.";
$_LANG['newsletterresubscribe'] = "If you wish to re-subscribe you can do so from the %sMy Details%s section of our client area at any time.";
$_LANG['unsubscribehashinvalid'] = "Unsubscribe failed, please contact support.";
$_LANG['unsubscribesuccess'] = "Unsubscribe Successful";
$_LANG['newsletterremoved'] = "Thank you, Your email has now been removed from our mailing list.";
$_LANG['erroroccured'] = "An Error Occurred";
$_LANG['pwresetsuccessdesc'] = "Your password has now been reset. %sClick here%s to continue to the client area...";
$_LANG['pwresetenternewpw'] = "Please enter your desired new password below.";
$_LANG['ordererrorsbudomainbanned'] = "The subdomain prefix you entered is not allowed - please try another";

$_LANG['ticketfeedbacktitle'] = "Feedback Request for Ticket";

$_LANG['nosupportdepartments'] = "No support departments found. Please try again later.";

$_LANG['feedbackclosed'] = "Feedback cannot be provided until the ticket is closed";
$_LANG['feedbackprovided'] = "You have already provided feedback for this ticket";
$_LANG['feedbackthankyou'] = "We thank you for taking the time to provide your feedback.";
$_LANG['feedbackreceived'] = "Submission Received";
$_LANG['feedbackdesc'] = "Please can we ask you to take a moment of your time to fill out the below form about the quality of your experience with our support team.";
$_LANG['feedbackclickreview'] = "Click here to Review The Ticket";
$_LANG['feedbackopenedat'] = "Opened At";
$_LANG['feedbacklastreplied'] = "Last Replied To";
$_LANG['feedbackstaffinvolved'] = "Staff Involved";
$_LANG['feedbacktotalduration'] = "Total Duration";
$_LANG['feedbackpleaserate1'] = "Please rate (on a scale of 1 to 10) how well";
$_LANG['feedbackpleasecomment1'] = "Please comment on how well";
$_LANG['feedbackhandled'] = "handled this support request";
$_LANG['feedbackworst'] = "Worst";
$_LANG['feedbackbest'] = "Best";
$_LANG['feedbackimprove'] = "How may we make your experience better in the future?";
$_LANG['pleaserate2'] = "handled this support request";
$_LANG['feedbacksupplyrating'] = "Please supply at least a rating for :staffname (comments are optional)";

$_LANG['returnclient'] = "Return to Client Area";

$_LANG['clientareanavsecurity'] = "Security Settings";
$_LANG['twofactorauth'] = "Two-Factor Authentication";
$_LANG['twofaenable'] = "Enable Two-Factor Authentication";
$_LANG['twofadisable'] = "Disable Two-Factor Authentication";
$_LANG['twofaenableclickhere'] = "Click here to Enable";
$_LANG['twofadisableclickhere'] = "Click here to Disable";
$_LANG['twofaenforced'] = "For your security, we require that you must enable Two-Factor Authentication before you can continue. This page will guide you through the process of setting it up.";
$_LANG['twofasetup'] = "Two-Factor Authentication Setup Process";
$_LANG['twofasetupgetstarted'] = "Get Started";
$_LANG['twofaactivationintro'] = "Two-Factor Authentication adds an extra layer of protection to logins. Once enabled &amp; configured, each time you sign in you will be asked to enter both your username & password as well as a second factor such as a security code.";
$_LANG['twofaactivationmultichoice'] = "To continue, please choose your desired Two-Factor Authentication method from below.";
$_LANG['twofadisableintro'] = "To disable Two-Factor Authentication please confirm your password in the field below.";
$_LANG['twofaactivationerror'] = "An error occurred while attempting to activate Two-Factor Authentication for your account. Please try again.";
$_LANG['twofamoduleerror'] = "An error occurred loading the module. Please try again.";
$_LANG['twofaactivationcomplete'] = "Two-Factor Authentication Setup is Complete!";
$_LANG['twofadisableconfirmation'] = "Two-Factor Authentication has now been disabled for your account.";
$_LANG['twofabackupcodeis'] = "Your Backup Code is";
$_LANG['twofanewbackupcodeis'] = "Your New Backup Code is";
$_LANG['twofabackupcodelogin'] = "Enter Your Backup Code to Login";
$_LANG['twofabackupcodeexpl'] = "Write this down on paper and keep it safe.<br />It will be needed if you ever lose your 2nd factor device or it is unavailable to you.";
$_LANG['twofaconfirmpw'] = "Enter Your Password";
$_LANG['twofa2ndfactorreq'] = "Your second factor is required to complete login.";
$_LANG['twofa2ndfactorincorrect'] = "The second factor you supplied was incorrect. Please try again.";
$_LANG['twofabackupcodereset'] = "Login via Backup Code Successful<br />Backup Codes are valid once only. It will now be reset.";
$_LANG['twofacantaccess2ndfactor'] = "Can't Access Your 2nd Factor Device?";
$_LANG['twofaloginusingbackupcode'] = "Login using Backup Code";
$_LANG['twofageneralerror'] = "An error occurred loading the module. Please try again.";

$_LANG['continue'] = "Continue";
$_LANG['disable'] = "Disable";
$_LANG['manage'] = "Manage";

# Version 5.3
$_LANG['quoteacceptancetitle'] = "Quote Acceptance";
$_LANG['quoteacceptancehowto'] = "To accept the quote, please confirm your acceptance of our terms of service which can be viewed @";
$_LANG['quoteacceptancewarning'] = "Please be aware that accepting a quote is considered entering into a contract and you will not be able to cancel once accepted.";

$_LANG['contactform'] = "Contact Form";

$_LANG['twoipverificationstep'] = "Verification Step";
$_LANG['twoipverificationstepmsg'] = "Enter the security code generated by your mobile authenticator app and we'll make sure it's configured correctly before enabling it.";
$_LANG['twoipverificationerror'] = "It seem's there's a problem...";
$_LANG['twoipcodemissmatch'] = "The code you entered did not match what was expected. Please try again.";
$_LANG['twoiptimebasedpassword'] = "Time-based One-Time Password";
$_LANG['twoiptimebasedexplain'] = "This authentication option get's it's second factor using a time based algorithm.  Your mobile phone can be used to generate the codes.  If you don't already have an app that can do this, we recommend Google Authenticator which is available for iOS, Android and Windows mobile devices.";
$_LANG['twoipconfigureapp'] = "To configure your authenticator app:";
$_LANG['twoipconfigurestep1'] = "Begin by selecting to add a new time based token";
$_LANG['twoipconfigurestep2'] = "Then use your app to scan the barcode below, or alternatively enter this secret key manually: ";
$_LANG['twoipgdmissing'] = "GD is missing from the PHP build on your server so unable to generate image";

$_LANG['domaincontactdetails']['First Name'] = "First Name";
$_LANG['domaincontactdetails']['Last Name'] = "Last Name";
$_LANG['domaincontactdetails']['Full Name'] = "Full Name";
$_LANG['domaincontactdetails']['Contact Name'] = "Contact Name";
$_LANG['domaincontactdetails']['Email'] = "Email";
$_LANG['domaincontactdetails']['Email Address'] = "Email Address";
$_LANG['domaincontactdetails']['Job Title'] = "Job Title";
$_LANG['domaincontactdetails']['Company Name'] = "Company Name";
$_LANG['domaincontactdetails']['Organisation Name'] = "Organisation Name";
$_LANG['domaincontactdetails']['Address'] = "Address";
$_LANG['domaincontactdetails']['Street'] = "Street";
$_LANG['domaincontactdetails']['Address 1'] = "Address 1";
$_LANG['domaincontactdetails']['Address 2'] = "Address 2";
$_LANG['domaincontactdetails']['Address 3'] = "Address 3";
$_LANG['domaincontactdetails']['City'] = "City";
$_LANG['domaincontactdetails']['State'] = "State";
$_LANG['domaincontactdetails']['County'] = "County";
$_LANG['domaincontactdetails']['Region'] = "Region";
$_LANG['domaincontactdetails']['Postcode'] = "Postcode";
$_LANG['domaincontactdetails']['ZIP Code'] = "ZIP Code";
$_LANG['domaincontactdetails']['ZIP'] = "ZIP";
$_LANG['domaincontactdetails']['Country'] = "Country";
$_LANG['domaincontactdetails']['Phone'] = "Phone";
$_LANG['domaincontactdetails']['Phone Number'] = "Phone Number";
$_LANG['domaincontactdetails']['Fax'] = "Fax";

$_LANG['serverhostnameexample'] = "eg. server1(.example.com)";
$_LANG['serverns1prefixexample'] = "eg. ns1(.example.com)";
$_LANG['serverns2prefixexample'] = "eg. ns2(.example.com)";

$_LANG['hosting'] = "Hosting";

$_LANG['enomfrregistration']['Heading'] = ".fr domains have different required values depending on your nationality and type of registration:";
$_LANG['enomfrregistration']['French Individuals']['Name'] = "French Individuals";
$_LANG['enomfrregistration']['French Individuals']['Requirements'] = "Please provide your \"Birthdate\", \"Birthplace City\", and \"Birthplace Postcode\".";
$_LANG['enomfrregistration']['EU Non-French Individuals']['Name'] = "EU Non-French Individuals";
$_LANG['enomfrregistration']['EU Non-French Individuals']['Requirements'] = "Please provide your \"Birthdate\".";
$_LANG['enomfrregistration']['French Companies']['Name'] = "French Companies";
$_LANG['enomfrregistration']['French Companies']['Requirements'] = "Please provide the \"Birthdate\", \"Birthplace City\", and \"Birthplace Postcode\" for the owner contact, along with your SIRET number.";
$_LANG['enomfrregistration']['EU Non-French Companies']['Name'] = "EU Non-French Companies";
$_LANG['enomfrregistration']['EU Non-French Companies']['Requirements'] = "Please provide the company \"DUNS Number\", and the \"Birthdate\" of the Owner Contact.";
$_LANG['enomfrregistration']['Non-EU Warning'] = "Client contact information must be within the EU or else registration will fail.";

$_LANG['confirm'] = "Confirm";

$_LANG['maxmind_checkconfiguration'] = "An error occurred with the Fraud Check. Please contact support.";
$_LANG['maxmind_addressinvalid'] = "Your address is not recognised. Please check and re-enter.";
$_LANG['maxmind_invalidip'] = "IP Address invalid or a local address. Please contact support.";

$_LANG['ssounabletologin'] = "Unable to auto-login. Please contact support.";
$_LANG['ssofatalerror'] = "A fatal error occurred. Please contact support.";

# Version 6.0

$_LANG['announcementschoosemonth'] = "Choose Month";
$_LANG['announcementsbymonth'] = "By Month";
$_LANG['announcementsolder'] = "Older Announcements";
$_LANG['createnewcontact'] = "New Contact...";
$_LANG['due'] = "Due";
$_LANG['affiliatessignups'] = "Signups";
$_LANG['affiliatesconversionrate'] = "Conversions";
$_LANG['affiliatesclicks'] = "Clicks";
$_LANG['contacts'] = "Contacts";
$_LANG['backtoservicedetails'] = "Back to Service Details";
$_LANG['invoicesintro'] = "Your invoice history with us";

$_LANG['sidebars']['viewAccount']['yourAccount'] = "Your Account";
$_LANG['sidebars']['viewAccount']['myDetails'] = "My Details";
$_LANG['sidebars']['viewAccount']['billingInformation'] = "Billing Information";
$_LANG['sidebars']['viewAccount']['contacts/subAccounts'] = "Contacts/Sub-Accounts";
$_LANG['sidebars']['viewAccount']['changePassword'] = "Change Password";
$_LANG['sidebars']['viewAccount']['securitySettings'] = "Security Settings";
$_LANG['sidebars']['viewAccount']['emailHistory'] = "Email History";

$_LANG['aboutsecurityquestions'] = "Why security questions?";
$_LANG['registersecurityquestionblurb'] = "Setting a security question will provide extra security, as all changes to your account require providing the additional information from your question.";

$_LANG['update'] = "Update";
$_LANG['yourinfo'] = "Your Info";
$_LANG['shortcuts'] = "Shortcuts";

$_LANG['yourservices'] = "Your Services";
$_LANG['yourdomains'] = "Your Domains";
$_LANG['yourtickets'] = "Your Tickets";
$_LANG['managecontacts'] = "Manage Contacts";
$_LANG['billingdetails'] = "Billing Details";
$_LANG['homechooseproductservice'] = "Choose a product/service to manage:";

$_LANG['invoicesdue'] = "Invoices Due";
$_LANG['invoicesduemsg'] = "You have %s invoice(s) currently unpaid with a total balance of %s";
$_LANG['noinvoicesduemsg'] = "You have no unpaid invoices at this time.";

$_LANG['expiringsoon'] = "Expiring Soon";

$_LANG['notice'] = "Notice";
$_LANG['networkstatussubtitle'] = "News & Information";

$_LANG['myaccount'] = "My Account";

$_LANG['manageproduct'] = "Manage Product";
$_LANG['overview'] = "Overview";
$_LANG['servername'] = "Server Name";
$_LANG['visitwebsite'] = "Visit Website";
$_LANG['whoisinfo'] = "WHOIS Info";

$_LANG['tableshowing'] = "Showing _START_ to _END_ of _TOTAL_ entries";
$_LANG['tableempty'] = "Showing 0 to 0 of 0 entries";
$_LANG['tablefiltered'] = "(filtered from _MAX_ total entries)";
$_LANG['tablelength'] = "Show _MENU_ entries";
$_LANG['tableloading'] = "Loading...";
$_LANG['tableprocessing'] = "Processing...";
$_LANG['tablepagesfirst'] = "First";
$_LANG['tablepageslast'] = "Last";
$_LANG['tablepagesnext'] = "Next";
$_LANG['tablepagesprevious'] = "Previous";
$_LANG['tableviewall'] = "All";
$_LANG['tableentersearchterm'] = "Enter search term...";

$_LANG['actions'] = "Actions";

$_LANG['upgradedowngradeshort'] = "Up/Downgrade";

$_LANG['masspayintro'] = "Pay all these invoices in one step";
$_LANG['masspaymentselectgateway'] = "Select Mass Payment Gateway";

$_LANG['ticketfeedbackrequest'] = "Feedback Request";
$_LANG['ticketfeedbackforticket'] = "for Ticket #";

$_LANG['notifications'] = "Notifikasi";
$_LANG['notificationsnone'] = "You have no notifications at this time.";

$_LANG['creditcardnonestored'] = "No card on file";

$_LANG['kbviewingarticlestagged'] = "Viewing articles tagged";

$_LANG['domainprivatenameservers'] = "Private Nameservers";

$_LANG['transferinadomain'] = "Transfer in a Domain";

$_LANG['nodomainextensions'] = "There are no Domain Extensions currently configured for purchase";

$_LANG['homebegin'] = "Mulai mencari domain yang anda inginkan ..";
$_LANG['howcanwehelp'] = "How can we help today?";
$_LANG['howcanwehelp'] = "Bagaimana kami membantu anda?";
$_LANG['exampledomain'] = "eg. example.com";
$_LANG['buyadomain'] = "Beli Homain";
$_LANG['orderhosting'] = "Order Hosting";
$_LANG['makepayment'] = "Pembayaran";
$_LANG['getsupport'] = "Get Support";

$_LANG['news'] = "News";
$_LANG['allthelatest'] = "All the latest from";
$_LANG['readmore'] = "Read More";
$_LANG['noannouncements'] = "No announcements to display";

$_LANG['kbsearchexplain'] = "Have a question? Start your search here.";
$_LANG['readyforquestions'] = "We're ready and waiting for your questions";

$_LANG['restrictedpage'] = "This page is restricted";
$_LANG['enteremail'] = "Enter email";

$_LANG['passwordtips'] = "<strong>Tips for a good password</strong><br />Use both upper and lowercase characters<br />Include at least one symbol (# $ ! % &amp; etc...)<br />Don't use dictionary words";

$_LANG['regdate'] = "Reg Date";
$_LANG['nextdue'] = "Next Due";

$_LANG['findyourdomain'] = "Find your new domain name";
$_LANG['searchtermrequired'] = "You must enter a domain name or keyword to look for";
$_LANG['unabletolookup'] = "Sorry, unable to lookup the term as provided";
$_LANG['invalidchars'] = "Please remove spaces or special punctuation characters";
$_LANG['bulkoptions'] = "Bulk Options";
$_LANG['checkingdomain'] = "Checking if the domain you requested is available...";
$_LANG['domainsgotocheckout'] = "Go to checkout";
$_LANG['domainssearchresults'] = "Search Results";
$_LANG['domainssuggestions'] = "Suggestions";
$_LANG['domainsothersuggestions'] = "You might also be interested in the following alternative names";
$_LANG['domainsmoresuggestions'] = "Give me more suggestions!";
$_LANG['domainssuggestionswarnings'] = "Domain name suggestions may not always be available. Availability is checked in real-time at the point of adding to the cart.";
$_LANG['disclaimers'] = "Disclaimers";
$_LANG['tldpricing'] = "TLD Pricing";
$_LANG['alltldpricing'] = "All TLD Pricing";

$_LANG['quotesdesc'] = "Quotes we have generated for you";
$_LANG['quotesrejected'] = "Rejected";

$_LANG['ticketsyourhistory'] = "Your ticket history";

$_LANG['clientareaemaildesc'] = "Your email history with us";

$_LANG['sslconfssl'] = "Configure SSL";
$_LANG['sslnoconfigurationpossible'] = "Configuration has already been completed!  Please contact support if you are experiencing issues.";

$_LANG['adminloggedin'] = "You are currently logged in as an admin.";
$_LANG['returntoadminarea'] = "Return to Admin Area";
$_LANG['adminmasqueradingasclient'] = "You are currently masquerading as a client.";
$_LANG['logoutandreturntoadminarea'] = "Logout & return to Admin Area";

$_LANG['supportAndUpdatesExpired'] = "Support & Updates are expired.";
$_LANG['supportAndUpdatesExpiredLicense'] = "Your Support & Updates period for this license has expired";
$_LANG['supportAndUpdatesRenewalRequired'] = "Support & Updates access needs to be renewed before you can access this download.";
$_LANG['supportAndUpdatesClickHereToRenew'] = "Click Here to Renew";

$_LANG['pwresetemailneeded'] = "Forgotten your password? Enter your email address below to begin the reset process.";

$_LANG['quotestageexpired'] = "Expired";

$_LANG['ticketinfo'] = "Ticket Information";
$_LANG['customfield'] = "Custom Fields";

$_LANG['domainsActive'] = "Active";
$_LANG['domainsExpired'] = "Expired";
$_LANG['domainsCancelled'] = "Cancelled";
$_LANG['domainsFraud'] = "Fraud";
$_LANG['domainsPending'] = "Pending";
$_LANG['domainsPendingTransfer'] = "Pending Transfer";
$_LANG['domainsTransferredAway'] = "Transferred Away";
$_LANG['domainsExpiringInTheNext30Days'] = "Expiring within 30 Days";
$_LANG['domainsExpiringInTheNext90Days'] = "Expiring within 90 Days";
$_LANG['domainsExpiringInTheNext180Days'] = "Expiring within 180 Days";
$_LANG['domainsExpiringInMoreThan180Days'] = "Expiring in 180+ Days";

$_LANG['kbtagcloud'] = "Tag Cloud";

$_LANG['cancellationrequestedexplanation'] = "There is an outstanding cancellation request for this product/service";
$_LANG['cancellationrequested'] = "Cancellation Requested";

$_LANG['yourrecenttickets'] = "Your Recent Tickets";

$_LANG['domains']['deTermsDescription1'] = "To register a new domain, transfer or change registrant information the registrant must explicitly accept the .DE terms and conditions.";
$_LANG['domains']['deTermsDescription2'] = "(See full text of .de terms and conditions: http://www.denic.de/en/bedingungen.html.)";
$_LANG['directDebitPageTitle'] = "Direct Debit Payment";
$_LANG['directDebitHeader'] = "Direct Debit Payment";
$_LANG['directDebitErrorNoBankName'] = "You must enter your banks name";
$_LANG['directDebitErrorAccountType'] = "You must select your bank account type";
$_LANG['directDebitErrorNoABA'] = "You must enter your banks ABA code";
$_LANG['directDebitErrorAccNumber'] = "You must enter your bank account number";
$_LANG['directDebitErrorConfirmAccNumber'] = "You must confirm your bank account number";
$_LANG['directDebitErrorAccNumberMismatch'] = "Your bank account number & confirmation don't match";
$_LANG['directDebitThanks'] = "Thank you for submitting your details. We will attempt to process your payment using the supplied details within the next few days, and contact you in case of any problems.";
$_LANG['directDebitPleaseSubmit'] = "Please submit your bank account details below to pay by Direct Debit.";
$_LANG['directDebitBankName'] = "Bank Name";
$_LANG['directDebitAccountType'] = "Bank Account Type";
$_LANG['directDebitABA'] = "Bank ABA Code";
$_LANG['directDebitAccNumber'] = "Bank Account Number";
$_LANG['directDebitConfirmAccNumber'] = "Confirm Account Number";
$_LANG['directDebitSubmit'] = "Submit";
$_LANG['directDebitChecking'] = "Checking";
$_LANG['directDebitSavings'] = "Savings";

$_LANG['outOfStockProductRemoved'] = "An out of stock product was automatically removed from the cart";

$_LANG['subaccountpermsquotes'] = "View & Accept Quotes";

$_LANG['chooselanguage'] = "Pilih Bahasa";

$_LANG['success'] = "Success";
$_LANG['error'] = "Error";
$_LANG['print'] = "Print";
$_LANG['invoicelineitems'] = "Invoice Items";

$_LANG['quotelineitems'] = "Quote Items";

$_LANG['quoteproposal'] = "Proposal";
$_LANG['quoteacceptagreetos'] = "To accept the quote, please confirm your acceptance of our terms of service.";
$_LANG['quoteacceptcontractwarning'] = "Please be aware that accepting a quote is considered entering into a contract and you will not be able to cancel once accepted.";

// Client alerts
$_LANG['clientAlerts']['creditCardExpiring'] = "Your credit card :creditCardType-:creditCardLastFourDigits expires within :days days. Please update it at your earliest convenience.";
$_LANG['clientAlerts']['domainsExpiringSoon'] = "You have :numberOfDomains domain(s) expiring within the next :days days.";
$_LANG['clientAlerts']['invoicesUnpaid'] = "You have :numberOfInvoices unpaid invoice(s). Pay them early for peace of mind.";
$_LANG['clientAlerts']['invoicesOverdue'] = "You have :numberOfInvoices overdue invoice(s) with a total balance due of :balanceDue. Pay them now to avoid any interruptions in service.";
$_LANG['clientAlerts']['creditBalance'] = "You have a credit balance of :creditBalance.";

// Client homepage panels
$_LANG['clientHomePanels']['unpaidInvoices'] = "Unpaid Invoices";
$_LANG['clientHomePanels']['unpaidInvoicesMsg'] = "You have :numberOfInvoices unpaid invoice(s) with a total balance due of :balanceDue. Pay them early for peace of mind.";
$_LANG['clientHomePanels']['overdueInvoices'] = "Overdue Invoices";
$_LANG['clientHomePanels']['overdueInvoicesMsg'] = "You have :numberOfInvoices overdue invoice(s) with a total balance due of :balanceDue. Pay them now to avoid any interruptions in service.";
$_LANG['clientHomePanels']['domainsExpiringSoon'] = "Domains Expiring Soon";
$_LANG['clientHomePanels']['domainsExpiringSoonMsg'] = "You have :numberOfDomains domain(s) expiring within the next :days days. Renew them today for peace of mind.";
$_LANG['clientHomePanels']['activeProductsServices'] = "Your Active Products/Services";
$_LANG['clientHomePanels']['activeProductsServicesNone'] = "It appears you do not have any products/services with us yet. <a href=\"cart.php\">Place an order to get started</a>.";
$_LANG['clientHomePanels']['recentNews'] = "Recent News";
$_LANG['clientHomePanels']['affiliateProgram'] = "Affiliate Program";
$_LANG['clientHomePanels']['recentSupportTickets'] = "Recent Support Tickets";
$_LANG['clientHomePanels']['recentSupportTicketsNone'] = "No Recent Tickets Found. If you need any help, please <a href=\"submitticket.php\">open a ticket</a>.";
$_LANG['clientHomePanels']['affiliateSummary'] = "Your current commission balance is :commissionBalance. You only need another :amountUntilWithdrawalLevel before you can withdraw your earnings.";
$_LANG['clientHomePanels']['affiliateSummaryWithdrawalReady'] = "Your current commission balance is :commissionBalance. You may withdraw your earnings now.";

$_LANG['upgradeNotPossible'] = "Upgrading this product is not possible.  If you believe you've reached this page in error, please notify our support department.  Otherwise, go back and double check your selection.";

$_LANG['hostingInfo'] = "Hosting Information";
$_LANG['additionalInfo'] = "Additional Information";
$_LANG['resourceUsage'] = "Resource Usage";
$_LANG['primaryIP'] = "Primary IP";
$_LANG['assignedIPs'] = "Assigned IPs";
$_LANG['diskSpace'] = "Disk Space";
$_LANG['bandwidth'] = "Bandwidth";
$_LANG['registered'] = "Registered";
$_LANG['upgrade'] = "Upgrade";

$_LANG['downdoadsdesc'] = "Manuals, programs, and other files";

$_LANG['doToday'] = "What would you like to do today?";
$_LANG['changeDomainNS'] = "Change the nameservers your domain points to";
$_LANG['updateWhoisContact'] = "Update the WHOIS contact information for your domain";
$_LANG['changeRegLock'] = "Change the registrar lock status for your domain";
$_LANG['renewYourDomain'] = "Renew Your Domain";

$_LANG['oops'] = "Oops";
$_LANG['goback'] = "Go Back";
$_LANG['returnhome'] = "Return Home";
$_LANG['blankCustomField'] = "(no value)";

$_LANG['viewAll'] = "View All";
$_LANG['moreDetails'] = "More Details";

$_LANG['clientHomeSearchKb'] = "Enter a question here to search our knowledgebase for answers...";

$_LANG['whoisContactWarning'] = "It is important to keep your domain WHOIS contact information up-to-date at all times to avoid losing control of your domain.";

$_LANG['paymentstodate'] = "Payments to Date";
$_LANG['balancedue'] = "Balance Due";
$_LANG['submitpayment'] = "Submit Payment";

$_LANG['domaincheckeravailable'] = "Available";
$_LANG['domaincheckertransferable'] = "Available to Transfer";
$_LANG['domaincheckertaken'] = "Taken";
$_LANG['domaincheckeradding'] = "Adding";
$_LANG['domaincheckeradded'] = "Added";
$_LANG['domaincheckernomoresuggestions'] = "That's all the results we have for you! If you still haven't found what you're looking for, please try a different search term or keyword.";
$_LANG['domaincheckerunabletooffertld'] = "Unfortunately we are unable to register this TLD at this time";
$_LANG['domaincheckerbulkplaceholder'] = "Enter up to 20 domain names.\nEach name must be on a separate line.\n\nExamples:\nexample.com\nexample.net";

$_LANG['domainchecker']['suggestiontakentitle'] = "Domain Unavailable";
$_LANG['domainchecker']['suggestiontakenmsg'] = "Unfortunately the domain you selected is unavailable. This can sometimes occur if the domain has been registered recently. Please go back and choose another domain.";
$_LANG['domainchecker']['suggestiontakenchooseanother'] = "Choose another domain";

$_LANG['domainchecker']['alreadyincarttitle'] = "Already In Cart";
$_LANG['domainchecker']['alreadyincartmsg'] = "This domain is already in your shopping cart. Proceed to checkout now to complete your purchase.";
$_LANG['domainchecker']['alreadyincartcheckoutnow'] = "Checkout Now";

$_LANG['genericerror']['title'] = "Oops, something went wrong!";
$_LANG['genericerror']['msg'] = "Please try again and if the problem persists, please contact our support team.";

# Licensing Addon

$_LANG['licensingaddon']['mylicenses'] = "My Licenses";
$_LANG['licensingaddon']['latestdownload'] = "Latest Download";
$_LANG['licensingaddon']['downloadnow'] = "Download Now";
$_LANG['licensingaddon']['licensekey'] = "License Key";
$_LANG['licensingaddon']['validdomains'] = "Valid Domains";
$_LANG['licensingaddon']['validips'] = "Valid IPs";
$_LANG['licensingaddon']['validdirectory'] = "Valid Directory";
$_LANG['licensingaddon']['status'] = "License Status";
$_LANG['licensingaddon']['reissue'] = "Reissue";
$_LANG['licensingaddon']['reissuestatusmsg'] = "The Valid Domain, IP and Directory will be detected & saved the next time the license is accessed.";
$_LANG['licensingaddon']['manageLicense'] = "Manage License";

$_LANG['affiliateWithdrawalSummary'] = "You will be able to request a withdrawal as soon as your balance reaches the minimum required amount of :amountForWithdrawal.";

$_LANG['projectManagement']['activeProjects'] = "Your Active Projects";

# cPanel Module

$_LANG['cPanel']['packageDomain'] = "Package/Domain";
$_LANG['cPanel']['addonsExtras'] = "Addons & Extras";
$_LANG['cPanel']['purchaseActivate'] = "Purchase & Activate";

$_LANG['cPanel']['usageStats'] = "Usage Statistics";
$_LANG['cPanel']['diskUsage'] = "Disk Usage";
$_LANG['cPanel']['bandwidthUsage'] = "Bandwidth Usage";
$_LANG['cPanel']['usageStatsBwLimitNear'] = "You are nearing your bandwidth limit.";
$_LANG['cPanel']['usageStatsDiskLimitNear'] = "You are nearing your disk space limit.";
$_LANG['cPanel']['usageUpgradeNow'] = "Upgrade Now";
$_LANG['cPanel']['usageLastUpdated'] = "Last Updated";

$_LANG['cPanel']['quickShortcuts'] = "Quick Shortcuts";
$_LANG['cPanel']['emailAccounts'] = "Email Accounts";
$_LANG['cPanel']['forwarders'] = "Forwarders";
$_LANG['cPanel']['autoresponders'] = "Autoresponders";
$_LANG['cPanel']['fileManager'] = "File Manager";
$_LANG['cPanel']['backup'] = "Backup";
$_LANG['cPanel']['subdomains'] = "Subdomains";
$_LANG['cPanel']['addonDomains'] = "Addon Domains";
$_LANG['cPanel']['cronJobs'] = "Cron Jobs";
$_LANG['cPanel']['mysqlDatabases'] = "MySQL Databases";
$_LANG['cPanel']['phpMyAdmin'] = "phpMyAdmin";
$_LANG['cPanel']['awstats'] = "Awstats";

$_LANG['cPanel']['createEmailAccount'] = "Quick Create Email Account";
$_LANG['cPanel']['usernamePlaceholder'] = "Yourname";
$_LANG['cPanel']['passwordPlaceholder'] = "Desired Password";
$_LANG['cPanel']['create'] = "Create";
$_LANG['cPanel']['emailAccountCreateSuccess'] = "Email account created successfully!";
$_LANG['cPanel']['emailAccountCreateFailed'] = "Email account creation failed: ";

$_LANG['cPanel']['packageNotActive'] = "This hosting package is currently";
$_LANG['cPanel']['statusPendingNotice'] = "You cannot begin using this hosting account until it is activated.";
$_LANG['cPanel']['statusSuspendedNotice'] = "You cannot continue to use or manage this package until it is reactivated.";

$_LANG['cPanel']['billingOverview'] = "Billing Overview";

$_LANG['liveHelp']['chatNow'] = "Chat Now";

$_LANG['quotes'] = "Quotes";

$_LANG['productMustBeActiveForModuleCmds'] = "Product must be active to perform this action";
$_LANG['domainCannotBeManagedUnlessActive'] = "This domain is not currently active. Domains cannot be managed unless active.";

$_LANG['actionRequiresAtLeastOneDomainSelected'] = 'Please select at least one domain to perform the selected action on.';

$_LANG['clientAreaProductDownloadsAvailable'] = "The following download items are available for your product/service";
$_LANG['clientAreaProductAddonsAvailable'] = "Addons are available for your product/service. <a href=\"cart.php?gid=addons\">Click here to view &amp; order &raquo;</a>";
$_LANG['clientAreaSecurityTwoFactorAuthRecommendation'] = "We strongly encourage you to enable Two-Factor Authentication for added security.";
$_LANG['clientAreaSecurityNoSecurityQuestions'] = "Setting a security question and answer helps protect your account from unauthorized password resets and allows us to verify your identity when requesting account changes.";
$_LANG['clientAreaSecuritySecurityQuestionOtherError'] = "The security question helps protect your account from unauthorized password resets and allows us to verify your identity when requesting account changes.";

$_LANG['billingAddress'] = "Billing Address";

$_LANG['noPasswordResetWhenLoggedIn'] = "You cannot request a password reset when logged in. Please logout to begin.";

$_LANG['unableToLoadShoppingCart'] = "Unable to load your shopping cart. Please contact support.";

$_LANG['showMenu'] = "Show Menu";
$_LANG['hideMenu'] = "Hide Menu";

$_LANG['from'] = "From";
$_LANG['featuredProduct'] = "Most Popular";
$_LANG['shoppingCartProductPerMonth'] = "<span>:price</span>/:countmo";
$_LANG['shoppingCartProductPerYear'] = "<span>:price</span>/:countyr";

$_LANG['orderForm']['findNewDomain'] = "Find your new domain name. Enter your name or keywords below to check availability.";
$_LANG['orderForm']['transferExistingDomain'] = "Transfer your existing domain names to us and save.";
$_LANG['orderForm']['www'] = "www.";
$_LANG['orderForm']['check'] = "Check";
$_LANG['orderForm']['returnToClientArea'] = "Return to Client Area";
$_LANG['orderForm']['checkout'] = "Checkout";
$_LANG['orderForm']['alreadyRegistered'] = "Sudah Registrasi?";
$_LANG['orderForm']['createAccount'] = "Buat akun baru";
$_LANG['orderForm']['enterPersonalDetails'] = "Please enter your personal details and billing information to checkout.";
$_LANG['orderForm']['correctErrors'] = "Please correct the following errors before continuing";
$_LANG['orderForm']['existingCustomerLogin'] = "Existing Customer Login";
$_LANG['orderForm']['emailAddress'] = "Email";
$_LANG['orderForm']['personalInformation'] = "Personal Information";
$_LANG['orderForm']['firstName'] = "Nama Depan";
$_LANG['orderForm']['lastName'] = "Nama Belakang";
$_LANG['orderForm']['phoneNumber'] = "Phone Number";
$_LANG['orderForm']['billingAddress'] = "Billing Address";
$_LANG['orderForm']['companyName'] = "Nama Perusahaan";
$_LANG['orderForm']['optional'] = "Optional";
$_LANG['orderForm']['streetAddress'] = "Street Address";
$_LANG['orderForm']['streetAddress2'] = "Street Address 2";
$_LANG['orderForm']['city'] = "Kota";
$_LANG['orderForm']['state'] = "State";
$_LANG['orderForm']['country'] = "Negara";
$_LANG['orderForm']['postcode'] = "Kodepos";
$_LANG['orderForm']['domainAlternativeContact'] = "You may specify alternative registered contact details for the domain registration(s) in your order when placing an order on behalf of another person or entity. If you do not require this, you can skip this section.";
$_LANG['orderForm']['accountSecurity'] = "Account Security";
$_LANG['orderForm']['mediumStrength'] = "Medium Strength";
$_LANG['orderForm']['paymentDetails'] = "Payment Details";
$_LANG['orderForm']['preferredPaymentMethod'] = "Please choose your preferred method of payment.";
$_LANG['orderForm']['cardNumber'] = "Card Number";
$_LANG['orderForm']['cvv'] = "CVV Security Number";
$_LANG['orderForm']['additionalNotes'] = "Additional Notes";
$_LANG['orderForm']['continueToClientArea'] = "Continue To Client Area";
$_LANG['orderForm']['reviewDomainAndAddons'] = "Please review your domain name selections and any addons that are available for them.";
$_LANG['orderForm']['addToCart'] = "Add to Cart";
$_LANG['orderForm']['addedToCartRemove'] = "Added to Cart (Remove)";
$_LANG['orderForm']['configureDesiredOptions'] = "Configure your desired options and continue to checkout.";
$_LANG['orderForm']['haveQuestionsContact'] = "Have questions? Contact our sales team for assistance.";
$_LANG['orderForm']['haveQuestionsClickHere'] = "Click here";
$_LANG['orderForm']['use'] = "Use";
$_LANG['orderForm']['check'] = "Check";
$_LANG['orderForm']['transfer'] = "Transfer";
$_LANG['orderForm']['domainAddedToCart'] = "This domain has been added to your cart.";
$_LANG['orderForm']['registerLongerAndSave'] = "Register for longer and save!";
$_LANG['orderForm']['tryRegisteringInstead'] = "Try registering this domain instead.";
$_LANG['orderForm']['domainAvailabilityCached'] = "Domain availability results are cached which may lead to recently registered domains being shown as available.";
$_LANG['orderForm']['submitTicket'] = "Submit a Ticket";
$_LANG['orderForm']['promotionAccepted'] = "Promotion Code Accepted! Your order total has been updated.";
$_LANG['orderForm']['productOptions'] = "Product/Options";
$_LANG['orderForm']['qty'] = "Qty";
$_LANG['orderForm']['priceCycle'] = "Price/Cycle";
$_LANG['orderForm']['edit'] = "Edit";
$_LANG['orderForm']['update'] = "Update";
$_LANG['orderForm']['remove'] = "Remove";
$_LANG['orderForm']['applyPromoCode'] = "Apply Promo Code";
$_LANG['orderForm']['estimateTaxes'] = "Estimate Taxes";
$_LANG['orderForm']['removePromotionCode'] = "Remove Promotion Code";
$_LANG['orderForm']['updateTotals'] = "Update Totals";
$_LANG['orderForm']['continueShopping'] = "Continue Shopping";
$_LANG['orderForm']['removeItem'] = "Remove Item";
$_LANG['orderForm']['yes'] = "Ya";
$_LANG['orderForm']['cancel'] = "Batal";
$_LANG['orderForm']['close'] = "Tutup";
$_LANG['orderForm']['totals'] = "Totals";
$_LANG['orderForm']['includedWithPlans'] = "Included With Every Plan";
$_LANG['orderForm']['whatIsIncluded'] = "What is Included?";
$_LANG['orderForm']['errorNoProductGroup'] = "Could not load any product groups.";
$_LANG['orderForm']['errorNoProducts'] = "Product group does not contain any visible products";

$_LANG['cloudSlider']['feature01Title'] = "Maximum Server Uptime";
$_LANG['cloudSlider']['feature01Description'] = "Server uptime is critical for all businesses - consider it the heart beat of hosting your business. File and databases servers, email and web servers are an indispensable part of most business processes, and downtimes have direct negative effects on productivity, sales, employee and customer satisfaction. That's why ensuring the maximum server uptime is so important to us - we want to make sure your business processes are running healthy so your customers are happy.";
$_LANG['cloudSlider']['feature01DescriptionTwo'] = "By trusting us with your business needs, we promise you a 99.9% uptime on any services we provide, outside of any standard maintenance we may provide.";
$_LANG['cloudSlider']['feature02Title'] = "World Wide Data Delivery";
$_LANG['cloudSlider']['feature02Description'] = "Our services are powered by hundreds of servers and data centers located all over the world, so you can rest easy knowing that customers can access your website from anywhere. In addition, we provide monitoring tools to provide you with expert analytics - website traffic is an important step in improving your site's efficiency and popularity, as well as keeping track of where your visitors are coming from, what time of day they're visiting you, and how long they're staying. Our dedication to a global marketplace extends to domain registrations, as we offer the most popular TLD's for registration.";
$_LANG['cloudSlider']['feature02DescriptionTwo'] = "Our dedication to customer support reaches across the globe as well. We are here to help you with your hosting in any way possible, and you can reach us via phone, email, or live chat.";
$_LANG['cloudSlider']['feature03Title'] = "Top Tier Security";
$_LANG['cloudSlider']['feature03Description'] = "Rest easy knowing that we provide 24/7 security monitoring and DDoS protection. You take protecting your customer's data seriously, and so do we. Our security team and safeguards are on the job all day, every day to provide the level of security needed in today's digital age.";
$_LANG['cloudSlider']['feature03DescriptionTwo'] = "A wide range of security tools are at your disposal, including SSL certificates, firewall configuration, security monitoring services, VPN access, and more.";
$_LANG['cloudSlider']['selectProductLevel'] = "Select the Perfect Level for You!";

$_LANG['domainChecker.additionalPricingOptions'] = 'Additional Pricing Options For :domain';

$_LANG['orderpaymenttermfree'] = "Free";

$_LANG['cPanel']['usageStatsBwOverLimit'] = "You have exceeded your bandwidth limit.";
$_LANG['cPanel']['usageStatsDiskOverLimit'] = "You have exceeded your disk space limit.";
$_LANG['insufficientstockmessage'] = "We are currently low on stock on certain items. The cart quantities have been adjusted accordingly.";

$_LANG['only'] = "Only";
$_LANG['startingat'] = "Starting at";

$_LANG['yourdomainplaceholder'] = 'example';
$_LANG['yourtldplaceholder'] = 'com';

$_LANG['subaccountpermsproductsso'] = "Perform Single Sign-On";

$_LANG['sso']['title'] = "Single Sign-On";
$_LANG['sso']['summary'] = "Third party applications leverage the Single Sign-On functionality to provide direct access to your billing account without you having to re-authenticate.";
$_LANG['sso']['disablenotice'] = "You may wish to disable this functionality if you provide access to any of your third party applications to users who you do not wish to be able to access your billing account.";
$_LANG['sso']['enabled'] = "Single Sign-On is currently permitted for your account.";
$_LANG['sso']['disabled'] = "Single Sign-On is currently disabled for your account.";
$_LANG['sso']['redirectafterlogin'] = "You will be redirected after login";

$_LANG['oauth']['badTwoFactorAuthModule'] = "Bad Two-Factor Auth Module. Please contact support.";
$_LANG['oauth']['permAccessNameAndEmail'] = "Access your name and email address";

$_LANG['errorButTryAgain'] = "An error occurred.  Please try again.";
$_LANG['emailSent'] = 'Email Sent';
$_LANG['verificationKeyExpired'] = 'This email verification key has expired. Please login to request a new one.';
$_LANG['emailAddressVerified'] = 'Thank you for confirming your email address.';
$_LANG['emailKeyExpired'] = 'This email verification key has expired. Please request a new one.';
$_LANG['verifyEmailAddress'] = 'Please check your email and follow the link to verify your email address.';
$_LANG['resendEmail'] = 'Resend Verification Email';

// Markdown Editor Help
$_LANG['markdown']['title']= "Markdown Guide";
$_LANG['markdown']['emphasis']= "Emphasis";
$_LANG['markdown']['bold']= "bold";
$_LANG['markdown']['italics']= "italics";
$_LANG['markdown']['strikeThrough']= "strikethrough";
$_LANG['markdown']['headers']= "Headers";
$_LANG['markdown']['bigHeader']= "Big header";
$_LANG['markdown']['mediumHeader']= "Medium header";
$_LANG['markdown']['smallHeader']= "Small header";
$_LANG['markdown']['tinyHeader']= "Tiny header";
$_LANG['markdown']['lists']= "Lists";
$_LANG['markdown']['genericListItem']= "Generic list item";
$_LANG['markdown']['numberedListItem']= "Numbered list item";
$_LANG['markdown']['links']= "Links";
$_LANG['markdown']['textToDisplay']= "Text to display";
$_LANG['markdown']['exampleLink']= "http://www.example.com";
$_LANG['markdown']['quotes']= "Quotes";
$_LANG['markdown']['thisIsAQuote']= "This is a quote.";
$_LANG['markdown']['quoteMultipleLines']= "It can span multiple lines!";
$_LANG['markdown']['tables']= "Tables";
$_LANG['markdown']['columnOne']= "Column 1";
$_LANG['markdown']['columnTwo']= "Column 2";
$_LANG['markdown']['columnThree']= "Column 3";
$_LANG['markdown']['withoutAligning']= "Or without aligning the columns...";
$_LANG['markdown']['john']= "John";
$_LANG['markdown']['doe']= "Doe";
$_LANG['markdown']['male']= "Male";
$_LANG['markdown']['mary']= "Mary";
$_LANG['markdown']['smith']= "Smith";
$_LANG['markdown']['female'] = "Female";
$_LANG['markdown']['displayingCode'] = "Displaying code";
$_LANG['markdown']['spanningMultipleLines'] = "Or spanning multiple lines...";
$_LANG['markdown']['saved'] = "saved";
$_LANG['markdown']['saving'] = "autosaving";

$_LANG['oauth']['authoriseAppToAccess'] = "Authorise :appName<br />to access your account?";
$_LANG['oauth']['willBeAbleTo'] = "This application will be able to";
$_LANG['oauth']['authorise'] = "Authorise";
$_LANG['oauth']['currentlyLoggedInAs'] = "You are currently logged in as :firstName :lastName";
$_LANG['oauth']['notYou'] = "Not You?";
$_LANG['oauth']['returnToApp'] = "Return to :appName";
$_LANG['oauth']['copyrightFooter'] = "Copyright &copy; :dateYear :companyName. All Rights Reserved.";
$_LANG['oauth']['loginToGrantApp'] = "Login to grant :appName<br />access to your account";
$_LANG['oauth']['redirectDescriptionOne'] = "Redirecting you back to the application. This may take a few moments.";
$_LANG['oauth']['redirectDescriptionTwo'] = "If your browser doesn't redirect you, please";
$_LANG['oauth']['redirectDescriptionThree'] = "click here to continue";
$_LANG['downloadLoginRequiredTagline'] = "Please login to access the requested file download";

$_LANG['orderForm']['year'] = "Year";
$_LANG['orderForm']['years'] = "Years";
$_LANG['orderForm']['domainOrKeyword'] = "Enter a domain or keyword";
$_LANG['orderForm']['searching'] = "Searching";
$_LANG['orderForm']['domainIsUnavailable'] = "<strong>:domain</strong> is unavailable"; //Strong tag is required here
$_LANG['orderForm']['add'] = "Add";
$_LANG['orderForm']['suggestedDomains'] = "Suggested Domains";
$_LANG['orderForm']['generatingSuggestions'] = "Generating suggestions for you";
$_LANG['orderForm']['addHosting'] = "Add Web Hosting";
$_LANG['orderForm']['chooseFromRange'] = "Choose from a range of web hosting packages";
$_LANG['orderForm']['packagesForBudget'] = "We have packages designed to fit every budget";
$_LANG['orderForm']['exploreNow'] = "Explore packages now";
$_LANG['orderForm']['transferToUs'] = "Transfer your domain to us";
$_LANG['orderForm']['transferExtend'] = "Transfer now to extend your domain by 1 year!";
$_LANG['orderForm']['transferDomain'] = "Transfer a domain";
$_LANG['orderForm']['extendExclusions'] = "Excludes certain TLDs and recently renewed domains";
$_LANG['orderForm']['singleTransfer'] = "Single domain transfer";
$_LANG['orderForm']['enterDomain'] = "Please enter your domain";
$_LANG['orderForm']['authCode'] = "Authorization Code";
$_LANG['orderForm']['authCodePlaceholder'] = "Epp Code / Auth Code";
$_LANG['orderForm']['authCodeTooltip'] = "To initiate a transfer you will need to obtain the authorization code from your current registrar. These can often be referred to as either the epp code or auth code. They act as a password and are unique to the domain name.";
$_LANG['orderForm']['help'] = "Help";
$_LANG['orderForm']['required'] = "Required";

$_LANG['orderForm']['checkingAvailability'] = 'Checking availability';
$_LANG['orderForm']['verifyingTransferEligibility'] = 'Verifying transfer eligibility';
$_LANG['orderForm']['verifyingDomain'] = 'Verifying your domain selection';
$_LANG['orderForm']['transferEligible'] = 'Your domain is eligible for transfer';
$_LANG['orderForm']['transferUnlockBeforeContinuing'] = 'Please ensure you have unlocked your domain at your current registrar before continuing.';
$_LANG['orderForm']['transferNotEligible'] = 'Not Eligible for Transfer';
$_LANG['orderForm']['transferNotRegistered'] = 'The domain you entered does not appear to be registered.';
$_LANG['orderForm']['trasnferRecentlyRegistered'] = 'If the domain was registered recently, you may need to try again later.';
$_LANG['orderForm']['transferAlternativelyRegister'] = 'Alternatively, you can perform a search to register this domain.';
$_LANG['orderForm']['domainInvalid'] = 'Invalid domain name provided';
$_LANG['orderForm']['domainInvalidCheckEntry'] = 'Please check your entry and try again.';
$_LANG['orderForm']['domainPriceRegisterLabel'] = 'Continue to register this domain for';
$_LANG['orderForm']['domainPriceTransferLabel'] = 'Transfer to us and extend by 1 year* for';

$_LANG['change'] = "Change";

$_LANG['filemanagement']['nofileuploaded'] = "No file uploaded.";
$_LANG['filemanagement']['invalidname'] = "Valid filenames contain only alpha-numeric, dot, hyphen and underscore characters.";
$_LANG['filemanagement']['couldNotSaveFile'] = "Could not save uploaded file.";
$_LANG['filemanagement']['checkPermissions'] = "Please check permissions.";
$_LANG['filemanagement']['checkAvailableDiskSpace'] = "Please check available disk space.";
$_LANG['filemanagement']['fileAlreadyExists'] = "File already exists.";
$_LANG['filemanagement']['noUniqueName'] = "Unable to find a unique filename.";

$_LANG['cartSimpleCaptcha'] = "Please enter the code shown below";

$_LANG['clientHomePanels']['showingRecent100'] = "Showing the most recent 100 records";
$_LANG['orderForm']['domainLetterOrNumber'] = "Domains must begin with a letter or a number";
$_LANG['orderForm']['domainLengthRequirements'] = " and be between <span class=\"min-length\"></span> and <span class=\"max-length\"></span> characters in length";

$_LANG['clientareatransferredaway'] = "Transferred Away";
$_LANG['clientareacompleted'] = "Completed";
$_LANG['domainContactUs'] = "Contact Us";

$_LANG['orderForm']['shortPerYear'] = "/:yearsyr";
$_LANG['orderForm']['shortPerYears'] = "/:yearsyrs";

$_LANG['domainCheckerSalesGroup']['sale'] = "Sale";
$_LANG['domainCheckerSalesGroup']['hot'] = "Hot";
$_LANG['domainCheckerSalesGroup']['new'] = "New";

#part 2

$_LANG["globalsystemname"] = "Beranda";
$_LANG["globalyouarehere"] = "Halaman";
$_LANG["language"] = "Bahasa";
$_LANG["imagecheck"] = "Silakan masukkan kode pengaman pada gambar dengan baik d an benar";
$_LANG["closewindow"] = "Tutup Halaman";
$_LANG["headertext"] = "Selamat Datang di Dijaminmurah.com";
$_LANG["clientareadescription"] = "Klik di sini untuk ubah data, lihat informasi billing pelayanan pemesanan";
$_LANG["knowledgebasedescription"] = "Klik di sini untuk pertanyaan seputar Doma in/Hosting";
$_LANG["viewticketsdescription"] = "Klik di sini untuk melihat status tiket";
$_LANG["submitticketdescription"] = "Kirim trouble ticket";
$_LANG["announcementsdescription"] = "Lihat Berita terbaru dan pemberitahuan";
$_LANG["downloadsdescription"] = "Lihat Daftar Download";
$_LANG["affiliatesdescription"] = "Click here to become an affiliate or see your earnings";
$_LANG["checkavailability"] = "Periksa Ketersediaan";
$_LANG["orderhomedescription"] = "Pesan Order untuk produk & Services";
$_LANG["availabilitydescription"] = "Periksa Domain";
$_LANG["serverstatusdescription"] = "Periksa Status Server";
$_LANG["supportpresalesquestions"] = "Jika ada pertanyaan ";
$_LANG["supportclickheretocontact"] = "Klik di sini untuk Hubungi Kami";
$_LANG["clientareanavhome"] = "Area Anggota";
$_LANG["clientareanavdetails"] = "Informasi User";
$_LANG["clientareanavhosting"] = "Atur Hosting";
$_LANG["clientareahostingaddons"] = "Tambahan";
$_LANG["clientareahostingaddonsview"] = "Lihat";
$_LANG["clientareanavdomains"] = "Atur Domain";
$_LANG["clientareanavsupporttickets"] = "LihatTiket";
$_LANG["clientareanavorder"] = "Pesan Item Tambahan";
$_LANG["clientareanavlogout"] = "Keluar";
$_LANG["clientareatitle"] = "Halaman Anggota";
$_LANG["clientareaheader"] = "Selamat datang di halaman Member. Anda dapat melih at, meubah rincian transaksi, melihat rincian domain dan hosting, kirim pengadua n, dan pesanan tambahan lainnya.";
$_LANG["clientareaupdateyourdetails"] = "Update Informasi Anda";
$_LANG["clientareanumhostingaccounts"] = "Jumlah Hosting Accounts";
$_LANG["clientareanumreselleraccounts"] = "Jumlah Accounts";
$_LANG["clientareanumserveraccounts"] = "Jumlah Dedicated/VPS Server";
$_LANG["clientareanumproductaccounts"] = "Jumlah Produk/Pelayanan lainnya";
$_LANG["clientareanumdomains"] = "Jumlah Domain";
$_LANG["clientareanumaffiliates"] = "Jumlah Affiliate Signups";
$_LANG["clientareacreditbalance"] = "Account Credit Balance";
$_LANG["clientareafullname"] = "Nama Anggota";
$_LANG["clientareafirstname"] = "Nama Depan";
$_LANG["clientarealastname"] = "Nama Belakang";
$_LANG["clientareaemail"] = "Email";
$_LANG["clientareaaddress1"] = "Alamat 1";
$_LANG["clientareaaddress2"] = "Alamat 2";
$_LANG["clientareacity"] = "Kota";
$_LANG["clientareastate"] = "Wilayah/Propinsi";
$_LANG["clientareapostcode"] = "Kode Pos";
$_LANG["clientareacountry"] = "Negara";
$_LANG["clientareaselectcountry"] = "Pilih Negara";
$_LANG["clientareaphonenumber"] = "No. Telepon";
$_LANG["clientareachangepassword"] = "Ubah Password";
$_LANG["clientarealeaveblank"] = "Biarkan kosong bila tidak ingin ubah password. ";
$_LANG["clientareapassword"] = "Password";

$_LANG["clientareaconfirmpassword"] = "Konfirmasi Password";
$_LANG["clientareaupdatebutton"] = "ubah";
$_LANG["clientareachangesuccessful"] = "Data Anda Berhasil diubah";
$_LANG["clientareahostingregdate"] = "Tanggal Registrasi";
$_LANG["clientareahostingdomain"] = "Domain";
$_LANG["clientareahostingpackage"] = "Paket";
$_LANG["clientareahostingnextduedate"] = "Jatuh Tempo";
$_LANG["clientareahostingamount"] = "Jumlah";
$_LANG["clientareahostingnone"] = "No Hosting Accounts Setup";
$_LANG["clientareaaddon"] = "Tambahan";
$_LANG["clientareaaddonsfor"] = "Tambahan untuk";
$_LANG["clientareaaddonorderconfirmation"] = "Terima Kasih. Pesanan Anda untuk A ddon sudah terdaftar. Silakan pilih Cara Pembayaran.";
$_LANG["firstpaymentamount"] = "Jumlah Pembayaran I";
$_LANG["recurringamount"] = "Di tahun berikut";
$_LANG["orderavailableaddons"] = "Periksa Tambahan yang tersedia";
$_LANG["clientareadomainexpirydate"] = "Tanggal Kadaluarsa";
$_LANG["clientareadomainnone"] = "Tidak ada Domain Yang terdaftar";
$_LANG["clientareaactive"] = "Aktif";
$_LANG["clientareapending"] = "Pending";
$_LANG["clientareasuspended"] = "Diblokir";
$_LANG["clientareaterminated"] = "Dihapus";
$_LANG["clientareaexpired"] = "Kadaluarsa";
$_LANG["clientareaticktoenable"] = "Centang untuk aktif";
$_LANG["recordsfound"] = "Rekord ditemukan";
$_LANG["page"] = "Halaman";
$_LANG["pageof"] = "dari";
$_LANG["previouspage"] = "halaman sebelumnya";
$_LANG["nextpage"] = "Halaman Berikutnya";
$_LANG["contacttitle"] = "Pre-Sales Contact Us";
$_LANG["contactheader"] = "Jika ada pertanyaan, silakan isi form di bawah.";
$_LANG["contactname"] = "Nama";
$_LANG["contactemail"] = "Email";
$_LANG["contactsubject"] = "Judul";
$_LANG["contactmessage"] = "Pesan";
$_LANG["contactsend"] = "Kirim";
$_LANG["contactsent"] = "Pesan Anda Sudah di kirim ";
$_LANG["clientareaaddonpricing"] = "Harga";
$_LANG["clientareaviewdetails"] = "Lihat Rincian/Manage";
$_LANG["clientareaproductdetails"] = "Rincian Produk";
$_LANG["clientareastatus"] = "Status";
$_LANG["clientareaaccountaddons"] = "Account Addons";
$_LANG["clientareaproducts"] = "Produk & Service";
$_LANG["clientareaemails"] = "Email";
$_LANG["clientareaemailsintrotext"] = "Berikut adalah Pesan yang dikirim ke Anda .";
$_LANG["clientareaemailsdate"] = "Tanggal Kirim";
$_LANG["clientareaemailssubject"] = "Judul Pesan";
$_LANG["clientareacpanellink"] = "Login Ke cPanel";
$_LANG["clientareawebmaillink"] = "Login ke Webmail";
$_LANG["clientareawhmlink"] = "Login ke WHM";
$_LANG["clientareabacklink"] = "<< back";
$_LANG["clientarearegistrationperiod"] = "Periode Registrasi";
$_LANG["clientareamodifywhoisinfo"] = "ubah Informasi WHOIS";
$_LANG["clientareamodifynameservers"] = "ubah Nameservers";
$_LANG["clientareanameserver"] = "Nameserver";
$_LANG["clientareasavechanges"] = "Simpan";
$_LANG["clientareasetlocking"] = "Set Locking";
$_LANG["clientareaerroroccured"] = "Gagal. Silakan Coba lagi.";
$_LANG["clientareanoaddons"] = "Tidak ada Addons untuk Account ini";

$_LANG["clientareaviewaddons"] = "Lihat Addons yang tersedia";
$_LANG["clientareaproductsnone"] = "Tidak ada Pemesanan Produk/Services";
$_LANG["clientareacancel"] = "Batal";
$_LANG["clientareamodifydomaincontactinfo"] = "ubah Informasi Contact Domain";
$_LANG["clientareacompanyname"] = "Nama Perusahaan";
$_LANG["clientareadiskusage"] = "Disk Space Usage";
$_LANG["clientareadisklimit"] = "Disk Space Limit";
$_LANG["clientareabwusage"] = "Bandwidth usage";
$_LANG["clientareabwlimit"] = "Bandwidth Limit";
$_LANG["clientareaused"] = "Used";
$_LANG["clientareaunlimited"] = "Unlimited";
$_LANG["clientarealastupdated"] = "Last Updated";
$_LANG["clientareacancelrequest"] = "Permohonan Pembatalan ORDER";
$_LANG["clientareacancelproduct"] = "Permohonan Pembatalan ORDER untuk";
$_LANG["clientareacancelreason"] = "Alasan Pembatalan Pemesanan";
$_LANG["clientareacancelinvalid"] = "ERROR! Either the account you are trying to request a cancellation for is already pending for cancellation or the item coul d not be found.";
$_LANG["clientareacancelrequestbutton"] = "Request Cancellation";
$_LANG["clientareacancelconfirmation"] = "Thank You. Your cancellation request h as been submitted. If you have done this in error, open a support ticket to noti fy us immediately or your account may be terminated.";
$_LANG["clientareaerrors"] = "The following errors occurred:";
$_LANG["clientareaerrorfirstname"] = "Silakan masukkan nama Depan";
$_LANG["clientareaerrorlastname"] = "Silakan masukkan nama Belakang";
$_LANG["clientareaerroremail"] = "Silakan Masukkan Alamt Email";
$_LANG["clientareaerroremailinvalid"] = "Alamat Email salah";
$_LANG["clientareaerroraddress1"] = "Masukkan Alamat Anda (alamat 1)";
$_LANG["clientareaerrorcity"] = "Masukkan Kota Anda";
$_LANG["clientareaerrorstate"] = "Masukkan Propinsi";
$_LANG["clientareaerrorpostcode"] = "Silakan Masukkan Kode POS";
$_LANG["clientareaerrorphonenumber"] = "Silakan Masukkan No. telepon yg bisa di hubungi";
$_LANG["clientareaerrorpasswordconfirm"] = "Anda Belum memasukkan Konfirmasi Pas sword";
$_LANG["clientareaerrorpasswordnotmatch"] = "Password tidak sama";
$_LANG["clientareaerrorisrequired"] = "harus di isi";
$_LANG["contacterrorname"] = "Anda belum memasukkan nama Anda";
$_LANG["contacterrorsubject"] = "Silakan Masukkan judul";
$_LANG["contacterrormessage"] = "Silakan Masukkan Pesan Anda";
$_LANG["supportticketinvalid"] = "Ada kesalahan. Tiket tidak di temukan.";
$_LANG["supportticketserrornoname"] = "Silakan masukkan Nama Anda";
$_LANG["supportticketserrornoemail"] = "Silakan Masukkan Alamat Email Anda";
$_LANG["supportticketserrornosubject"] = "Anda belum memasukkan judul";
$_LANG["supportticketserrornomessage"] = "Anda belum memasukkan Pesan";
$_LANG["supportticketsuploadfailed"] = "Gagal Upload file";
$_LANG["supportticketsfilenotallowed"] = "File tidak di ijinkan upload.";
$_LANG["clientareaerrorbannedemail"] = "Alamat email tidak di ijinkan.Silakan gu nakan alamat Email yang lainnya.";
$_LANG["announcementstitle"] = "Berita Terbaru";
$_LANG["announcementsnone"] = "Tidak ada Berita";
$_LANG["announcementsrss"] = "Lihat RSS Feed";
$_LANG["downloadstitle"] = "Downloads";
$_LANG["downloadsintrotext"] = "Halaman Download mencakup buku petunjuk, program , dan file-fle yang di butuhkan untuk mendukung website/Domain Anda.";
$_LANG["downloadsnone"] = "Tidak Ada Download";
$_LANG["knowledgebasetitle"] = "Knowledgebase";
$_LANG["knowledgebaseintrotext"] = "Knowlegdebase mencakup pertanyaan seputar Ho sting/Domain";
$_LANG["knowledgebasecategories"] = "Kategori";

$_LANG["knowledgebasepopular"] = "Top Rank -- ";
$_LANG["knowledgebasearticles"] = "Artikel";
$_LANG["knowledgebasenoarticles"] = "Tidak ada Artikel";
$_LANG["knowledgebaseviews"] = "Views";
$_LANG["knowledgebasesearch"] = "Cari";
$_LANG["knowledgebasehelpful"] = "Apakah jawaban ini membantu?";
$_LANG["knowledgebaserating"] = "Rating:";
$_LANG["knowledgebaseratingtext"] = "Users Found This Useful";
$_LANG["knowledgebasevotes"] = "Votes";
$_LANG["knowledgebaseyes"] = "Ya";
$_LANG["knowledgebaseno"] = "Tidak";
$_LANG["knowledgebasevote"] = "Vote";
$_LANG["knowledgebaseprint"] = "Cetak Artikel";
$_LANG["knowledgebasefavorites"] = "Add to Favourites";
$_LANG["knowledgebaserelated"] = "Artikel Terkait";
$_LANG["knowledgebasenorelated"] = "Tidak ada artikel terkait";
$_LANG["knowledgebasemore"] = "Selengkapnya";
$_LANG["loginintrotext"] = "Anda harus Login terlebih dahulu.";
$_LANG["loginemail"] = "Email";
$_LANG["loginpassword"] = "Password";
$LANG["loginrememberme"] = "Simpan Password";
$_LANG["loginbutton"] = "Login";
$_LANG["loginforgotten"] = "Lupa Password?";
$_LANG["loginforgotteninstructions"] = "klik di sini untuk Lupa Password";
$_LANG["loginincorrect"] = "Username dan Password tidak sesuai. Silakan ulangi";
$_LANG["logouttitle"] = "Keluar";
$_LANG["logoutsuccessful"] = "Anda Berhasil Keluar.";
$_LANG["logoutcontinuetext"] = "Klik Di sini Untuk melanjutkan.";
$_LANG["passwordremindertitle"] = "Pengingat Password";
$_LANG["passwordreminderintrotext"] = "Jika Anda lupa Password, silakan masukkan alamat email Anda, dan password akan di kirim ke alamat Email.";
$_LANG["passwordreminderdetailssent"] = "Data Sudah Terkirim Ke E-mail yang anda isi";
$_LANG["passwordremindernotfound"] = "Alamat Email tidak di temukan";
$_LANG["passwordremindersendbutton"] = "Request Password";
$_LANG["supportticketspagetitle"] = "Support Tickets";
$_LANG["supportticketssystemdescription"] = "The support ticket system allows us to respond to your problems and enquiries as quickly as possible. When we make a response to your support ticket, you will be notified via email.";
$_LANG["supportticketssubmitticket"] = "Kirim Tiket";
$_LANG["supportticketsheader"] = "Jika Anda tidak menemukan Solusi dari Knowledg ebase, Anda bisa kirim tiket pengaduan ke Dijaminmurah.com"; $_LANG["supportticketsticketcreated"] = "Ticket Created";
$_LANG["supportticketsticketcreateddesc"] = "Your ticket has been successfully c reated. An email has been sent to your address with the ticket information. If y ou would like to view this ticket now you can do so.";
$_LANG["supportticketsopentickets"] = "Open Support Tickets";
$_LANG["supportticketsnotickets"] = "No support tickets created";
$_LANG["supportticketsnoopentickets"] = "Tidak Ada support tickets";
$_LANG["supportticketsviewticket"] = "Lihat Tiket";
$_LANG["supportticketsposted"] = "Posted";
$_LANG["supportticketsreply"] = "Reply";
$_LANG["supportticketsstaff"] = "Staff";
$_LANG["supportticketsclient"] = "Client";
$_LANG["supportticketsticketid"] = "Ticket ID";
$_LANG["supportticketsdate"] = "Tanggal";
$_LANG["supportticketsdepartment"] = "Department";
$_LANG["supportticketssubject"] = "Judul";
$_LANG["supportticketsstatus"] = "Status";
$_LANG["supportticketsstatusopen"] = "Buka";

$_LANG["supportticketsstatusanswered"] = "Jawab";
$_LANG["supportticketsstatusinprogress"] = "Dalam Proses";
$_LANG["supportticketsstatusonhold"] = "di Tunda";
$_LANG["supportticketsstatuscustomerreply"] = "Customer-Reply";
$_LANG["supportticketsstatusclosed"] = "Tutup";
$_LANG["supportticketschoosedepartment"] = "Pilih Department";
$_LANG["supportticketsclientname"] = "Nama";
$_LANG["supportticketsclientemail"] = "Email";
$_LANG["supportticketsticketsubject"] = "Judul";
$_LANG["supportticketsticketurgency"] = "Kepentingan";
$_LANG["supportticketsticketurgencylow"] = "biasa";
$_LANG["supportticketsticketurgencymedium"] = "Sedang";
$_LANG["supportticketsticketurgencyhigh"] = "Penting";
$_LANG["supportticketsticketattachment"] = "Attachment";
$_LANG["supportticketsallowedextensions"] = "Allowed File Extensions";
$_LANG["supportticketsticketsubmit"] = "Kirim";
$_LANG["supportticketserror"] = "gagal";
$_LANG["supportticketsnotfound"] = "Tiket tidak di temukan";
$_LANG["affiliatestitle"] = "Affiliasi Anda";
$_LANG["affiliatesintrotext"] = "Activate your affiliate account today to:";
$_LANG["affiliatesbullet1"] = "Receive an initial bonus deposit in your affiliat e account of";
$_LANG["affiliatesearn"] = "Earn";
$_LANG["affiliatesbullet2"] = "of every payment each customer you refer to us ma kes for the entire duration of their hosting account";
$_LANG["affiliatesfootertext"] = "When you refer someone to our website with you r unique referral ID, a cookie is placed on their computer containing this ID so if they bookmark the site and come back later you will still receive the commis sion.";
$_LANG["affiliatesactivate"] = "Activate Affiliate Account";
$_LANG["affiliatesrealtime"] = "These statistics are in real time and update ins tantly";
$_LANG["affiliatesreferallink"] = "Your Unique Referral Link";
$_LANG["affiliatesregdate"] = "Registration Date";
$_LANG["affiliatesvisitorsreferred"] = "Number of Visitors Referred";
$_LANG["affiliatesearningstodate"] = "Total Earnings to Date";
$_LANG["affiliateswithdrawn"] = "Total Amount Withdrawn";
$_LANG["affiliatesbalance"] = "Current Balance";
$_LANG["affiliatesreferals"] = "Your referral";
$_LANG["affiliatesnosignups"] = "You have currently not received any signups";
$_LANG["affiliatessignupdate"] = "Signup Date";
$_LANG["affiliateshostingpackage"] = "Hosting Package";
$_LANG["affiliatesamount"] = "Amount";
$_LANG["affiliatescommission"] = "Commission";
$_LANG["affiliatesstatus"] = "Status";
$_LANG["affiliateslinktous"] = "Link to Us";
$_LANG["affiliatesdisabled"] = "We do not currently offer an affiliate system to our clients.";
$_LANG["affiliatesrequestwithdrawal"] = "Request Withdrawal";
$_LANG["affiliateswithdrawalrequestsuccessful"] = "Your request for a withdrawal has been submitted. You will be contacted shortly.";
$_LANG["domaintitle"] = "Periksa Domain";
$_LANG["domainintrotext"] = "Silakan masukkan Nama Domain, klik periksa untuk me lihat ketersediaan Domain.";
$_LANG["domainlookupbutton"] = "Periksa";
$_LANG["domainavailable1"] = "Selamat ! Domain ";
$_LANG["domainavailable2"] = " Tersedia!";
$_LANG["domainavailableexplanation"] = "Untuk daftar Domain ini, silakan klik li nk di bawah";
$_LANG["domainordernow"] = "Pesan Sekarang!";

$_LANG["domainunavailable1"] = "Maaf, Domain";
$_LANG["domainunavailable2"] = "tidak dapat anda pergunakan!";
$_LANG["domainviewwhois"] = "Lihat whois";
$_LANG["domainalternatives"] = "Alternatif Lain:";
$_LANG["domainerror"] = "Mohon Maaf ada Kegagalan:";
$_LANG["domainerrorservererror"] = "Terdapat error pada request anda:";
$_LANG["domainerrornodomain"] = "Silakan masukkan Nama Domain dengan benar";
$_LANG["domainerrortoolong"] = "Domain terlalu panjang, maksimum 67 karakter.";
$_LANG["domaintld"] = "Ekstensi";
$_LANG["domainminyears"] = "Min. Tahun";
$_LANG["domainprice"] = "Harga";
$_LANG["serverstatustitle"] = "Status Server";
$_LANG["serverstatusheadingtext"] = "Tabel Berikut Menjelaskan Status dari serve r, Anda menggunakan halaman ini untuk memeriksa layanan Server apabila Down/mati .";
$_LANG["servername"] = "Server";
$_LANG["serverstatusserverload"] = "Server Load";
$_LANG["serverstatusphpinfo"] = "PHP Info";
$_LANG["serverstatusuptime"] = "Uptime";
$_LANG["serverstatusonline"] = "Online";
$_LANG["serverstatusoffline"] = "Offline";
$_LANG["serverstatusnoservers"] = "Tidak ada server yang bisa di monitor";
$_LANG["serverstatusnotavailable"] = "Tidak Tersedia";
$_LANG["invoices"] = "Invoices Saya";
$_LANG["invoicesnoinvoices"] = "Tidak Ada Invoices";
$_LANG["invoicestitle"] = "Invoice #";
$_LANG["invoicesinvoicedto"] = "Invoiced Ke";
$_LANG["invoicesinvoicenotes"] = "Catatan Invoice";
$_LANG["invoicesoutstandinginvoices"] = "Tunggakan Invoices";
$_LANG["invoicesdatecreated"] = "Tanggal Invoice";
$_LANG["invoicesdatedue"] = "Jatuh Tempo";
$_LANG["invoicesstatus"] = "Status";
$_LANG["invoicesview"] = "Lihat Invoice";
$_LANG["invoicesunpaid"] = "Belum Dibayar";
$_LANG["invoicespaid"] = "LUNAS";
$_LANG["invoicescancelled"] = "Di Batalkan";
$_LANG["invoicesdescription"] = "Produk/Layanan";
$_LANG["invoicesamount"] = "Jumlah";
$_LANG["invoicessubtotal"] = "Sub Total";
$_LANG["invoicesbefore"] = "Sebelum";
$_LANG["invoicescredit"] = "Credit";
$_LANG["invoicestax"] = "Tax Due";
$_LANG["invoicestotal"] = "Total";
$_LANG["invoicespaynow"] = "Bayar Sekarang";
$_LANG["invoicesnotes"] = "Catatan";
$_LANG["invoiceserror"] = "Ada kegagalan, silakan coba lagi.";
$_LANG["invoicespayto"] = "Bayar Ke:";
$_LANG["invoicerefnum"] = "Nomor Invoice";
$_LANG["invoiceofflinepaid"] = "Offline Credit Card Payments are processed manua lly. You will receive confirmation by email once your payment has been processed .";
$_LANG["invoicestaxindicator"] = "Indicates a taxed item.";
$_LANG["invoicesdownload"] = "Download";
$_LANG["invoicestransactions"] = "Transaksi";
$_LANG["invoicestransdate"] = "Tanggal Transaksi";
$_LANG["invoicestransgateway"] = "Pembayaran :";
$_LANG["invoicestransid"] = "Transaction ID";
$_LANG["invoicestransamount"] = "Jumlah";
$_LANG["invoicestransnonefound"] = "Tidak di temukan Transaksi";
$_LANG["creditcard"] = "Pay by Credit Card";

$_LANG["creditcarddetails"] = "Credit Card Details";
$_LANG["creditcardyourinfo"] = "Your Information";
$_LANG["creditcarduseexisting"] = "Use Existing Card";
$_LANG["creditcardenternewcard"] = "Enter New Card Information Below";
$_LANG["creditcardcardtype"] = "Card Type";
$_LANG["creditcardcardnumber"] = "Card Number";
$_LANG["creditcardcvvnumber"] = "CVV/CVC2 Number";
$_LANG["creditcardsecuritynotice"] = "Any data you enter here is submitted secur ely and is encrypted to reduce the risk of fraud";
$_LANG["creditcardenterexpirydate"] = "You did not enter the card expiry date";
$_LANG["creditcardccvinvalid"] = "The card code you entered is invalid";
$_LANG["creditcarddeclined"] = "The credit card details you entered were decline d. Please try a different card or contact support.";
$_LANG["creditcardinvalid"] = "The credit card details you entered were invalid. Please try a different card or contact support.";
$_LANG["creditcardconfirmation"] = "Thank You! Your new card details have been a ccepted and the first payment for your account has been taken. You have been sen t a confirmation email about this.";
$_LANG["creditcardcvvwhere"] = "Where do I find this?";
$_LANG["flashtutorials"] = "Flash Tutorials";
$_LANG["flashtutorialsdescription"] = "Click here to view tutorials showing you how to use your hosting control panel";
$_LANG["flashtutorialsheadertext"] = "Our Flash Tutorials are here to help you f ully utilise your web hosting control panel. Choose a task from below to see a s tep by step tutorial on how to complete it.";
$_LANG["bannedtitle"] = "IP di Blok";
$_LANG["bannedyourip"] = "IP Address Anda";
$_LANG["bannedhasbeenbanned"] = "Telah di Blokir";
$_LANG["bannedbanreason"] = "Alasan Blokir";
$_LANG["bannedbanexpires"] = "Kadaluarsa Blokir";
$_LANG["ordertitle"] = "Pesan / Order";
$_LANG["orderprogress"] = "Dalam Proses";
$_LANG["orderchooseapackage"] = "Pilih Paket";
$_LANG["orderdomainoptions"] = "Pilihan Domain";
$_LANG["orderconfigure"] = "Konfigurasi";
$_LANG["orderyourinformation"] = "Informasi";
$_LANG["orderconfirmorder"] = "Konfirmasi Pesan";
$_LANG["ordercheckout"] = "Proses";
$_LANG["ordercategories"] = "Kategori";
$_LANG["ordernowbutton"] = "Pesan Sekarang";
$_LANG["ordernoproducts"] = "Tidak ada Produk";
$_LANG["orderavailable"] = "Tersedia";
$_LANG["orderfree"] = "Gratisss....!";
$_LANG["orderpaymenttermfreeaccount"] = "Free Account";
$_LANG["orderpaymenttermonetime"] = " Satu kali bayar";
$_LANG["orderpaymenttermmonthly"] = " Per 1 Bulan";
$_LANG["orderpaymenttermquarterly"] = " Per 3 Bulan";
$_LANG["orderpaymenttermsemiannually"] = "Per 6 Bulan";
$_LANG["orderpaymenttermannually"] = " Per 1 Tahun";
$_LANG["orderpaymenttermbiennially"] = " Per 2 Tahun";
$_LANG["ordersetupfee"] = "Biaya Setup";
$_LANG["orderdomainoption1part1"] = "Saya Akan Register Domain baru di ";
$_LANG["orderdomainoption1part2"] = ".";
$_LANG["orderdomainoption2"] = "Saya Akan Meubah Nameserver Domain Saya, Dan Say a Hanya Order Hosting Saja";
$_LANG["orderdomainoption3"] = "Saya Akan Transfer Domain ke ";
$_LANG["orderdomainoption4"] = "Saya ingin menggunakan Subdomain gratis.";
$_LANG["orderserverhostname"] = "Server Hostname";
$_LANG["orderserverrootpassword"] = "Root Password";
$_LANG["orderservernameservers"] = "Nameservers";

$_LANG["orderservernameserversdescription"] = "The prefixes you enter here will determine the default nameservers for the server eg. ns1.yourdomain.com and ns2. yourdomain.com";
$_LANG["orderservernameserversprefix1"] = "Prefix 1";
$_LANG["orderservernameserversprefix2"] = "Prefix 2";
$_LANG["ordercontinuebutton"] = "Berikutnya >>";
$_LANG["orderproduct"] = "Produk/Service";
$_LANG["orderdesc"] = "Deskripsi";
$_LANG["orderbillingcycle"] = "Periode Tagihan";
$_LANG["orderconfigpackage"] = "Pilihan Konfigurasi";
$_LANG["orderchooseaddons"] = "Pilih Addons Produk (ini hanya bersifat pilihan, tidak berkaitan dengan performa)"; $_LANG["orderaddondescription"] = "Addons Berikut adalah tersedia untuk Produk i ni. Silakan pilih Addons Produk.";
$_LANG["orderadditionalrequiredinfo"] = "Informasi Tambahan yang dibutuhkan";
$_LANG["orderdomain"] = "Domain";
$_LANG["orderregperiod"] = "Periode Pendaftaran";
$_LANG["orderyears"] = "Tahun";
$_LANG["ordertransfersecret"] = "Transfer Secret";
$_LANG["ordertransfersecretexplanation"] = "Silakan masukkan Domain Transfer Sec ret. ";
$_LANG["orderpaymentmethod"] = "Metode Pembayaran";
$_LANG["orderyourorder"] = "Pesanan Anda";
$_LANG["orderprice"] = "Harga";
$_LANG["orderaddon"] = "Addon";
$_LANG["orderprorata"] = "Pro Rata";
$_LANG["orderdomainregonly"] = "Hanya Pesan Register Domain";
$_LANG["orderdomainregistration"] = "Registrasi Domain";
$_LANG["orderdomaintransfer"] = "Domain Transfer";
$_LANG["orderregisterdomain"] = "Register Domain Baru";
$_LANG["ordertransferdomain"] = "Transfer domain name yang sudah ada";
$_LANG["orderchangenameservers"] = "Hanya ubah Nameserver";
$_LANG["orderusesubdomain"] = "Pilih Subdomain";
$_LANG["orderfreedomainonly"] = "Free Domain";
$_LANG["orderfreedomainregistration"] = "Registrasi Free Domain";
$_LANG["orderfreedomaindescription"] = "pada term pembayaran tertentu";
$_LANG["orderfreedomainappliesto"] = "terapkan hanya pada extensions berikut saj a";
$_LANG["ordertaxcalculations"] = "Tax Calculations";
$_LANG["ordertaxstaterequired"] = "You must enter your state for tax calculation s to take place";
$_LANG["ordersubtotal"] = "Subtotal";
$_LANG["ordertotalduetoday"] = "Total Due Hari Ini";
$_LANG["ordertotalrecurring"] = "Total Di tahun berikut";
$_LANG["orderchangeproduct"] = "Ubah Produk";
$_LANG["orderchangedomain"] = "Ubah Domain";
$_LANG["orderchangeconfig"] = "Change Configurable Options";
$_LANG["orderchangeaddons"] = "Ubah Addon";
$_LANG["orderpromotioncode"] = "Kode Promosi/ Voucher";
$_LANG["orderpromovalidatebutton"] = "Validasi Kode >>";
$_LANG["orderdontusepromo"] = "Tidak menggunakan kode Promosi";
$_LANG["orderdiscount"] = "Diskon";
$_LANG["orderinvalidcodeforbillingcycle"] = "Kode ini tidak berlaku untuk billin g cycle berikut";
$_LANG["ordercodenotfound"] = "Kode Promosi tidak terdaftar";
$_LANG["orderpromoinvalid"] = "Kode Promosi tidak sesuai dengan Pesanan Anda";
$_LANG["orderpromomaxusesreached"] = "Kode Promosi sudah pernah di gunakan";
$_LANG["orderpromoexpired"] = "Kode Promosi sudah Kadaluarsa";
$_LANG["ordernewuser"] = "Saya Pelanggan baru, dan ingin daftar account baru";
$_LANG["orderexistinguser"] = "Saya pelanggan lama Dijaminmurah.com, dan ingin menambahkan pesanan/order ini ke dalam account saya";
$_LANG["ordertosagreement"] = "Saya sudah baca dan setuju dengan";
$_LANG["ordertos"] = "Syarat dan Persetujuan";
$_LANG["orderlogininfo"] = "Infomasi Login";
$_LANG["orderlogininfopart1"] = "Silahkan masukkan password yang ingin anda guna kan untuk login";
$_LANG["orderlogininfopart2"] = "Client Area. This will differ from your website control panel username & password.";
$_LANG["orderfailed"] = "Order Gagal";
$_LANG["orderstartover"] = "Mulai Kembali";
$_LANG["orderconfirmation"] = "Konfirmasi Order";
$_LANG["ordernumberis"] = "Nomor Order Anda adalah :";
$_LANG["orderreceived"] = "Terima kasih atas pesanan Anda. Sesaat lagi Anda akan menerima email konfirmasi order. Silakan cek email Anda.";
$_LANG["orderfinalinstructions"] = "Jika Anda memiliki pertanyaan tentang order Anda, silakan kirimkan support ticket melalui client area Anda dan sebutkan nomo r order Anda.";
$_LANG["ordergotoclientarea"] = "Klik di sini untuk masuk ke Client Area";
$_LANG["ordererrordomainnotld"] = "Anda harus memasukkan TLD";
$_LANG["ordererrordomaininvalid"] = "Domain yang Anda masukkan tidak valid";
$_LANG["ordererrordomainregistered"] = "Domain yang kamu masukkan sudah terdafta r sebelumnya";
$_LANG["ordererrordomainnotregistered"] = "Anda tidak dapat mentransfer domain y ang tidak aktif";
$_LANG["ordererrornodomain"] = "Anda tidak memasukkan nama domain";
$_LANG["ordererrorsubdomaintaken"] = "Subdoman yang Anda masukkan sudah dipakai. Silakan ulangi.";
$_LANG["ordererrorservernohostname"] = "You must enter a hostname for your serve r";
$_LANG["ordererrorservernonameservers"] = "You must enter a prefix for both name servers";
$_LANG["ordererrorservernorootpw"] = "You must enter your desired root password" ;
$_LANG["ordererrorpassword"] = "You did not enter a password";
$_LANG["ordererroruserexists"] = "A user already exists with that email address" ;
$_LANG["ordersubdomaininuse"] = "The subdomain you entered is already in use";
$_LANG["ordererrornameserver1"] = "You must enter nameserver 1";
$_LANG["ordererrornameserver2"] = "You must enter nameserver 2";
$_LANG["ordererrortransfersecret"] = "You must enter the transfer secret";
$_LANG["clientareaerrorfirstname2"] = "Your first name can only contain letters" ;
$_LANG["clientareaerrorlastname2"] = "Your last name can only contain letters";
$_LANG["clientareaerroraddress12"] = "Your address can only contain letters, num bers and spaces";
$_LANG["clientareaerrorcity2"] = "Your city can only contain letters and spaces" ;
$_LANG["clientareaerrorpostcode2"] = "Your postcode can only contain letters, nu mbers and spaces";
$_LANG["clientareaerrorphonenumber2"] = "Your phone number is not valid";
$_LANG["clientareaerrorcountry"] = "Please choose your country from the drop dow n box";
$_LANG["ordererroraccepttos"] = "Anda Harus menyetujui Syarat dan Ketentuan Dija minmurah.com";
$_LANG["maxmind_title"] = "MaxMind";
$_LANG["maxmind_error"] = "Error";
$_LANG["maxmind_rejectemail"] = "We do not permit orders using a free email addr ess, please try again using a different email address";
$_LANG["maxmind_countrymismatch"] = "The country of your IP address did not matc h the billing address country you entered so we cannot accept your order";

$_LANG["maxmind_anonproxy"] = "We do not allow orders to be placed using an Anon ymous Proxy";
$_LANG["maxmind_highriskcountry"] = "We do not accept orders from your country a s there is a very high risk of fraud";
$_LANG["maxmind_highfraudriskscore"] = "Our system has detected a high risk of f raud for your order and has therefore blocked it";
$_LANG["maxmind_callingnow"] = "We are placing an automated call to your phone n umber now. This is part of our fraud checking measures. You will be given a 4 di git security code which you need to enter below to complete your order.";
$_LANG["maxmind_pincode"] = "Pin Code";
$_LANG["maxmind_incorrectcode"] = "Incorrect Code";
$_LANG["maxmind_faileddescription"] = "The code you entered was incorrect. If yo u feel this to be an error, please contact our support department as soon as pos sible.";
$_LANG["varilogixfraudcall_title"] = "VariLogix FraudCall";
$_LANG["varilogixfraudcall_pincode"] = "Pin Code";
$_LANG["varilogixfraudcall_description"] = "As part of our fraud prevention meas ures, we will now call the phone number registered for your account and ask you to enter the above pin code. Please make a note of the pin code and when you are ready for us to place the phone call, please click on the button below.";
$_LANG["varilogixfraudcall_callnow"] = "Call Now!";
$_LANG["varilogixfraudcall_failed"] = "Failed";
$_LANG["varilogixfraudcall_fail"] = "The call to verify your order failed. This could be because your phone number was incorrectly entered or is blacklisted on our system. Please contact our support department as soon as possible to complet e your order.";
$_LANG["varilogixfraudcall_error"] = "An error occurred and we could not call yo ur phone number to verify your order. Please contact our support department as s oon as possible to complete your order.";
$_LANG["clientareacancellationtype"] = "Cancellation Type";
$_LANG["clientareacancellationimmediate"] = "Immediate";
$_LANG["clientareacancellationendofbillingperiod"] = "End of Billing Period";
$_LANG["supportticketsstatuscloseticket"] = "Tutup Ticket";
$_LANG["invoicesbalance"] = "Balance";
$_LANG["invoicesbacktoclientarea"] = "<< Back to Client Area";
$_LANG["clientregistertitle"] = "Daftar Account";
$_LANG["clientregisterheadertext"] = "Please fill in the fields below to registe r for a new account. Fields marked with a * are required.";
$_LANG["clientregisterverify"] = "Verify Registration";
$_LANG["clientregisterverifydescription"] = "Please enter the text you see in th e image below into the text box provided. This is required to prevent automated registrations.";
$_LANG["clientregisterverifyinvalid"] = "Incorrect Verification Image Code Enter ed";
$_LANG["hometitle"] = "Home";
$_LANG["welcomeback"] = "Welcome Back";
$_LANG["please"] = "Silakan";
$_LANG["clientlogin"] = "Login Member";
$_LANG["email"] = "Email";
$_LANG["go"] = "Go";
$_LANG["invoicesdue"] = "Due Invoices";
$_LANG["serverlogindetails"] = "Info Login";
$_LANG["serverusername"] = "Username";
$_LANG["serverpassword"] = "Password";
$_LANG["serverchangepassword"] = "ubah Password";
$_LANG["serverchangepasswordenter"] = "Password Baru";
$_LANG["serverchangepasswordconfirm"] = "Ulangi Password Baru";
$_LANG["serverchangepasswordupdate"] = "Update";
$_LANG["serverchangepasswordsuccessful"] = "ubah Passwords Sukses!";
$_LANG["serverchangepasswordfailed"] = "ubah Passwords Gagal!";

$_LANG["domainspricing"] = "Harga Domain";
$_LANG["domainsregister"] = "Register";
$_LANG["domainstransfer"] = "Transfer";
$_LANG["domainsrenew"] = "Perpanjang";
$_LANG["domainregistryinfo"] = "Domain Registry Information";
$_LANG["domainrenewalprice"] = "Perpanjang";
$_LANG["choosedomains"] = "Pilih Domain";
$_LANG["domainname"] = "Domain Name";
$_LANG["domainstatus"] = "Status";
$_LANG["domainmoreinfo"] = "Info Lengkap";
$_LANG["domainavailable"] = "Tersedia! Order Sekarang";
$_LANG["domainunavailable"] = "Tidak Tersedia";
$_LANG["invoicesattn"] = "ATTN";
$_LANG["downloadscategories"] = "Kategori";
$_LANG["downloadsfiles"] = "Files";
$_LANG["downloadsfilesize"] = "Besar File";
$_LANG["creditcardcardexpires"] = "Expiry Date";
$_LANG["creditcardcardstart"] = "Start Date";
$_LANG["creditcardcardissuenum"] = "Issue Number";
$_LANG["upgradeerroroverdue"] = "You cannot upgrade/downgrade this account becau se it is overdue on payment.";
$_LANG["upgradechoosepackage"] = "Choose the package you want to upgrade/downgra de your current package to from the options below.";
$_LANG["upgradechooseconfigoptions"] = "Upgrade/Downgrade the configurable optio ns on this product.";
$_LANG["upgradecurrentconfig"] = "Current Configuration";
$_LANG["upgradenewconfig"] = "Configuration Baru";
$_LANG["upgradenochange"] = "No Change";
$_LANG["upgradesummary"] = "Below is a summary of your upgrade order.";
$_LANG["orderpaymentterm1month"] = "Harga 1 Bulan";
$_LANG["orderpaymentterm3month"] = "Harga 3 Bulan";
$_LANG["orderpaymentterm6month"] = "Harga 6 Bulan";
$_LANG["orderpaymentterm12month"] = "Harga 12 Bulan";
$_LANG["orderpaymentterm24month"] = "Harga 2 Bulan";
$_LANG["upgradedowngradepackage"] = "Upgrade/Downgrade Paket";
$_LANG["upgradedowngradeconfigoptions"] = "Pilihan Upgrade/Downgrade";
$_LANG["ordersecure"] = "Untuk Menjaga keamanan dan menghindari fraud, maka kami mencatat IP address Anda";
$_LANG["ordersecure2"] = "kedalam sistem kami.";
$_LANG["nocarddetails"] = "No existing card details on record";
$_LANG["domainnameservers"] = "Nameservers";
$_LANG["domainnameserver1"] = "Nameserver 1";
$_LANG["domainnameserver2"] = "Nameserver 2";
$_LANG["domainnameserver3"] = "Nameserver 3";
$_LANG["domainnameserver4"] = "Nameserver 4";
$_LANG["domainmanagementtools"] = "Management Tools";
$_LANG["domaincontactinfo"] = "Contact Information/ubah Whois Domain";
$_LANG["domainemailforwarding"] = "Email Forwarding";
$_LANG["domaindnsmanagement"] = "DNS Management";
$_LANG["domainemailforwardingdesc"] = "If the Email Forwarding Server determines the Forward To address is invalid, we will disable the forwarding record automa tically. Please check the Forward To address before you enable it again. The cha nges on any existing forwarding record may not take effect for up to 1 hour.";
$_LANG["domaindnsmanagementdesc"] = "Point your domain to a web site by pointing to an IP Address, or forward to another site, or point to a temporary page (kno wn as Parking), and more. These records are also known as sub-domains.";
$_LANG["domaindnshostname"] = "Host Name";
$_LANG["domaindnsrecordtype"] = "Record Type";
$_LANG["domaindnsaddress"] = "Address";
$_LANG["domainidprotection"] = "ID Protection - sangat bermanfaat menghindari spam karena info WHOIS domain.";
$_LANG["domainrenew"] = "Perpanjang Domain";
$_LANG["domainrenewdesc"] = "Secure your domain by adding more years to it. Choo se how many years you want to renew it for below and then click order renewal.";
$_LANG["domainorderrenew"] = "Order Renewal";
$_LANG["domainemailforwardingprefix"] = "Prefix";
$_LANG["domainemailforwardingforwardto"] = "Forward To";
$_LANG["domainregistrarlock"] = "Registrar Lock";
$_LANG["domainregistrarlockdesc"] = "Beri Cek List Untuk Kunci Domain Anda (Kami sarankan). Ini Untuk mencegah Terjadinya Transfer Domain ini ke orang lain."; $_LANG["domaincurrentrenewaldate"] = "Current Renewal Date";
$_LANG["domainemailforwardingforwardto"] = "Forward To";
$_LANG["domainregistrarlock"] = "Registrar Lock";
$_LANG["domainregistrarlockdesc"] = "Beri Cek List Untuk Kunci Domain Anda (Kami sarankan). Ini Untuk mencegah Terjadinya Transfer Domain ini ke orang lain."; $_LANG["domaincurrentrenewaldate"] = "Current Renewal Date";
$_LANG["domaineppcode"] = "EPP Code";
$_LANG["domaineppcodedesc"] = "Anda harus mendapatkan EPP Code / Authorization C ode mendapatkan dari Registrar lama / tempat Anda membeli domain tsb";
$_LANG["domaineppcoderequired"] = "Anda harus memasukkan EPP Code / Authorizatio n Code untuk ";
$_LANG["invoicenumber"] = "Invoice #";
$_LANG["proformainvoicenumber"] = "Proforma Invoice #";
$_LANG["forwardingtogateway"] = "Please wait while you are redirected to the gat eway you chose to make payment...";
$_LANG["clientareacancelled"] = "Cancelled";
$_LANG["clientareafraud"] = "Fraud";

# Version 3.5

$_LANG["addfunds"] = "Add Funds";
$_LANG["addfundsdescription"] = "You can add funds to your account so that invoi ces are automatically paid when they are generated. All deposits are non-refunda ble.";
$_LANG["addfundsminimum"] = "Minimum Deposit";
$_LANG["addfundsmaximum"] = "Maximum Deposit";
$_LANG["addfundsmaximumbalance"] = "Maximum Balance";
$_LANG["addfundsamount"] = "Amount to Add";
$_LANG["addfundsminimumerror"] = "Minimum Deposit amount is";
$_LANG["addfundsmaximumerror"] = "Maximum Deposit amount is";
$_LANG["addfundsmaximumbalanceerror"] = "Maximum Balance amount is";
$_LANG["clientareanavcontacts"] = "Manage Kontak Info";
$_LANG["clientareanavaddcontact"] = "Tambah Kontak Info Baru";
$_LANG["clientareanavchangecc"] = "ubah Data Kartu Kredit";
$_LANG["clientareanavchangepw"] = "ubah Password";

#- General Announceme - Order Details, Wel Renewal Notices, Reg - Invoices & Billing - Support Ticket Not

$_LANG["clientareacontactsemails"] = "Email Preferences";
$_LANG["clientareacontactsemailsgeneral"] = "General Emails nts & Password Reminders";
$_LANG["clientareacontactsemailsproduct"] = "Product Emails come Emails, etc...";
$_LANG["clientareacontactsemailsdomain"] = "Domain Emails istration Confirmations, etc...";
$_LANG["clientareacontactsemailsinvoice"] = "Invoice Emails Reminders";
$_LANG["clientareacontactsemailssupport"] = "Support Emails ifications";

$_LANG["clientareachoosecontact"] = "Pilih Kontak Info";
$_LANG["clientareanocontacts"] = "No Contacts Found";
$_LANG["clientareadeletecontact"] = "Hapus Kontak Info";
$_LANG["clientareadeletecontactareyousure"] = "Are you sure you want to delete t his contact?";

$_LANG["newpassword"] = "Password Baru";
$_LANG["confirmnewpassword"] = "Ulangi Password Baru";
$_LANG["defaultbillingcontact"] = "Settingan Awal Kontak Info";
$_LANG["usedefaultcontact"] = "Settingan Awal Kontak Info (Kontak Info Default)" ;
$_LANG["carttitle"] = "Pesanan Saya";
$_LANG["addtocart"] = "Masukkan kedalam Pesanan";
$_LANG["viewcart"] = "lihat Pesanan";
$_LANG["emptycart"] = "Kosongkan Pesanan";
$_LANG["updatecart"] = "Update Pesanan";
$_LANG["continueshopping"] = "Lihat Pilihan Lainnya";
$_LANG["checkout"] = "OK - Proses Pesanan Saya";
$_LANG["cartempty"] = "Pesanan Anda Kosong";
$_LANG["carteditproductconfig"] = "ubah Configuration";
$_LANG["cartremove"] = "Remove";
$_LANG["cartconfigdomainextras"] = "Configure Domain Extras";
$_LANG["cartbrowse"] = "Browse Products & Services";
$_LANG["cartproductconfig"] = "Product Configuration";
$_LANG["cartproductdomaindesc"] = "Produk/Service yang Anda pilih membutuhkan na ma Domain. Silakan Masukkan nama Domain yang Anda inginkan.";
$_LANG["cartproductdomainuseincart"] = "Gunakan Nama Domain yang sudah ada di da lam Account Saya";
$_LANG["cartproductdomainchoose"] = "Pilih Domain";
$_LANG["cartproductdesc"] = "Produk/Service yang Anda pilih memiliki beberapa pi lihan konfigurasi yang bisa Anda tentukan Sendiri sesuai kebutuhan.";
$_LANG["cartconfigoptionsdesc"] = "Product/servis ini mempunyai beberapa pilihan di bawah untuk kostumisasi order anda.";
$_LANG["cartaddons"] = "Addon";
$_LANG["cartcustomfieldsdesc"] = "Product/servis ini memerlukan beberapa tambaha n informasi agar bisa kami proses lebih lanjut.";
$_LANG["cartproductdomain"] = "Domain";
$_LANG["registerdomain"] = "Daftar Domain";
$_LANG["transferdomain"] = "Transfer Domain";
$_LANG["registerdomainname"] = "Daftar Nama Domain";
$_LANG["transferdomainname"] = "Transfer Nama Domain";
$_LANG["registerdomaindesc"] = "Cukup masukkan nama domain yang ingin anda dafta rkan untuk mengecek apakah masih tersedia atau tidak.";
$_LANG["transferdomaindesc"] = "Ingin memindahkan domain anda ke tempat kami? Ji ka ya, masukkan nama domain anda untuk memulai transfer.";
$_LANG["cartdomainsconfig"] = "Konfigurasi Domain";
$_LANG["cartdomainsconfigdesc"] = "Below you can configure the domain names in y our shopping cart selecting the addon services you would like, providing require d information for them and defining the nameservers that they will use.";
$_LANG["cartdomainsnohosting"] = "No Hosting! Click to Add";
$_LANG["cartnameserversdesc"] = "If you want to use custom nameservers then ente r them below. By default, new domains will use our nameservers for hosting on ou r network.";
$_LANG["domainregistrantinfo"] = "Domain Registrant Information";
$_LANG["domainregistrantchoose"] = "Select the contact you want to use here";
$_LANG["cartexistingclientlogin"] = "Existing Client Login";
$_LANG["cartexistingclientlogindesc"] = "To add this order to your existing acco unt, you will need to login below.";

$_LANG["cartproductaddons"] = "Produk Tambahan";
$_LANG["cartproductaddonsnone"] = "Produk Tambahan Tidak tersedia untuk Paket in i";
$_LANG["cartproductaddonschoosepackage"] = "Pilih Package";
$_LANG["cartconfigserver"] = "Configure Server";
$_LANG["serverhostname"] = "Hostname";
$_LANG["serverns1prefix"] = "NS1 Prefix";
$_LANG["serverns2prefix"] = "NS2 Prefix";
$_LANG["serverrootpw"] = "Root Password";
$_LANG["ordercompletebutnotpaid"] = "Attention! Your order has been completed bu t you have not yet paid for it so it will not be activated.
Click on the li nk below to go to your invoice to make payment.";
$_LANG["domainregnotavailable"] = "Tidak Ada";
$_LANG["domainregistration"] = "Registrasi Domain";
$_LANG["domaintransfer"] = "Transfer Domain ";
$_LANG["domainrenewal"] = "Perpanjang Domain";
$_LANG["carttaxupdateselections"] = "Tax may be charged depending upon the state and country selections you make. Click to recalculate after making your choices .";
$_LANG["carttaxupdateselectionsupdate"] = "Update";
$_LANG["completeorder"] = "Complete Order";
$_LANG["yourdetails"] = "Data lengkap anda";
$_LANG["alreadyregistered"] = "Apakah Anda Sudah daftar Account?";
$_LANG["clickheretologin"] = "klik di sini untuk Login";

# Version 3.6

$_LANG["existingpassword"] = "Password lama";
$_LANG["existingpasswordincorrect"] = "Password lama tidak sesuai dengan passwor d pada kolom password lama yang anda tulis";
$_LANG["morechoices"] = "Pilihan lain";
$_LANG["checkavailability"] = "Cek ketersediaan";
$_LANG["latefee"] = "Biaya keterlambatan";
$_LANG["latefeeadded"] = "Biaya keterlambatan sudah dimasukkan";
$_LANG["clientareaaddfundsnotallowed"] = "Anda harus memiliki paling tidak satu order sehingga bisa memasukkan fund jadi hal ini belum bisa di proses saat ini! ";

# Version 3.6.1

$_LANG["domaingeteppcode"] = "Ambil EPP Code";
$_LANG["domaingeteppcodeexplanation"] = "EPP Code itu fungsinya tidak lebih dari sebuah password domain. Itu untuk memastikan apakah benar yang melakukan transf er domain adalah pemilik domain yang sebenarnya.";
$_LANG["domaingeteppcodeemailconfirmation"] = "EPP Code sudah berhasil diambil d an telah dikirim ke email registrant domain yang bersangkutan.";
$_LANG["domaingeteppcodeis"] = "EPP Code untuk domain anda adalah:";
$_LANG["domaingeteppcodefailure"] = "Maaf ada masalah saat mengambil EPP Code:";
$_LANG["domainregisterns"] = "Register Nameservers";
$_LANG["domainregisternsexplanation"] = "Dari sini anda bisa membuat dan melakuk an pengaturan name server untuk domain anda(Misal: ns1.domainanda.com, ns2.domai nanda.com).";
$_LANG["domainregisternsreg"] = "Daftarkan nameserver baru";
$_LANG["domainregisternsmod"] = "Ubah IP nameserver";
$_LANG["domainregisternsdel"] = "Hapus nameserver";
$_LANG["domainregisternsns"] = "Nameserver";
$_LANG["domainregisternsip"] = "Alamat IP";
$_LANG["domainregisternscurrentip"] = "Alamat IP lama";
$_LANG["domainregisternsnewip"] = "Alamat IP baru";
$_LANG["domainregisternsregsuccess"] = "Nameserver {$nameserver} berhasil dibuat ";
$_LANG["domainregisternsmodsuccess"] = "Nameserver {$nameserver} berhasil diubah ";
$_LANG["domainregisternsdelsuccess"] = "Nameserver {$nameserver} berhasil dihapu s";
$_LANG["invoiceaddcreditdesc1"] = "Total kredit tersisa adalah";
$_LANG["invoiceaddcreditdesc2"] = "Total tersebut bisa digunakan untuk mengaktif kan invoice berikut.";
$_LANG["invoiceaddcreditamount"] = "Silahkan masukkan jumlah untuk mengaktifkan" ;
$_LANG["invoiceaddcreditapply"] = "Aktifkan Kredit";
$_LANG["invoiceaddcreditovercredit"] = "Anda tidak bisa melakukan apply credit l ebih besar dari yang anda punya";
$_LANG["invoiceaddcreditoverbalance"] = "Anda tidak bisa meng apply credit lebih dari yang di tetapkan";
$_LANG["domainsimplesearch"] = "Pencarian domain mudah";
$_LANG["domainbulksearch"] = "Pencarian domain sekali banyak";
$_LANG["domainbulksearchintro"] = "Pencarian banyak ini memungkinkan untuk menca ri lebih dari 20 domain sekaligus. Masukkan nama domain dengan tatacara sebagi berikut, masukkan satu perbarisnya - jangan masukkan www. atau http:// di depan nama domainnya.";
$_LANG["cartdomainshashosting"] = "Memiliki Hosting";
$_LANG["domainsautorenew"] = "Perpanjangaan Otomatis";
$_LANG["domainsautorenewstatus"] = "Status saat ini";
$_LANG["domainsautorenewenabled"] = "Aktif";
$_LANG["domainsautorenewdisabled"] = "Non Aktif";
$_LANG["domainsautorenewenable"] = "Aktifkan Perpanjangan Otomatis";
$_LANG["domainsautorenewdisable"] = "Non Aktifkan Perpanjangan Otomatis";
$_LANG["domainsautorenewdisabledwarning"] = "PERHATIAN! Domain ini perpanjangan otomatisnya tidak diaktifkan.
Ini akan menyebabkan domain tidak aktif bila tidak dilakukan perpanjangan dan pada akhirnya tidak dapat dilakukan perpanjangn lagi.";
$_LANG["cartremoveitemconfirm"] = "Apakah benar anda ingin membuang item ini dar i kotak belanjaan?";
$_LANG["cartemptyconfirm"] = "Apakah benar anda ingin mengosongkan kotak belanja an?";
$_LANG["outofstock"] = "Stok tidak ada lagi";
$_LANG["outofstockdescription"] = "Untuk saat ini tidak ada stok untuk item ini oleh karena itu untuk beberapa waktu akan dimatikan, menunggu stok ada lagi agar bisa digunakan kembali. Untuk informasi lebih lanjut hubungi support.";
$_LANG["downloadpurchaserequired"] = "Akses Ditolak - Anda harus melakukan pemesanan yang berhubungan dengan produk sebelum anda mendownloadnya";
$_LANG["downloadloginrequired"] = "Akses Ditolak - Anda harus Login dulu sebelum mendownload file ini";
$_LANG["clientareapendingtransfer"] = "Pending Transfer";

# Version 3.6.2

$_LANG["more"] = "Lebih lagi";
$_LANG["kbsuggestions"] = "Rekomendasi";
$_LANG["kbsuggestionsexplanation"] = "The following articles were found in the k nowledgebase which may answer your question. Please review the suggestions befor e submission.";
$_LANG["sslconfsslcertificate"] = "Konfigurasi Sertifikasi SSL";
$_LANG["sslcertinfo"] = "Informasi Sertifikasi SSL";
$_LANG["sslorderdate"] = "Tanggal Order";
$_LANG["sslcerttype"] = "Tipe Sertifikat";
$_LANG["sslstatus"] = "Konfigurasi Status";
$_LANG["sslserverinfo"] = "Informasi Server";
$_LANG["sslserverinfodetails"] = "Anda harus mempunyai \"CSR\" (Certificate Sign ing Request) untuk mengonfigurasi sertifikat SSL. The CSR is an encrypted piece of text that is generated by the web server where the SSL Certificate will be in stalled. If you do not already have a CSR, you must generate one or ask your web hosting provider to generate one for you. Also please ensure you enter the corr ect information as it cannot be changed after the SSL Certificate has been issue d.";
$_LANG["sslservertype"] = "Tipe Web Server";
$_LANG["sslcsr"] = "CSR";
$_LANG["ssladmininfo"] = "Informasi Kontak Administrasi";
$_LANG["ssladmininfodetails"] = "The contact information below will not be displ ayed on the Certificate - it is used only for contacting you regarding this orde r. The SSL Certificate and future renewal reminders will be sent to the email ad dress specified below.";
$_LANG["sslcertapproveremail"] = "Certificate Approver Email";
$_LANG["sslcertapproveremaildetails"] = "You must now choose from the options be low where you would like the approval email request for this certificate to be s ent.";
$_LANG["sslconfigcomplete"] = "Konfigurasi Selesai";
$_LANG["sslconfigcompletedetails"] = "Your SSL certificate configuration has now been completed and sent to the Certificate Authority for validation. You should receive an email from them shortly to approve it.";
$_LANG["sslinvalidlink"] = "Kesalahan mengikuti link.";
$_LANG["sslerrorselectserver"] = "Anda harus memilih tipe server";
$_LANG["sslerrorentercsr"] = "Anda Harus Memasukkan Certificate Signing Request (CSR)";
$_LANG["sslerrorapproveremail"] = "Anda harus memilih email untuk persetujuan";
$_LANG["organizationname"] = "Nama Organisasi";
$_LANG["jobtitle"] = "Job Title";
$_LANG["upgradeproductlogic"] = "Upgrade price is calculated from a credit of th e unused portion of the current plan and billing of the new plan for the same pe riod";
$_LANG["days"] = "Hari";

# Telesign Module Messages

$_LANG['telesigntitle'] = "TeleSign phone verification.";
$_LANG['telesignmessage'] = "Phone verification initiated for number %s . Harap Tunggu...";
$_LANG['telesignpin'] = "Enter your PIN: ";
$_LANG['telesigntype'] = "Choose verification type for number %s:";
$_LANG['telesigninvalidpin'] = "The PIN you entered was invalid!";
$_LANG['telesignincorrectpin'] = "Incorrect Pin!";
$_LANG['telesigninvalidpin2'] = "The pin you entered was not valid.";
$_LANG['telesigninvalidpinmessage'] = "Pin Code Verification Failed";
$_LANG['telesignverificationcanceled'] = "There is a temporary problem with the phone verification service and phone verification has been canceled.";
$_LANG['telesignverificationproblem'] = "There was a problem with the phone veri fication service and your order could not be validated. Please try again later." ;
$_LANG['telesigninitiatephone'] = "We can't initiate phone verification for your number. Please contact us.";
$_LANG['telesignsmstextmessage'] = "Thank you for using our SMS verification sys tem. Your code is: %s Please enter this code on your computer now.!";
$_LANG['telesignphonecall'] = "Phone call";
$_LANG['telesignsms'] = "Sms";
$_LANG['telesigninvalidnumber'] = "Invalid phone number";
$_LANG['telesignverify'] = "Your phone number %s needs to be verified to complet e the order.";

# Version 3.7.0

$_LANG["show"] = "Show";
$_LANG["all"] = "Semua";
$_LANG["yes"] = "Ya";
$_LANG["no"] = "Tidak";
$_LANG["recurring"] = "Recurring";
$_LANG["relatedservice"] = "Servis Terkait";
$_LANG["none"] = "None";
$_LANG["ordersummary"] = "Order Summary";
$_LANG["norecordsfound"] = "No Records Found";
$_LANG["accountstats"] = "Account Statistics";
$_LANG["statsnumproducts"] = "Number of Products/Services";
$_LANG["statsnumdomains"] = "Number of Domains";
$_LANG["statsnumtickets"] = "Number of Support Tickets";
$_LANG["statsnumreferredsignups"] = "Number of Referred Signups";
$_LANG["statscreditbalance"] = "Account Credit Balance";
$_LANG["statsdueinvoicesbalance"] = "Due Invoices Balance";

# Version 3.8.0

$_LANG["networkissuestitle"] = "Network Issues";
$_LANG["networkissuesaffecting"] = "Affecting";
$_LANG["networkissuespriority"] = "Priority";
$_LANG["networkissuesdate"] = "Date";
$_LANG["networkissueslastupdated"] = "Last Updated";
$_LANG["networkissuesprioritycritical"] = "Critical";
$_LANG["networkissuesprioritylow"] = "Low";
$_LANG["networkissuesprioritymedium"] = "Medium";
$_LANG["networkissuespriorityhigh"] = "High";
$_LANG["networkissuesstatusopen"] = "Open";
$_LANG["networkissuesstatusreported"] = "Reported";
$_LANG["networkissuesstatusinprogress"] = "In Progress";
$_LANG["networkissuesstatusinvestigating"] = "Investigating";
$_LANG["networkissuesstatusoutage"] = "Outage";
$_LANG["networkissuesstatusscheduled"] = "Scheduled";
$_LANG["networkissuesstatusresolved"] = "Resolved";

$_LANG["networkissuestypeserver"] = "Server";
$_LANG["networkissuestypesystem"] = "System";
$_LANG["networkissuestypeother"] = "Other";
$_LANG["networkissuesnonefound"] = "No network issues found";
$_LANG["networkissuesaffectingyourservers"] = "Please Note: Issues affecting ser vers you have accounts on will be highlighted with a gold background";
$_LANG["creditcard3dsecure"] = "As part of our fraud prevention measures, you wi ll now be asked to perform the Verified by Visa or Mastercard SecureCode checks if your credit card is enrolled for this service";
$_LANG["ordernotes"] = "Notes / Additional Information";
$_LANG["ordernotesdescription"] = "You can enter any additional notes or informa tion you want including with your order here...";
$_LANG["ticketratingquestion"] = "How would you rate this reply?";
$_LANG["ticketreatinggiven"] = "You rated this response";
$_LANG["ticketratingpoor"] = "Poor";
$_LANG["ticketratingexcellent"] = "Excellent";
$_LANG["pleasewait"] = "Harap Tunggu...";
$_LANG["clientareadescription"] = "View & update your account details";
$_LANG["affiliatesdescription"] = "Join our affiliate program or view earnings";
$_LANG["announcementsdescription"] = "View our latest news & announcements";
$_LANG["downloadsdescription"] = "View our library of downloads";
$_LANG["knowledgebasedescription"] = "Browse our KB for answers to FAQs";
$_LANG["supportticketsdescription"] = "View and respond to existing tickets";
$_LANG["submitticketdescription"] = "Submit a trouble ticket";
$_LANG["presalescontactdescription"] = "Place any pre-sales enquiries here";
$_LANG["orderdescription"] = "Place a new order with us";
$_LANG["domaincheckerdescription"] = "Check the availability of a domain";
$_LANG["serverstatusdescription"] = "View live status info for our servers";
$_LANG["networkissuesdescription"] = "Read about current and scheduled network o utages";
$_LANG["domainbulktransfersearch"] = "Domain Transfer Massal";

////////// End of english language file.  Do not place any translation strings below this line!

echo '<pre>'; print_r( $_LANG );
exit();
